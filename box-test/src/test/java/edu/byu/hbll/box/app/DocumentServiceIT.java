/** */
package edu.byu.hbll.box.app;

import static org.junit.Assert.assertEquals;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

/** @author Charles Draper */
@RunWith(Parameterized.class)
public class DocumentServiceIT {

  // TODO test default limit and max limit
  // TODO test impls as well
  // TODO verify that principal source returns documents for /box/documents (harvest)

  private static final Client client = ClientBuilder.newClient();
  private static final ObjectMapper mapper = new ObjectMapper();

  private String name;
  private String path;
  private int status;

  public DocumentServiceIT(String name, String path, int status) {
    this.name = name;
    this.path = path;
    this.status = status;
  }

  @Test
  public void test() throws Exception {

    switch (name) {
      case "align":
        TestUtils.align(status);
        return;
      case "pause":
        Thread.sleep(status);
        return;
      case "reset":
        TestUtils.reset();
        return;
      default:
    }

    URI uri = URI.create("http://localhost:8080/box-test/" + path);

    Response response = client.target(uri).request().get();
    String json = response.readEntity(String.class);
    ObjectNode actual = (ObjectNode) mapper.readTree(json);

    Path file =
        Paths.get(
            "src/test/resources",
            uri.getPath().replaceFirst("/box-test/", "/document/") + "/" + name + ".json");

    if (!Files.exists(file)) {
      System.out.println(file + " does not exist, creating");
      TestUtils.writeFile(file, actual);
    }

    ObjectNode expected =
        (ObjectNode) mapper.readTree(new String(Files.readAllBytes(file), "utf8"));

    for (ObjectNode node : Arrays.asList(actual, expected)) {

      // remove volatile box fields
      removeVolatileBox(node);

      for (JsonNode document : node.path("documents")) {
        removeVolatileBox(document);

        for (JsonNode innerDoc : document.path("documents")) {
          removeVolatileBox(innerDoc);
        }
      }

      // remove other volatile fields
      node.remove("nextUri");
      node.remove("nextCursor");
    }

    try {
      assertEquals(status, response.getStatus());
      assertEquals(expected, actual);
    } catch (AssertionError e) {
      // uncomment to update existing files
      //      System.out.println(file + " is not up-to-date, updating");
      //      TestUtils.writeFile(file, actual);
      throw e;
    }
  }

  private void removeVolatileBox(JsonNode node) {
    if (node.has("@box")) {
      ObjectNode box = (ObjectNode) node.path("@box");
      box.remove("modified");
      box.remove("cursor");
      box.remove("processed");
    }
  }

  @Parameters(name = "{index}: {0}: {1}")
  public static Collection<Object[]> params() throws Exception {
    Object[][] params =
        new Object[][] {
          {"reset", null, 0},

          // make sure primaries are established
          {"pause", null, 10000},

          // tests not dependent on time schedules

          {"principal5", "box/documents", 200},
          {"view1", "view/documents", 200},
          {"localview1", "localview/documents", 200},
          {"cachedview1", "cachedview/documents", 200},
          {"localcachedview1", "localcachedview/documents", 200},
          {"viewsrc1", "viewsrc/documents/100", 404},
          {"viewsrc2", "viewsrc/documents/110", 404},
          {"viewsrc3", "viewsrc/documents/120", 404},
          {"localviewsrc1", "localviewsrc/documents/100", 404},
          {"localviewsrc2", "localviewsrc/documents/110", 404},
          {"localviewsrc3", "localviewsrc/documents/120", 404},
          {"principal1", "box/documents/100", 404},
          {"one1", "one/documents/100", 404},
          {"two1", "two/documents/100", 404},
          {"three1", "three/documents/100", 404},
          {"dependencies1", "dependencies/documents/101", 404},
          {"disabled", "disabled/documents/100", 400},
          {"fake", "disabled/documents/100", 400},
          {"directdisabled1", "directdisabled/documents/100", 410},
          {"deleted1", "deleted/documents/100", 404},
          {"deleted2", "deleted/documents/200", 404},
          {"ejbdirect1", "ejbprocess/documents/100", 404},
          {"cdidirect1", "cdiprocess/documents/100", 404},
          {"depemptyharvest1", "depemptyharvest/documents/100", 404},
          {"save1", "save/documents/100", 404},
          {"emptyharvest1", "emptyharvest/documents/100", 410},
          {"process1", "one/documents/110?process", 200},
          {"process2", "one/documents/111?process=true", 200},
          {"process3", "one/documents/112?process=false", 404},
          {"wait1", "one/documents/120?wait", 200},
          {"wait2", "one/documents/121?wait=true", 200},
          {"wait3", "one/documents/122?wait=false", 404},
          {"empty1", "empty/documents/600", 404},
          {"facet1", "facet/documents/100", 404},
          {"facet1.1", "facet/documents/101", 404},
          {"facetfield1", "facetfield/documents/100", 404},
          {"parent1", "parent/documents/100", 404},
          {"parent2", "parent/documents/101", 404},
          {"multiple1", "multiple/documents?id=100&id=101&id=102", 200},
          {"long1", "long/documents/100", 404},
          {"dependencyonly11", "dependencyonly/documents/100", 404},
          {"dependencyonly21", "dependencyonlydependent/documents/101", 404},
          {"process", "process/documents/100", 200},
          {"defaultlimit1", "defaultlimit/documents/100", 404},
          {"defaultlimit2", "defaultlimit/documents/101", 404},
          {"error1", "error/documents/100", 500},
          {"dependencyerror1", "dependencyerror/documents/100", 404},
          {"badview1", "badview/documents/100", 500},
          {"badview2", "badview/documents", 500},
          {"viewdeperror1", "viewdeperror/documents/100", 404},
          {"health1", "box/health", 200},
          {"health2", "principal/health", 200},
          {"health3", "view/health", 200},
          {"health4", "badview/health", 503},
          {"health5", "fake/health", 400},
          {"xsltsrc1", "xsltsrc/documents/100", 404},
          {"limitedviewsrc1", "limitedviewsrc/documents/100", 404},
          {"limitedviewsrc2", "limitedviewsrc/documents/101", 404},
          {"harvest1", "harvest/documents/100", 410},
          {"harvest2", "harvest/documents/100?process", 410},
          {"harvest3", "harvest/documents/100?wait", 410},
          {"view4", "view/documents/101", 404},
          {"localview4", "localview/documents/101", 404},
          {"cachedview4", "cachedview/documents/102", 404},
          {"localcachedview4", "localcachedview/documents/102", 404},
          {"unprocdeps1", "unprocdeps/documents/100", 404},
          {"savedeletedsrc1", "savedeletedsrc/documents/100", 404},
          {"savedeletedsrc2", "savedeletedsrc/documents/101?process", 410},
          {"savedeletedsrc3", "savedeletedsrc/documents/101?process", 200},
          {"facetviewdoclimitsrc1", "facetviewdoclimitsrc/documents/100", 404},
          {"facetviewdoclimitsrc2", "facetviewdoclimitsrc/documents/101", 404},
          {"facetviewdoclimitsrc3", "facetviewdoclimitsrc/documents/102", 404},
          {"facetviewdoclimitsrc4", "facetviewdoclimitsrc/documents/111", 404},
          {"facetviewdoclimitsrc5", "facetviewdoclimitsrc/documents/112", 404},
          {"facetviewdoclimitsrc6", "facetviewdoclimitsrc/documents/122", 404},
          {"unprocessdeps1", "unprocessdeps/documents/100", 404},

          // pause to allow documents to be built
          {"pause", null, 4000},
          {"principal2", "box/documents/100", 200},
          {"principal3", "principal/documents/100", 200},
          {"principal4", "principal/documents/100?metadataLevel=none", 200},
          {"principal4.1", "principal/documents/100?metadataLevel=core", 200},
          {"principal4.2", "principal/documents/100?metadataLevel=full", 200},
          {"principal6", "box/documents", 200},
          {"principal7", "principal/documents", 200},
          {"principal8.0", "principal/documents?metadataOnly", 200},
          {"principal8.1", "principal/documents?metadataOnly=true", 200},
          {"principal8.2", "principal/documents?metadataOnly=false", 200},
          {"principal8.3", "principal/documents/100?metadataOnly", 200},
          {"principal8.4", "principal/documents/100?metadataOnly=true", 200},
          {"principal8.5", "principal/documents/100?metadataOnly=false", 200},
          {"view2", "view/documents", 200},
          {"localview2", "localview/documents", 200},
          {"view3", "view/documents/100", 200},
          {"localview3", "localview/documents/100", 200},
          {"cachedview2", "cachedview/documents", 200},
          {"localcachedview2", "localcachedview/documents", 200},
          {"cachedview3", "cachedview/documents/100", 200},
          {"localcachedview3", "localcachedview/documents/100", 200},
          {"followview", "followview/documents", 200},
          {"facetview1", "facetview/documents", 200},
          {"localfacetview1", "localfacetview/documents", 200},
          {"facetview2", "facetview/documents/0", 200},
          {"localfacetview2", "localfacetview/documents/0", 200},
          {"cachedfacetview1", "cachedfacetview/documents", 200},
          {"localcachedfacetview1", "localcachedfacetview/documents", 200},
          {"cachedfacetview2", "cachedfacetview/documents/0", 200},
          {"localcachedfacetview2", "localcachedfacetview/documents/0", 200},
          {"one2", "one/documents/100", 200},
          {"two2", "two/documents/100", 200},
          {"three2", "three/documents/100", 200},
          {"dependencies2", "dependencies/documents/101", 200},
          {"directdisabled2", "directdisabled/documents/100", 410},
          {"deleted3", "deleted/documents/100?process", 200},
          {"deleted4", "deleted/documents/200", 410},
          {"deleted5", "deleted/documents", 200},
          {"deleted6", "deleted/documents?status=deleted", 200},
          {"deleted7", "deleted/documents?status=ready&status=deleted", 200},
          {"deleted8", "deleted/documents?status=ready", 200},
          {
            "deleted9", "deleted/documents?field=id", 200
          }, // deleted documents were showing up as unprocessed when specifying fields
          {"ejbdirect2", "ejbprocess/documents/100", 200},
          {"depemptyharvest2", "depemptyharvest/documents/100", 200},
          {"empty2", "empty/documents/600", 200},
          {"facet2", "facet/documents/100", 200},
          {"facet3", "facet/documents", 200},
          {"facet4", "facet/documents?facet=one:100", 200},
          {"facet5", "facet/documents?facet=one:100&facet=two:100", 200},
          {
            "facet6",
            "facet/documents?facet=one:100&facet=two:100&facet=one:fake&facet=two:fake",
            200
          },
          {"facet7", "facet/documents?facet=one:fake", 200},
          {"facet8", "facet/documents?facet=one:100&facet=two:fake", 200},
          {"facet9", "facet/documents?facet=one:100&facet=one:101", 200},
          {"facetfield2", "facetfield/documents?metadataLevel=full", 200},
          {"facetfield3", "facetfield/documents?facet=id:100", 200},
          {"facetfield4", "facetfield/documents?facet=depsource:one", 200},
          {"facetfield5", "facetfield/documents?facet=depsource:two", 200},
          {"facetfield6", "facetfield/documents?facet=depid:100", 200},
          {"save2", "save/documents/100", 404},
          {"cdidirect2", "cdiprocess/documents/100", 200},
          {"parent3", "parent/documents", 200},
          {"parent4", "parent/documents/100?process", 200},
          {"parent5", "parent/documents/101?process", 200},
          {"multiple2", "multiple/documents?id=100&id=101&id=102", 200},
          {"long2", "long/documents/100", 200},
          {"dependencyonly12", "dependencyonly/documents/100", 404},
          {"dependencyonly22", "dependencyonly/documents/101", 200},
          {"facetedview1", "facetedview/documents", 200},
          {"facetedview2", "facetedview/documents?facet=last:0", 200},
          {"facetedview3", "facetedview/documents?facet=last:1", 200},
          {"facetedview4", "facetedview/documents?facet=last:2", 200},
          {"facetedview5", "facetedview/documents?facet=last:0&facet=last:1&facet=last:2", 200},
          {"facetedview6", "facetedview/documents?facet=id:100", 200},
          {"facetedview7", "facetedview/documents?facet=last:0&facet=id:100", 200},
          {"facetedview8", "facetedview/documents?facet=last:2&facet=id:100", 200},
          {"facetedview9", "facetedview/documents?facet=last:0&facet=id:101", 200},
          {"fieldview1", "fieldview/documents", 200},
          {"fieldview2", "fieldview/documents?field=id", 200},
          {"defaultlimit3", "defaultlimit/documents", 200},
          {"dependencyerror2", "dependencyerror/documents/100", 200},
          {"dependencyerror3", "dependencyerror/documents/100?process", 500},
          {"viewdeperror2", "viewdeperror/documents/100", 200},
          {"viewdeperror3", "viewdeperror/documents/100?process", 500},
          {"xslt1", "xslt/documents/100?metadataLevel=full", 200},
          {"viewsrc5", "viewsrc/documents/111", 404},
          {"localviewsrc5", "localviewsrc/documents/111", 404},
          {"viewsrc6", "viewsrc/documents/122", 404},
          {"localviewsrc6", "localviewsrc/documents/122", 404},
          {"unprocdeps2", "unprocdeps/documents/100?process", 404},
          {"savedeletedsrc4", "savedeletedsrc/documents/101?process", 410},
          {"facetedview10", "facetedview/documents/101", 200},
          {"facetedview11", "facetedview/documents/102", 410},
          {"facetviewdoclimit1", "facetviewdoclimit/documents", 200},
          {"unprocessdeps2", "unprocessdeps/documents/100", 200},

          // pause
          {"pause", null, 2000},
          {"parent6", "parent/documents", 200},
          {"facetview3", "facetview/documents?limit=1", 200},
          {"localfacetview3", "localfacetview/documents?limit=1", 200},
          {"facetview4", "facetview/documents?field=fake&facet=last:1", 200},
          {"localfacetview4", "localfacetview/documents?field=fake&facet=last:1", 200},
          {"limitedview1", "limitedview/documents", 200},
          {"limitedview2", "limitedview/documents/100", 200},
          {"limitedview3", "limitedview/documents/101", 404},
          {"limitedview4", "limitedview/documents?facet=last:0", 200},
          {"limitedview5", "limitedview/documents?facet=last:1", 200},
          {"limitedview6", "limitedview/documents?field=obj", 200},
          {"limitedview7", "limitedview/documents?field=bad", 200},
          {"limitedview8", "limitedview/documents?field=sub.maybe", 200},
          {"limitedview9", "limitedview/documents?field=obj&field=bad", 200},
          {"limitedview10", "limitedview/documents/110", 404},
          {"error2", "error/documents?status=error", 200},
          {"savedeletedtrue", "savedeletedtrue/documents", 200},
          {"savedeletedfalse", "savedeletedfalse/documents", 200},

          // tests dependent on time schedules
          {"align", null, 10000},
          {"pause", null, 8000},
          // reset just prior
          {"reset", null, 0},
          {"align", null, 10000},
          {"pause", null, 1000},
          // redetermine primaries
          {"align", null, 10000},
          {"pause", null, 1000},
          // allow harvesters to run
          {"align", null, 10000},
          {"pause", null, 1000},
          {"quota01", "quota/documents/100", 404},
          {"quota11", "quota/documents/101", 404},
          {"quota21", "quota/documents/102", 404},
          {"suspend01", "suspend/documents/100", 404},
          {"suspend02", "suspend/documents/101", 404},
          {"onoff1", "onoff/documents/100", 404},
          {"batch10", "batch/documents/100", 404},
          {"batch11", "batch/documents/101", 404},
          {"batch12", "batch/documents/102", 404},
          {"remove1", "remove/documents/200", 404},
          {"harvest", "harvest/documents/1000000000", 200},
          {"signal", "signal/documents/1000000000", 200},
          {"reprocess1", "reprocess/documents/300", 404},
          {"ejbharvest", "ejbharvest/documents?limit=1", 200},
          {"parentharvest1", "parentharvest/documents", 200},
          {"harvestunprocessed1", "harvestunprocessed/documents", 200},
          {"viewsrc7", "viewsrc/documents/150", 404},
          {"pause", null, 1000},
          {"quota02", "quota/documents/100", 200},
          {"quota12", "quota/documents/101", 200},
          {"quota22", "quota/documents/102", 404},
          {"suspend11", "suspend/documents/100", 200},
          {"suspend12", "suspend/documents/101", 404},
          {"onoff2", "onoff/documents/100", 404},
          {"batch20", "batch/documents/100", 200},
          {"batch21", "batch/documents/101", 200},
          {"batch22", "batch/documents/102", 404},
          {"remove2", "remove/documents/200", 410},
          {"reprocess2", "reprocess/documents/300", 200},
          {"harvestunprocessed2", "harvestunprocessed/documents", 200},
          {"align", null, 5000},
          {"pause", null, 1000},
          {"quota23", "quota/documents/102", 200},
          {"suspend22", "suspend/documents/101", 200},
          {"onoff3", "onoff/documents/100", 200},
          {"remove3", "remove/documents/200", 404},
          {"reprocess3", "reprocess/documents/300", 410},
          {"parentharvest2", "parentharvest/documents", 200},
          {"harvestunprocessed3", "harvestunprocessed/documents", 200},
          {"pause", null, 1000},
          {"batch32", "batch/documents/102", 200},
        };

    return Arrays.asList(params);
  }
}
