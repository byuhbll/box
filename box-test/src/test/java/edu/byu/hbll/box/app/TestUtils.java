/** */
package edu.byu.hbll.box.app;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import org.bson.Document;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import edu.byu.hbll.config.YamlLoader;

/** @author Charles Draper */
public class TestUtils {

  private static final Client client = ClientBuilder.newClient();
  private static final ObjectMapper mapper = new ObjectMapper();

  public static void clearTestMongoDatabase() throws JsonProcessingException, IOException {

    JsonNode config = new YamlLoader().load(Paths.get("src/test/resources/box.yml"));
    JsonNode params = config.path("box").path("db").path("params");
    MongoClientURI uri = new MongoClientURI(params.path("uri").asText("mongodb://localhost/"));
    String database = params.path("database").asText(null);

    // clear out the old data
    try (MongoClient mongo = new MongoClient(uri)) {
      MongoDatabase db = mongo.getDatabase(database);

      for (String collectionName : db.listCollectionNames()) {
        db.getCollection(collectionName).deleteMany(new Document());
      }
    }
  }

  public static void align(long unitInMillis) {
    long now = System.currentTimeMillis();
    long next = now / unitInMillis * unitInMillis + unitInMillis;

    try {
      Thread.sleep(next - now);
    } catch (InterruptedException e) {
      return;
    }
  }

  public static void writeFile(Path file, JsonNode document) throws Exception {
    String content = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(document);
    file.toFile().getParentFile().mkdirs();
    Files.write(file, content.getBytes("utf8"));
  }

  public static Response get(String subUri) {
    URI uri = URI.create("http://localhost:8080/box-test/" + subUri);
    return client.target(uri).request().get();
  }
  
  public static void reset() {
    client.target("http://localhost:8080/box-test/reset").request().get();
  }
}
