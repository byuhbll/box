/** */
package edu.byu.hbll.box.client;

import static org.junit.Assert.assertEquals;
import java.util.HashSet;
import java.util.Set;
import org.junit.Test;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.app.TestUtils;

/** @author Charles Draper */
public class BoxClientIT {

  /**
   * Test method for {@link edu.byu.hbll.box.client.BoxClient#stream()} and {@link
   * edu.byu.hbll.box.client.BoxClient#stream(edu.byu.hbll.box.BoxQuery)}.
   */
  @Test
  public void testStream() {
    TestUtils.reset();
    BoxClient client = new BoxClient("http://localhost:8080/box-test/box");

    Set<String> expectedIds = new HashSet<>();

    for (int i = 0; i < 101; i++) {
      TestUtils.get("box/documents/" + i + "?process");
      expectedIds.add(i + "");
    }

    Set<String> actualIds = new HashSet<>();

    for (BoxDocument doc : client.stream()) {
      actualIds.add(doc.getId());
    }

    assertEquals(expectedIds, actualIds);

    actualIds = new HashSet<>();

    for (BoxDocument doc : client.stream(new BoxQuery().setLimit(3))) {
      actualIds.add(doc.getId());
    }

    assertEquals(expectedIds, actualIds);
  }
}
