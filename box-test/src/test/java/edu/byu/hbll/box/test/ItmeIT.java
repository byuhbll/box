/** */
package edu.byu.hbll.box.test;

import org.junit.runner.RunWith;
import edu.byu.hbll.itme.Itme;
import edu.byu.hbll.itme.ItmeConfig;

/** @author Charles Draper */
@RunWith(Itme.class)
@ItmeConfig("src/test/resources/itme.yml")
public class ItmeIT {}
