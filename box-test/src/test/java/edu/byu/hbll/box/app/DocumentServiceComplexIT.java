/** */
package edu.byu.hbll.box.app;

import static org.junit.Assert.assertEquals;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import org.junit.BeforeClass;
import org.junit.Test;
import com.fasterxml.jackson.core.JsonProcessingException;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.QueryResult;
import edu.byu.hbll.box.client.BoxClient;

/** @author Charles Draper */
public class DocumentServiceComplexIT {

  private static final Client client = ClientBuilder.newClient();

  private final String postDocument =
      "{\n" + "  \"id\": \"testing123\",\n" + "  \"data\": \"testData\"\n" + "}";

  @BeforeClass
  public static void beforeClass() throws Exception {
    TestUtils.clearTestMongoDatabase();
  }

  @Test
  public void testPostDocument() throws JsonProcessingException, IOException {
    Response response =
        client
            .target("http://localhost:8080/box-test/box/documents")
            .request()
            .post(Entity.json(postDocument));
    assertEquals(200, response.getStatus());
  }

  /**
   * Tests a facet view to make sure that we can harvest from beginning to end and see all facet
   * documents and that it will actually finish.
   */
  @Test
  public void testHarvestFacetView() {

    List<String> ids = new ArrayList<>();

    for (int i = 0; i < 26; i++) {
      ids.add(((char) ('a' + i)) + "");
    }

    // add a document at the end that will be picked up by the first document's `last` facet
    // Note: the problem was that the cursor was advancing to this last document and skipping
    // all others because it got picked up by the first document.
    ids.add("bada");

    BoxQuery idQuery = new BoxQuery(ids).setProcess();
    new BoxClient("http://localhost:8080/box-test/viewsrc").find(idQuery);

    BoxClient client = new BoxClient("http://localhost:8080/box-test/facetview");
    BoxQuery query = new BoxQuery().setLimit(1);

    int totalQueries = 0;
    QueryResult result;

    do {
      result = client.find(query);
      query.setCursor(result.getNextCursor());
      totalQueries++;

      int expectedDocumentsSize = 0;

      if (totalQueries == 1 || totalQueries == 27) {
        expectedDocumentsSize = 2;
      } else if (totalQueries < 27) {
        expectedDocumentsSize = 1;
      }

      if (!result.isEmpty()) {
        assertEquals(expectedDocumentsSize, result.get(0).getDocument().path("documents").size());
      }
    } while (!result.isEmpty());

    assertEquals(28, totalQueries);
  }
}
