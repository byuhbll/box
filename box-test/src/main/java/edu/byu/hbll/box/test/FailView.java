/**
 * 
 */
package edu.byu.hbll.box.test;

import edu.byu.hbll.box.HarvestContext;
import edu.byu.hbll.box.HarvestResult;
import edu.byu.hbll.box.Harvester;
import edu.byu.hbll.box.ProcessBatch;
import edu.byu.hbll.box.ProcessResult;
import edu.byu.hbll.box.Processor;

/**
 * @author Charles Draper
 *
 */
public class FailView implements Processor, Harvester {
  
  public FailView() {
    throw new RuntimeException("Failed to construct");
  }

  @Override
  public HarvestResult harvest(HarvestContext context) {
    return new HarvestResult();
  }

  @Override
  public ProcessResult process(ProcessBatch batch) {
    return new ProcessResult();
  }

}
