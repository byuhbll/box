/** */
package edu.byu.hbll.box.test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import org.bson.Document;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import edu.byu.hbll.box.Box;
import edu.byu.hbll.box.BoxConfigurable;
import edu.byu.hbll.box.Source;

/** @author Charles Draper */
@Path("reset")
public class Reset {

  @Inject private Box box;

  @Inject private Configuration configuration;

  @GET
  public String reset() {
    clearDatabase();
    resetCounters();
    return "{\"message\":\"Done at " + LocalDateTime.now() + "\"}";
  }

  private void resetCounters() {
    List<BoxConfigurable> objects = new ArrayList<>();

    for (Source source : box.getSources()) {
      objects.add(source.getProcessor());
      objects.add(source.getHarvester());
      objects.add(source.getDb());
      objects.add(source.getCursorDb());
      objects.addAll(source.getOthers());
    }

    for (BoxConfigurable object : objects) {
      if (object instanceof Resettable) {
        ((Resettable) object).reset();
      }
    }
  }

  private void clearDatabase() {
    ObjectNode config = configuration.getConfig();

    JsonNode params = config.path("box").path("db").path("params");
    MongoClientURI uri = new MongoClientURI(params.path("uri").asText("mongodb://localhost/"));
    String database = params.path("database").asText(null);

    // clear out the old data
    try (MongoClient mongo = new MongoClient(uri)) {
      MongoDatabase db = mongo.getDatabase(database);

      for (String collectionName : db.listCollectionNames()) {
        db.getCollection(collectionName).deleteMany(new Document());
      }
    }
  }
}
