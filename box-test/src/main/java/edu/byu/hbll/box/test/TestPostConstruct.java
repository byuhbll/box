/** */
package edu.byu.hbll.box.test;

import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.HarvestContext;
import edu.byu.hbll.box.HarvestResult;
import edu.byu.hbll.box.Harvester;
import edu.byu.hbll.box.InitConfig;
import edu.byu.hbll.box.ProcessBatch;
import edu.byu.hbll.box.ProcessResult;
import edu.byu.hbll.box.Processor;
import edu.byu.hbll.box.QueryResult;
import edu.byu.hbll.box.ReadOnlyDatabase;
import edu.byu.hbll.box.ObjectType;

/** @author Charles Draper */
public class TestPostConstruct implements Processor, Harvester, ReadOnlyDatabase {

  @Override
  public void postConstruct(InitConfig config) {

    if (config == null) {
      throw new IllegalArgumentException("config is null");
    }

    if (config.getParams() == null) {
      throw new IllegalArgumentException("params is null");
    }

    ObjectNode params = config.getParams();

    if (config.getSource() == null) {
      throw new IllegalArgumentException("source is null");
    }

    if (config.getSourceName() == null) {
      throw new IllegalArgumentException("sourceName is null");
    }

    if (!config.getSourceName().equals(params.path("sourceName").asText())) {
      throw new IllegalArgumentException("sourceName is not " + config.getSourceName());
    }

    if (config.getObjectType() == null) {
      throw new IllegalArgumentException("objectType is null");
    }

    ObjectType objectType =
        ObjectType.valueOf(params.path("objectType").asText().toUpperCase());

    switch (objectType) {
      case CURSOR_DATABASE:
        if (!config.isCursorDatabase()) {
          throw new IllegalArgumentException("objectType is not " + objectType);
        }

        break;
      case BOX_DATABASE:
        if (!config.isBoxDatabase()) {
          throw new IllegalArgumentException("objectType is not " + objectType);
        }
        break;
      case HARVESTER:
        if (!config.isHarvester()) {
          throw new IllegalArgumentException("objectType is not " + objectType);
        }
        break;
      case OTHER:
        if (!config.isOther()) {
          throw new IllegalArgumentException("objectType is not " + objectType);
        }
        break;
      case PROCESSOR:
        if (!config.isProcessor()) {
          throw new IllegalArgumentException("objectType is not " + objectType);
        }
        break;
    }
  }

  @Override
  public HarvestResult harvest(HarvestContext context) {
    return null;
  }

  @Override
  public ProcessResult process(ProcessBatch batch) {
    return null;
  }

  @Override
  public QueryResult find(BoxQuery query) {
    return null;
  }
}
