/** */
package edu.byu.hbll.box.test;

import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.ProcessContext;
import edu.byu.hbll.box.ProcessResult;
import edu.byu.hbll.box.Processor;
import edu.byu.hbll.box.SingleProcessor;

/** A {@link Processor} for testing box. */
public class TestUnprocessor implements SingleProcessor {

  @Override
  public ProcessResult process(ProcessContext context) {
    ProcessResult result = new ProcessResult();
    BoxDocument doc = new BoxDocument(context.getId());
    ObjectNode document = doc.getDocument();

    for (BoxDocument dep : context.getDependencies("one")) {
      document.withArray("one").add(dep.getId());
    }

    int numDeps = document.path("one").size();

    switch (numDeps) {
      case 2:
      case 1:
        doc.addDependency("one", "2");
      case 0:
        doc.addDependency("one", "1");
    }

    if (numDeps == 0 || numDeps == 2) {
      doc.setAsReady();
    } else {
      doc.setAsUnprocessed();
    }

    result.add(doc);
    return result;
  }
}
