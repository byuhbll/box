/** */
package edu.byu.hbll.box.test;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Necessary part of jax-rs. If this wasn't here we'd have to create an entry in the web.xml
 *
 * @author Charles Draper
 */
@ApplicationPath("")
public class MyApplication extends Application {}
