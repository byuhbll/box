/** */
package edu.byu.hbll.box.test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.Box;
import edu.byu.hbll.json.YamlLoader;

/** */
@Singleton
@Startup
public class Configuration {

  /** */
  static final Logger logger = LoggerFactory.getLogger(Configuration.class);

  /** */
  public static final String CONFIG_FILE_PROPERTY = "edu.byu.hbll.box.config";

  /** */
  @Inject private Box box;

  private ObjectNode config;

  /** Initializer method. */
  @PostConstruct
  public void init() {
    try {
      String[] configs = System.getProperty(CONFIG_FILE_PROPERTY).split("\\s+");
      Path[] paths =
          Arrays.asList(configs)
              .stream()
              .map(c -> Paths.get(c))
              .collect(Collectors.toList())
              .toArray(new Path[configs.length]);

      config = (ObjectNode) new YamlLoader().load(paths);
    } catch (Exception e) {
      logger.error(e.toString(), e);
    }

    try {
      box.initialize(config.path("box"));
    } catch (Exception e) {
      logger.error(e.toString(), e);
    }
  }

  /** @return the config */
  public ObjectNode getConfig() {
    return config;
  }

  /** @param config the config to set */
  public void setConfig(ObjectNode config) {
    this.config = config;
  }
}
