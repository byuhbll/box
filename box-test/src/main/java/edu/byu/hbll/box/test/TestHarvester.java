/** */
package edu.byu.hbll.box.test;

import java.util.Arrays;
import javax.ejb.Singleton;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.HarvestContext;
import edu.byu.hbll.box.HarvestResult;
import edu.byu.hbll.box.Harvester;
import edu.byu.hbll.box.ProcessBatch;

/** */
@Singleton
public class TestHarvester implements Harvester {

  private TestProcessor processor = new TestProcessor();

  public long baseId = 1000000000;
  public int max = 100;
  public int limit = 10;
  public boolean signal = false;
  public boolean more = true;
  public boolean orphans = false;

  private boolean orphaned;

  @Override
  public HarvestResult harvest(HarvestContext context) {
    HarvestResult result = new HarvestResult();

    int total = context.getCursor().path("processed").asInt(0);

    if (total == 0) {
      orphaned = !orphaned;
    }

    for (int i = 0; i < limit && total < max; i++) {
      if (orphans && orphaned) {
        if (i % 2 == 1) {
          total++;
          continue;
        }
      }

      String id = (total + baseId) + "";

      if (signal) {
        result.addDocuments(new BoxDocument(id));
      } else {
        result.addDocuments(processor.process(ProcessBatch.ofIds(Arrays.asList(id))));
      }

      result.setMore(this.more && true);
      total++;
    }

    result.setCursor(context.getCursor().put("processed", total));

    return result;
  }
}
