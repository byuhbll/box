/** */
package edu.byu.hbll.box.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxDocument.Status;
import edu.byu.hbll.box.ProcessContext;
import edu.byu.hbll.box.ProcessResult;
import edu.byu.hbll.box.Processor;
import edu.byu.hbll.box.SingleProcessor;

/** A {@link Processor} for testing box. */
public class TestProcessor implements SingleProcessor, Resettable {

  private static ObjectMapper mapper = new ObjectMapper();

  /** Counts number of times document processed. */
  private ConcurrentHashMap<String, Integer> counter = new ConcurrentHashMap<>();

  /** Additional fields to add to the json document. */
  private ObjectNode document = mapper.createObjectNode();

  /** Toggle or rotate through theses resulting statuses each time processed. */
  private List<Status> statuses = Arrays.asList(Status.READY);

  /** Other sources that are dependencies. */
  private List<String> dependencies = new ArrayList<>();

  /** Throws an error during processing. */
  private boolean throwError;

  /** Forces json document to be empty. */
  private boolean empty;

  /** Whether or not this represents a parent that splits into children */
  private boolean parent;

  private String sourceName;

  private boolean includeCount;

  @Override
  public ProcessResult process(ProcessContext context) {

    if (throwError) {
      throw new RuntimeException();
    }

    ProcessResult result = new ProcessResult();

    String id = context.getId();

    int count = incrementCountAndGet(id);
    Status status = statuses.get((count - 1) % statuses.size());

    if (parent) {
      result.startGroup(id);

      // creates children whose ids shift back and forth each time leaving one to be deleted
      for (int i = 0; i < 3; i++) {
        result.addDocument(
            document(id + "." + (i + (count % 2)), Status.READY, context).setGroupId(id));
      }

      result.endGroup(id);
    }

    result.addDocument(document(id, status, context));

    return result;
  }

  /**
   * @param id
   * @param status
   * @param context
   * @return
   */
  public BoxDocument document(String id, Status status, ProcessContext context) {
    ObjectNode document = mapper.createObjectNode();

    if (!empty) {
      document.put("id", id);
      
      if(includeCount) {
        document.put("count", getCount(id));
      }
      
      document.setAll(this.document);
    }

    BoxDocument boxDocument = new BoxDocument(id, document);
    boxDocument.setStatus(status);
    boxDocument.addFacet("id", id);
    boxDocument.addFacet("first", id.substring(0, 1));
    boxDocument.addFacet("last", id.substring(id.length() - 1, id.length()));

    for (String dependencySource : dependencies) {
      BoxDocument dependency = context.getDependency(dependencySource, id);

      if (dependency != null && dependency.isProcessed() && !dependency.isDeleted()) {
        boxDocument
            .getDocument()
            .withArray("dependencies")
            .addObject()
            .put("source", dependencySource)
            .set("id", dependency.getDocument().path("id"));
        boxDocument.addFacet(dependencySource, dependency.getDocument().path("id").asText());
      }

      boxDocument.addDependency(dependencySource, id);
    }

    return boxDocument;
  }

  @Override
  public void reset() {
    counter.clear();
  }

  /**
   * @param id
   * @return
   */
  private int incrementCountAndGet(String id) {
    return counter.compute(id, (k, v) -> v == null ? 1 : v + 1);
  }

  /**
   * @param id
   * @return
   */
  private int getCount(String id) {
    return counter.getOrDefault(id, 0);
  }

  /** @return the document */
  public String getDocument() {
    return document.toString();
  }

  /**
   * @param document the document to set
   * @throws Exception
   */
  public void setDocument(String document) throws Exception {
    this.document = (ObjectNode) mapper.readTree(document);
  }

  /** @return the throwError */
  public boolean isThrowError() {
    return throwError;
  }

  /** @param throwError the throwError to set */
  public void setThrowError(boolean throwError) {
    this.throwError = throwError;
  }

  /** @return the statuses */
  public List<Status> getStatuses() {
    return statuses;
  }

  /** @param statuses the statuses to set */
  public void setStatuses(List<String> statuses) {
    this.statuses =
        statuses.stream().map(s -> Status.valueOf(s.toUpperCase())).collect(Collectors.toList());
  }

  /** @return the dependencies */
  public List<String> getDependencies() {
    return dependencies;
  }

  /** @param dependencies the dependencies to set */
  public void setDependencies(List<String> dependencies) {
    this.dependencies = dependencies;
  }

  /** @return the empty */
  public boolean isEmpty() {
    return empty;
  }

  /** @param empty the empty to set */
  public void setEmpty(boolean empty) {
    this.empty = empty;
  }

  /** @return the parent */
  public boolean isParent() {
    return parent;
  }

  /** @param parent the parent to set */
  public void setParent(boolean parent) {
    this.parent = parent;
  }

  /** @return the sourceName */
  public String getSourceName() {
    return sourceName;
  }

  /** @param sourceName the sourceName to set */
  public void setSourceName(String sourceName) {
    this.sourceName = sourceName;
  }

  /** @return the includeCount */
  public boolean isIncludeCount() {
    return includeCount;
  }

  /** @param includeCount the includeCount to set */
  public void setIncludeCount(boolean includeCount) {
    this.includeCount = includeCount;
  }
}
