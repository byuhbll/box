/** */
package edu.byu.hbll.box.test;

/** @author Charles Draper */
public interface Resettable {
  void reset();
}
