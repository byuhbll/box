/** */
package edu.byu.hbll.box.test;

import javax.ejb.Stateless;
import edu.byu.hbll.box.Processor;

/** A {@link Processor} for testing box. */
@Stateless
public class TestEjbProcessor extends TestProcessor {}
