/** */
package edu.byu.hbll.box;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.Collection;
import org.junit.Test;

/** @author Charles Draper */
public class BoxDocumentTest {

  /** Test method for {@link edu.byu.hbll.box.BoxDocument#matches(java.util.Collection)}. */
  @Test
  public void testMatches() {
    BoxDocument doc = new BoxDocument();
    Collection<Facet> facets = new ArrayList<>();

    assertTrue(doc.matches(facets));

    doc.addFacet("1", "1");

    assertTrue(doc.matches(facets));

    doc = new BoxDocument();
    facets.add(new Facet("1", "1"));

    assertFalse(doc.matches(facets));

    doc.addFacet("1", "1");
    facets.add(new Facet("1", "2"));

    assertTrue(doc.matches(facets));

    facets.add(new Facet("2", "1"));

    assertFalse(doc.matches(facets));

    doc.addFacet("2", "2");

    assertFalse(doc.matches(facets));

    doc.addFacet("2", "1");

    assertTrue(doc.matches(facets));
  }
}
