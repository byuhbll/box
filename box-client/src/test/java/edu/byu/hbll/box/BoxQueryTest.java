/** */
package edu.byu.hbll.box;

import static org.junit.Assert.*;
import org.junit.Test;

/** @author cfd2 */
public class BoxQueryTest {

  BoxQuery query = new BoxQuery();

  /** Test method for {@link edu.byu.hbll.box.BoxQuery#isId()}. */
  @Test
  public void testIsId() {
    assertTrue(query.addId("").isId());
  }

  /** Test method for {@link edu.byu.hbll.box.BoxQuery#isHarvest()}. */
  @Test
  public void testIsHarvest() {
    assertTrue(query.isHarvest());
  }
}
