/** */
package edu.byu.hbll.box.client.internal.util;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

/** @author Charles Draper */
@RunWith(MockitoJUnitRunner.class)
public class BasicAuthenticationtFilterTest {

  @Mock ClientRequestContext requestContext;

  MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();

  @Before
  public void setUp() throws Exception {
    when(requestContext.getHeaders()).thenReturn(headers);
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.box.internal.util.BasicAuthenticationtFilter#filter(javax.ws.rs.client.ClientRequestContext)}.
   */
  @Test
  public void testFilter() {
    BasicAuthenticationtFilter filter = new BasicAuthenticationtFilter("username", "password");
    filter.filter(requestContext);
    assertEquals("Basic dXNlcm5hbWU6cGFzc3dvcmQ=", headers.get("Authorization").get(0));
  }
}
