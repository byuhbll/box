/** */
package edu.byu.hbll.box;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import org.junit.Test;

/** @author Charles Draper */
public class FacetTest {

  /** Test method for {@link edu.byu.hbll.box.Facet#group(java.util.Collection)}. */
  @Test
  public void testGroup() {
    Collection<Facet> facets = new ArrayList<>();
    Map<String, Set<Facet>> facetMap = Facet.group(facets);

    assertTrue(facetMap.isEmpty());

    facets.add(new Facet("1", "1"));
    facets.add(new Facet("1", "2"));
    facets.add(new Facet("2", "1"));

    facetMap = Facet.group(facets);

    assertTrue(facetMap.get("1").contains(new Facet("1", "1")));
    assertTrue(facetMap.get("1").contains(new Facet("1", "2")));
    assertTrue(facetMap.get("2").contains(new Facet("2", "1")));
    assertFalse(facetMap.get("2").contains(new Facet("2", "2")));
  }
}
