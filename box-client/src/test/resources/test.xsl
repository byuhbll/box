<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet
  version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  
  <xsl:output
    indent="yes"
    method="xml"
    encoding="UTF-8" />
  
  <xsl:template match="source">
    <xsl:apply-templates select="document" />
  </xsl:template>
  
  <xsl:template match="document">
    <result>
      <document>
        <xmlid><xsl:text>new</xsl:text><xsl:value-of select="xmlid" /></xmlid>
        <xsl:for-each select="/source/dependency">
          <depid><xsl:value-of select="document/id" /></depid>
        </xsl:for-each>
      </document>
      <facet><name>facet1</name><value>value1</value></facet>
      <facet><name>facet2</name><value>value2</value></facet>
      <dependency><sourceName>dep</sourceName><id>1</id></dependency>
      <dependency><sourceName>dep</sourceName><id>2</id></dependency>
    </result>
  </xsl:template>

</xsl:stylesheet>
