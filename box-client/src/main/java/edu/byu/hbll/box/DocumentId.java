/** */
package edu.byu.hbll.box;

import java.io.Serializable;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * An immutable representation of a full document id which includes the source name of the document
 * and its id.
 */
public final class DocumentId implements Serializable {

  /** */
  private static final long serialVersionUID = 1L;

  /** The source name of the document. */
  @JsonProperty private String sourceName;

  /** The ID of the document. */
  @JsonProperty private String id;

  /** */
  @SuppressWarnings("unused")
  private DocumentId() {}

  /**
   * @param sourceName the source name of the document.
   * @param id the id of the document.
   */
  public DocumentId(String sourceName, String id) {
    this.sourceName = sourceName;
    this.id = id;
  }

  /** @return the sourceName */
  public String getSourceName() {
    return sourceName;
  }

  /** @return the id */
  public String getId() {
    return id;
  }

  /** {@inheritDoc} */
  @Override
  public int hashCode() {
    return Objects.hash(sourceName, id);
  }

  /**
   * Checks equality of the source name and ID.
   *
   * @param o the object to be compared.
   * @return whether the given object is equal to this.
   */
  @Override
  public boolean equals(Object o) {

    if (this == o) {
      return true;
    }

    if (o == null) {
      return false;
    }

    if (!(o instanceof DocumentId)) {
      return false;
    }

    DocumentId other = (DocumentId) o;

    return Objects.equals(sourceName, other.sourceName) && Objects.equals(id, other.id);
  }

  /** @return sourceName.id */
  @Override
  public String toString() {
    return sourceName + "." + id;
  }
}
