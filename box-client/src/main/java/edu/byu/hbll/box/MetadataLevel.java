/** */
package edu.byu.hbll.box;

/**
 * What level metadata a client should return. Ordered from lowest to highest.
 *
 * @author Charles Draper
 */
public enum MetadataLevel {

  /** Do not return any metadata. */
  NONE,

  /** Only return the core metadata (ie, id and status). */
  CORE,

  /** Return all metadata. */
  FULL
}
