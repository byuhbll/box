/** */
package edu.byu.hbll.box.client;

import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.UriBuilder;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.QueryResult;
import edu.byu.hbll.box.client.internal.util.BasicAuthenticationtFilter;
import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;

/**
 * A client for communicating with Box through it's web api.
 *
 * @author Charles Draper
 */
public class BoxClient {

  private static final UncheckedObjectMapper mapper = ObjectMapperFactory.newUnchecked();

  private String uri;
  private Client client;

  /** @param uri the base uri of the box source (eg, http://localhost:8080/app/box) */
  public BoxClient(String uri) {
    this(uri, ClientBuilder.newClient());
  }

  /**
   * @param uri the base uri of the box source (eg, http://localhost:8080/app/box)
   * @param username username for basic auth
   * @param password password for basic auth
   */
  public BoxClient(String uri, String username, String password) {
    this(
        uri,
        ClientBuilder.newBuilder()
            .register(new BasicAuthenticationtFilter(username, password))
            .build());
  }

  /**
   * @param uri the base uri of the box source (eg, http://localhost:8080/app/box)
   * @param client jax-rs client to use
   */
  public BoxClient(String uri, Client client) {
    this.uri = uri;
    this.client = client;
  }

  /**
   * Finds documents according to the given query.
   *
   * @param query the query
   * @return the results
   */
  public QueryResult find(BoxQuery query) {
    return find(query, false);
  }

  /**
   * Finds documents according to the given query and will page to the end if desired.
   *
   * @param query the query
   * @param pageToEnd whether or not to page through to the end
   * @return the results
   */
  public QueryResult find(BoxQuery query, boolean pageToEnd) {
    return find(query, pageToEnd ? Integer.MAX_VALUE : -1);
  }

  /**
   * Finds documents according to the given query and will page further if desired.
   *
   * @param query the query
   * @param pageToLimit if number of documents found is less than pageToLimit, continue paging until
   *     pageToLimit documents is found. If the number of documents found is greater than
   *     pageToLimit, then the document list will be truncated. A negative value causes pageToLimit
   *     to be completely ignored and so the limit on the query itself is honored.
   * @return the results
   */
  public QueryResult find(BoxQuery query, int pageToLimit) {

    QueryResult result = new QueryResult();
    List<BoxDocument> documents = new ArrayList<>();
    long nextCursor = query.getCursor();
    boolean more = true;

    while (more) {

      UriBuilder builder = UriBuilder.fromUri(uri).path("documents");

      if (query.getIds().isEmpty()) {
        builder.queryParam("cursor", nextCursor);
        builder.queryParam("facet", query.getFacets().toArray());

        if (query.getLimit() >= 0) {
          builder.queryParam("limit", query.getLimit());
        }

        builder.queryParam("status", query.getStatuses().toArray());
      } else {
        builder.queryParam("id", query.getIds().toArray()).build();
        builder.queryParam("process", query.isProcess());
        builder.queryParam("wait", query.isWait());
      }

      builder.queryParam("metadataLevel", query.getMetadataLevel());
      builder.queryParam("field", query.getFields().toArray());

      URI uri = builder.build();

      String textResponse = client.target(uri).request().get(String.class);

      JsonNode response = mapper.readTree(textResponse);

      for (JsonNode responseDocument : response.path("documents")) {
        BoxDocument boxDocument = BoxDocument.fromJson((ObjectNode) responseDocument);
        documents.add(boxDocument);
      }

      more = documents.size() < pageToLimit && response.path("documents").size() != 0;
      nextCursor = response.path("nextCursor").asLong();
    }
    
    if(pageToLimit >= 0 && documents.size() > pageToLimit) {
      documents = documents.subList(0, pageToLimit);
    }

    result.addAll(documents);
    result.setNextCursor(nextCursor);

    return result;
  }

  /**
   * Finds all processed documents and streams them back one at a time until the end is reached.
   *
   * <p>Same as BoxClient.stream(new BoxQuery());
   *
   * @return the results
   */
  public Iterable<BoxDocument> stream() {
    return stream(new BoxQuery());
  }

  /**
   * Finds documents according to the given query and streams them back one at a time until the end
   * is reached. The limit field in the query sets the internal batch size, but does not determine
   * the maximum number of documents returned.
   *
   * @param query the query
   * @return the results
   */
  public Iterable<BoxDocument> stream(BoxQuery query) {
    return new BoxIterable(query);
  }

  /** @return the uri */
  public String getUri() {
    return uri;
  }

  /** @return the client */
  public Client getClient() {
    return client;
  }

  /**
   * Allows for streaming of results.
   *
   * @author Charles Draper
   */
  private class BoxIterable implements Iterable<BoxDocument> {

    private BoxQuery query;

    private BoxIterable(BoxQuery query) {
      this.query = new BoxQuery(query);
    }

    @Override
    public Iterator<BoxDocument> iterator() {
      return new BoxIterator(query);
    }
  }

  /**
   * Allows for streaming of results.
   *
   * @author Charles Draper
   */
  private class BoxIterator implements Iterator<BoxDocument> {

    private BoxQuery query;
    private QueryResult result;
    private Iterator<BoxDocument> it;
    private BoxDocument next;

    private BoxIterator(BoxQuery query) {
      // make sure to copy the query because we will modify it
      this.query = new BoxQuery(query);
    }

    @Override
    public boolean hasNext() {
      init();
      return next != null;
    }

    @Override
    public BoxDocument next() {
      init();

      if (next == null) {
        throw new NoSuchElementException();
      }

      BoxDocument thisNext = next;
      findNext();

      return thisNext;
    }

    /** Run initial query if not run yet. */
    private void init() {
      if (result == null) {
        query();
        findNext();
      }
    }

    /** Finds and sets the next document. Sets document to null if end reached. */
    private void findNext() {
      if (it.hasNext()) {
        next = it.next();
      } else if (result.isEmpty() || query.isId()) {
        next = null;
      } else {
        query();
        findNext();
      }
    }

    /**
     * Runs the current query, sets the result, sets the document iterator, and prepares the next
     * query.
     */
    private void query() {
      result = find(query);
      it = result.iterator();
      query.setCursor(result.getNextCursor());
    }
  }
}
