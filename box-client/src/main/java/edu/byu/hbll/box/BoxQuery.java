/** */
package edu.byu.hbll.box;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Query for requesting documents from Box.
 *
 * <p>There are two different ways of querying box. The first is to request documents by id. When
 * requesting by id, the limit is ignored. There will be a 1:1 representation of documents according
 * to the ids. If a document is not processed for a given id, a placeholder will still appear for
 * that id. Documents are always returned in the same order as the requested ids.
 *
 * <p>The second query type harvests documents by requesting and filtering all documents according
 * to the cursor, facets and limit instructions. Documents are always returned in cursor order. Only
 * processed documents are returned. For this query type, the ids, process and wait clauses are
 * ignored.
 *
 * <p>If there are ids in the query, then the id query type is used.
 */
public class BoxQuery {

  private List<String> ids = new ArrayList<>();

  private boolean process = false;

  private boolean wait = false;

  private long cursor;

  private long limit = -1;

  private Set<BoxDocument.Status> statuses =
      new LinkedHashSet<>(Arrays.asList(BoxDocument.Status.READY, BoxDocument.Status.DELETED));

  private Set<String> fields = new LinkedHashSet<>();

  private Set<Facet> facets = new LinkedHashSet<>();

  private MetadataLevel metadataLevel = MetadataLevel.FULL;

  private boolean metadataOnly = false;

  /** */
  public BoxQuery() {}

  /** Creates a new query with the given ids. */
  public BoxQuery(String... ids) {
    this.ids.addAll(Arrays.asList(ids));
  }

  /** Creates a new query with the given ids. */
  public BoxQuery(Collection<String> ids) {
    this.ids.addAll(ids);
  }

  /**
   * Copy constructor.
   *
   * @param query query to copy
   */
  public BoxQuery(BoxQuery query) {
    this.ids.addAll(query.ids);
    this.process = query.process;
    this.wait = query.wait;
    this.cursor = query.cursor;
    this.limit = query.limit;
    this.statuses.clear();
    this.statuses.addAll(query.getStatuses());
    this.metadataLevel = query.metadataLevel;
    this.fields.addAll(query.fields);
    this.facets.addAll(query.facets);
    this.metadataOnly = query.metadataOnly;
  }

  /**
   * Adds an id to this query.
   *
   * @param id
   * @return this
   */
  public BoxQuery addId(String id) {
    return addIds(Arrays.asList(id));
  }

  /**
   * Adds ids to this query.
   *
   * @param ids
   * @return this
   */
  public BoxQuery addIds(String... ids) {
    return addIds(Arrays.asList(ids));
  }

  /**
   * Adds ids to this query.
   *
   * @param ids
   * @return this
   */
  public BoxQuery addIds(Collection<String> ids) {
    this.ids.addAll(ids);
    return this;
  }

  /**
   * Whether or not this is an ID query rather than harvest query.
   *
   * @return whether or not is an ID query
   */
  public boolean isId() {
    return !isHarvest();
  }

  /**
   * Whether or not this is a harvest query rather than an ID query.
   *
   * @return whether or not is a harvest query
   */
  public boolean isHarvest() {
    return ids.isEmpty();
  }

  /**
   * Asks Box to (re)process the document(s) now and return the results. Not valid for harvest type
   * queries. Default false.
   *
   * @return this
   */
  public BoxQuery setProcess() {
    this.process = true;
    return this;
  }

  /**
   * Waits for Box to process the document before returning. The document is immediately returned if
   * it is already processed. Not valid for harvest type queries. Default false.
   *
   * @return this
   */
  public BoxQuery setWait() {
    this.wait = true;
    return this;
  }

  /**
   * Limit the document to only these fields. Dot notation.
   *
   * @param field
   * @return this
   */
  public BoxQuery addField(String field) {
    this.fields.addAll(Arrays.asList(field));
    return this;
  }

  /**
   * Limit the document to only these fields. Dot notation.
   *
   * @param fields
   * @return this
   */
  public BoxQuery addFields(String... fields) {
    this.fields.addAll(Arrays.asList(fields));
    return this;
  }

  /**
   * Limit the document to only these fields. Dot notation.
   *
   * @param fields
   * @return this
   */
  public BoxQuery addFields(Collection<String> fields) {
    this.fields.addAll(fields);
    return this;
  }

  /**
   * Filter by facets. Facets across facet groups are ANDed. Facets within a facet group are ORed.
   * Not valid for id type queries.
   *
   * @param facet
   * @return this
   */
  public BoxQuery addFacet(Facet facet) {
    this.facets.addAll(Arrays.asList(facet));
    return this;
  }

  /**
   * Filter by facets. Facets across facet groups are ANDed. Facets within a facet group are ORed.
   * Not valid for id type queries.
   *
   * @param facets
   * @return this
   */
  public BoxQuery addFacets(Facet... facets) {
    this.facets.addAll(Arrays.asList(facets));
    return this;
  }

  /**
   * Filter by facets. Facets across facet groups are ANDed. Facets within a facet group are ORed.
   * Not valid for id type queries.
   *
   * @param name name of the facet group
   * @param value value of the facet
   * @return this
   */
  public BoxQuery addFacet(String name, String value) {
    this.facets.addAll(Arrays.asList(new Facet(name, value)));
    return this;
  }

  /**
   * Filter by facets. Facets across facet groups are ANDed. Facets within a facet group are ORed.
   * Not valid for id type queries.
   *
   * @param facets
   * @return this
   */
  public BoxQuery addFacets(Collection<? extends Facet> facets) {
    this.facets.addAll(facets);
    return this;
  }

  /** @return the ids */
  public List<String> getIds() {
    return ids;
  }

  /**
   * @param ids the ids to set
   * @return this
   */
  public BoxQuery setIds(Collection<String> ids) {
    this.ids.clear();
    this.ids.addAll(ids);
    return this;
  }

  /** @return the process */
  public boolean isProcess() {
    return process;
  }

  /**
   * Whether or not to ask Box to (re)process the document now and return the result. Not valid for
   * harvest type queries. Default false.
   *
   * @param process the process to set
   * @return this
   */
  public BoxQuery setProcess(boolean process) {
    this.process = process;
    return this;
  }

  /** @return the wait */
  public boolean isWait() {
    return wait;
  }

  /**
   * Whether or not to wait for Box to process the document before returning. The document is
   * immediately returned if it is already processed. Not valid for harvest type queries. Default
   * false.
   *
   * @param wait the wait to set
   * @return this
   */
  public BoxQuery setWait(boolean wait) {
    this.wait = wait;
    return this;
  }

  /** @return the cursor */
  public long getCursor() {
    return cursor;
  }

  /**
   * Documents are order by cursor ascending so this is used for paging. Only documents greater than
   * or equal to this cursor will be returned. Not valid for id type queries.
   *
   * @param cursor the cursor to set
   * @return this
   */
  public BoxQuery setCursor(long cursor) {
    this.cursor = cursor;
    return this;
  }

  /** @return the limit */
  public long getLimit() {
    return limit;
  }

  /**
   * Limits the number of documents returned to no more than this number. This is effectively the
   * page size. Not valid for id type queries. A negative limit (or no limit) informs the server to
   * return its default number of documents. Usually 10. Default -1.
   *
   * @param limit the limit to set
   * @return this
   */
  public BoxQuery setLimit(long limit) {
    this.limit = limit;
    return this;
  }

  /** @return the statuses */
  public Set<BoxDocument.Status> getStatuses() {
    return statuses;
  }

  /**
   * Queries by these document statuses. Default [READY,DELETED].
   *
   * @param statuses the statuses to set
   */
  public BoxQuery setStatuses(BoxDocument.Status... statuses) {
    return setStatuses(Arrays.asList(statuses));
  }

  /**
   * Queries by these document statuses. Default [READY,DELETED].
   *
   * @param statuses the statuses to set
   */
  public BoxQuery setStatuses(Collection<BoxDocument.Status> statuses) {
    this.statuses.clear();
    this.statuses.addAll(statuses);
    return this;
  }

  /** @return the metadataLevel */
  public MetadataLevel getMetadataLevel() {
    return metadataLevel;
  }

  /**
   * What level metadata to return. Default MetadataLevel.FULL.
   *
   * @param metadataLevel the metadataLevel to set
   * @return this
   */
  public BoxQuery setMetadataLevel(MetadataLevel metadataLevel) {
    this.metadataLevel = metadataLevel;
    return this;
  }

  /** @return the fields */
  public Set<String> getFields() {
    return fields;
  }

  /**
   * Clears the fields and adds the given values.
   *
   * @param fields the fields to set
   * @return this
   */
  public BoxQuery setFields(Collection<String> fields) {
    this.fields.clear();
    this.fields.addAll(fields);
    return this;
  }

  /** @return the facets */
  public Set<Facet> getFacets() {
    return facets;
  }

  /**
   * Clears the facets and adds the given values.
   *
   * @param facets the facets to set
   * @return this
   */
  public BoxQuery setFacets(Collection<? extends Facet> facets) {
    this.facets.clear();
    this.facets.addAll(facets);
    return this;
  }

  /** @return the metadataOnly */
  public boolean isMetadataOnly() {
    return metadataOnly;
  }

  /**
   * Sets the query to only return metadata. The internal document will be empty.
   *
   * @return this
   */
  public BoxQuery setMetadataOnly() {
    return setMetadataOnly(true);
  }

  /**
   * Sets the query to only return metadata. The internal document will be empty.
   *
   * @param metadataOnly the metadataOnly to set
   * @return this
   */
  public BoxQuery setMetadataOnly(boolean metadataOnly) {
    this.metadataOnly = metadataOnly;
    return this;
  }
}
