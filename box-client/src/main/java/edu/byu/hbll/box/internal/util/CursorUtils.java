/** */
package edu.byu.hbll.box.internal.util;

import java.time.Instant;

/** @author Charles Draper */
public class CursorUtils {

  /** the epoch nanosecond last time nextCursor() was called */
  private static long lastCursor = 0;

  /**
   * Returns the cursor for the given time. Returns the number of nanoseconds since epoch until the
   * given time.
   *
   * @return the cursor for the given time
   */
  public static long getCursor(Instant time) {
    long t = (time.getEpochSecond() * 1000000000) + time.getNano();
    return t;
  }

  /**
   * Generates a timestamp based long that can be used as cursor in a database. It equals the number
   * of nanoseconds since the epoch.
   *
   * <p>With 63 bits available (64 bit long minus 1 bit for the sign), this strategy should work
   * until approximately the year 2262. There is a slight chance that a distributed box application
   * will create the same cursor. To overcome this, clients that read from this box should use a
   * page size of at least the number of instances of the box application. There is; however, no
   * chance of a single box instance creating identical cursors.
   *
   * @return
   */
  public static synchronized long nextCursor() {
    // workaround to get the number of nano seconds since the epoch, System.nanoTime does not return
    // the nanoseconds
    // of the clock, but it at least always counts up
    long cursor = (System.currentTimeMillis() * 1000000) + (System.nanoTime() % 1000000);

    // sometimes System.nanoTime() returns the same value more than once, so make sure if that
    // happens that we
    // increment the cursor from the value of last time
    if (cursor <= lastCursor) {
      cursor = lastCursor + 1;
    }

    lastCursor = cursor;
    return cursor;
  }
}
