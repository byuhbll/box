/** */
package edu.byu.hbll.box.client.internal.util;

import java.nio.charset.Charset;
import java.util.Base64;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.core.MultivaluedMap;

/**
 * A Basic Authentication client filter.
 *
 * @author Charles Draper
 */
public class BasicAuthenticationtFilter implements ClientRequestFilter {

  private final String authorization;

  /**
   * @param username the username used with basic authentication
   * @param password the password used with basic authentication
   */
  public BasicAuthenticationtFilter(String username, String password) {
    String token = username + ":" + password;
    this.authorization =
        "Basic "
            + Base64.getEncoder().encodeToString(token.getBytes(Charset.forName("iso-8859-1")));
  }

  @Override
  public void filter(ClientRequestContext requestContext) {
    MultivaluedMap<String, Object> headers = requestContext.getHeaders();
    headers.add("Authorization", authorization);
  }
}
