/** */
package edu.byu.hbll.box;

import java.util.ArrayList;
import java.util.Collection;

/** A response from a querying box. */
public class QueryResult extends ArrayList<BoxDocument> {

  private static final long serialVersionUID = 1L;

  /** */
  private long nextCursor;

  /** */
  public QueryResult() {}

  /** @param documents */
  public QueryResult(Collection<? extends BoxDocument> documents) {
    this.addAll(documents);
  }

  /** @return the nextCursor */
  public long getNextCursor() {
    return nextCursor;
  }

  /** @param nextCursor the nextCursor to set */
  public QueryResult setNextCursor(long nextCursor) {
    this.nextCursor = nextCursor;
    return this;
  }

  /**
   * Updates nextCursor according to the last document in the query result. If the result is empty,
   * it reuses the cursor in the query.
   */
  public QueryResult updateNextCursor(BoxQuery query) {
    if (!isEmpty()) {
      nextCursor = get(size() - 1).getCursor() + 1;
    } else {
      nextCursor = query.getCursor();
    }
    
    return this;
  }
}
