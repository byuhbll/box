/** */
package edu.byu.hbll.box;

import java.io.OutputStream;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;

/**
 * Represents a document in box including metadata and dependencies. This object contains an
 * internal document in the form of a {@link ObjectNode}. This internal document is the data of this
 * object and all other fields are metadata.
 *
 * @author Charles Draper
 */
public class BoxDocument {

  private static UncheckedObjectMapper mapper = ObjectMapperFactory.newUnchecked();

  private String id;

  private Status status = Status.UNPROCESSED;

  private ObjectNode document = JsonNodeFactory.instance.objectNode();

  private long cursor;

  private Instant modified;

  private Instant processed;

  private String message;

  private Set<Facet> facets = new LinkedHashSet<>();

  private Set<DocumentId> dependencies = new LinkedHashSet<>();

  private String groupId;

  private boolean statusSet;

  /** */
  protected BoxDocument() {}

  /**
   * Creates a new document initially in an UNPROCESSED state.
   *
   * @param id the unique id of the document
   */
  public BoxDocument(String id) {
    this.id = Objects.requireNonNull(id);
  }

  /**
   * Creates a new document with the given state.
   *
   * @param id the unique id of the document
   * @param status the status of the document
   */
  public BoxDocument(String id, Status status) {
    this.id = Objects.requireNonNull(id);
    this.status = status;
    this.statusSet = true;
  }

  /**
   * Creates a new document initially in a READY state with the given internal document.
   *
   * @param id the unique id of the document
   * @param document the processed document
   */
  public BoxDocument(String id, ObjectNode document) {
    this.id = Objects.requireNonNull(id);
    this.document = Objects.requireNonNull(document);
    this.status = Status.READY;
  }

  /**
   * Copy Constructor.
   *
   * @param boxDocument the box document to copy
   */
  public BoxDocument(BoxDocument boxDocument) {
    this.id = boxDocument.id;
    this.status = boxDocument.status;
    this.document = boxDocument.document.deepCopy();
    this.cursor = boxDocument.cursor;
    this.modified = boxDocument.modified;
    this.processed = boxDocument.processed;
    this.message = boxDocument.message;
    this.facets.addAll(boxDocument.facets);
    this.dependencies.addAll(boxDocument.dependencies);
    this.groupId = boxDocument.groupId;
    this.statusSet = boxDocument.statusSet;
  }

  /** @return a json representation of this document at the FULL metadata level */
  public ObjectNode toJson() {
    return toJson(MetadataLevel.FULL);
  }

  /** @return a json representation of this document at the given metadata level */
  public ObjectNode toJson(MetadataLevel metadataLevel) {
    ObjectNode metadata = mapper.valueToTree(this);
    ObjectNode json = (ObjectNode) metadata.remove("document");
    json.set("@box", metadata);

    // convert cursor to string or it will lose precision
    metadata.put("cursor", cursor + "");

    if (facets.isEmpty()) {
      metadata.remove("facets");
    }

    if (dependencies.isEmpty()) {
      metadata.remove("dependencies");
    }

    if (metadataLevel == MetadataLevel.NONE) {
      json.remove("@box");
    } else if (metadataLevel == MetadataLevel.CORE) {
      metadata.remove("cursor");
      metadata.remove("modified");
      metadata.remove("processed");
      metadata.remove("facets");
      metadata.remove("dependencies");
      metadata.remove("groupId");
    }

    return json;
  }

  /**
   * Writes this document in JSON UTF-8 format to the given {@link OutputStream}. The JSON that is
   * written is equivalent to that created by {@link #toJson()}.
   *
   * @param out the output stream
   */
  public void write(OutputStream out) {
    write(out, MetadataLevel.FULL);
  }

  /**
   * Writes this document in JSON UTF-8 format to the given {@link OutputStream}. The JSON that is
   * written is equivalent to that created by {@link #toJson(MetadataLevel)}.
   *
   * @param out the output stream
   */
  public void write(OutputStream out, MetadataLevel metadataLevel) {
    mapper.writeValue(out, toJson(metadataLevel));
  }

  /**
   * Creates a new box document from the given json.
   *
   * @param json
   * @return
   */
  public static BoxDocument fromJson(String json) {
    return fromJson((ObjectNode) mapper.readTree(json));
  }

  /**
   * Creates a new box document from the given json.
   *
   * @param json
   * @return
   */
  public static BoxDocument fromJson(ObjectNode json) {
    if (!json.path("@box").has("id")) {
      throw new IllegalArgumentException("Missing required field: @box.id");
    }

    ObjectNode document = json.deepCopy();
    ObjectNode metadata = (ObjectNode) document.remove("@box");

    BoxDocument boxDocument = mapper.treeToValue(metadata, BoxDocument.class);
    boxDocument.document = document;

    return boxDocument;
  }

  /**
   * Whether or not this document's dependencies differ from the give document's.
   *
   * @param o the document to compare
   * @return whether or not the dependencies differ
   */
  public boolean hasDifferentDependencies(BoxDocument o) {
    return !Objects.equals(dependencies, o.dependencies);
  }

  /**
   * Whether or not this document's processing has completed. That is whether the document is in a
   * READY or DELETED state.
   *
   * @return if in a ready or deleted state
   */
  @JsonIgnore
  public boolean isProcessed() {
    return status == Status.READY || status == Status.DELETED;
  }

  /**
   * Whether or not this document is in the UNPROCESSED state.
   *
   * @return if in the UNPROCESSED state
   */
  @JsonIgnore
  public boolean isUnProcessed() {
    return status == Status.UNPROCESSED;
  }

  /**
   * Whether or not this document is in the DELETED state.
   *
   * @return if in the DELETED state
   */
  @JsonIgnore
  public boolean isDeleted() {
    return status == Status.DELETED;
  }

  /**
   * Whether or not this document is in the ERROR state.
   *
   * @return if in the ERROR state
   */
  @JsonIgnore
  public boolean isError() {
    return status == Status.ERROR;
  }

  /**
   * Whether or not this document is in the READY state.
   *
   * @return if in the READY state
   */
  @JsonIgnore
  public boolean isReady() {
    return status == Status.READY;
  }

  /**
   * Determines if this document matches (or should be included in a result set) given the supplied
   * facets. In order to match, the document must have at least one facet from each facet group
   * represented in the supplied facets. If the supplied facet list is empty, the document will
   * match.
   *
   * @param facets the facets to test the document against
   * @return whether or not this document matches the given facets
   */
  public boolean matches(Collection<Facet> facets) {
    Map<String, Set<Facet>> facetMap = Facet.group(facets);

    for (String group : facetMap.keySet()) {
      if (Collections.disjoint(this.facets, facetMap.get(group))) {
        return false;
      }
    }

    return true;
  }

  /**
   * Explicitly sets the status to deleted.
   *
   * @return this
   */
  public BoxDocument setAsDeleted() {
    this.status = Status.DELETED;
    this.statusSet = true;
    return this;
  }

  /**
   * Explicitly sets the status to unprocessed.
   *
   * @return this
   */
  public BoxDocument setAsUnprocessed() {
    this.status = Status.UNPROCESSED;
    this.statusSet = true;
    return this;
  }

  /**
   * Explicitly sets the status to ready.
   *
   * @return this
   */
  public BoxDocument setAsReady() {
    this.status = Status.READY;
    this.statusSet = true;
    return this;
  }

  /**
   * Explicitly sets the status to error.
   *
   * @return this
   */
  public BoxDocument setAsError() {
    this.status = Status.ERROR;
    this.statusSet = true;
    return this;
  }

  /**
   * Explicitly sets the status to error and includes an error message.
   *
   * @return this
   */
  public BoxDocument setAsError(String message) {
    this.status = Status.ERROR;
    this.message = message;
    this.statusSet = true;
    return this;
  }

  /**
   * Adds a dependency for this document.
   *
   * @param sourceName the sourceName of the dependency to add.
   * @param id the id of the dependency to add.
   * @return this
   */
  public BoxDocument addDependency(String sourceName, String id) {
    this.dependencies.add(new DocumentId(sourceName, id));
    return this;
  }

  /**
   * Adds dependencies for this document.
   *
   * @param dependencies the dependencies to add.
   * @return this
   */
  public BoxDocument addDependencies(DocumentId... dependencies) {
    this.dependencies.addAll(Arrays.asList(dependencies));
    return this;
  }

  /**
   * Adds dependencies for this document.
   *
   * @param dependencies the dependencies to add.
   * @return this
   */
  public BoxDocument addDependencies(Collection<? extends DocumentId> dependencies) {
    this.dependencies.addAll(dependencies);
    return this;
  }

  /**
   * Adds a facet to this document.
   *
   * @param name name of the facet group
   * @param value value of the facet
   * @return this
   */
  public BoxDocument addFacet(String name, String value) {
    return addFacets(name, Arrays.asList(value));
  }

  /**
   * Adds multiple facets to the document.
   *
   * @param name name of the facet group
   * @param values value(s) of the facet
   * @return this
   */
  public BoxDocument addFacets(String name, String... values) {
    return addFacets(name, Arrays.asList(values));
  }

  /**
   * Adds multiple facets to the document.
   *
   * @param name name of the facet group
   * @param values values of the facet
   * @return this
   */
  public BoxDocument addFacets(String name, Collection<String> values) {
    values.forEach(v -> facets.add(new Facet(name, v)));
    return this;
  }

  /**
   * Adds a facet to this document.
   *
   * @param facet
   * @return this
   */
  public BoxDocument addFacet(Facet facet) {
    facets.add(facet);
    return this;
  }

  /**
   * Adds multiple facets to the document.
   *
   * @param facets
   * @return this
   */
  public BoxDocument addFacets(Facet... facets) {
    this.facets.addAll(Arrays.asList(facets));
    return this;
  }

  /**
   * Adds multiple facets to the document.
   *
   * @param facets
   * @return this
   */
  public BoxDocument addFacet(Collection<? extends Facet> facets) {
    this.facets.addAll(facets);
    return this;
  }

  /**
   * Adds a facet by querying the internal document using the given path.
   *
   * @param name name of facet
   * @param path path to field value in dot notation
   * @return this
   */
  public BoxDocument addFacetByQuery(String name, String path) {
    if (document != null) {
      String[] splitPath = path.trim().split("\\s*\\.\\s*");
      addFacetByQuery(name, document, splitPath);
    }

    return this;
  }

  /**
   * Adds facets by querying the internal document using the given paths.
   *
   * @param paths the paths to the field values in dot notation, key is name of facet, value is set
   *     of paths
   * @return this
   */
  public BoxDocument addFacetsByQuery(Map<String, Set<String>> paths) {
    if (document != null) {
      for (String name : paths.keySet()) {
        for (String path : paths.get(name)) {
          addFacetByQuery(name, path);
        }
      }
    }

    return this;
  }

  /**
   * @param name
   * @param fieldPath
   * @param node
   */
  private void addFacetByQuery(String name, JsonNode node, String[] path) {
    if (path.length == 0) {
      if (node.isValueNode()) {
        this.facets.add(new Facet(name, node.asText()));
      }
    } else if (node.isArray()) {
      for (JsonNode element : node) {
        addFacetByQuery(name, element, path);
      }
    } else if (node.isObject()) {
      addFacetByQuery(name, node.path(path[0]), Arrays.copyOfRange(path, 1, path.length));
    }
  }

  /** @return the unique id of the document */
  public String getId() {
    return id;
  }

  /**
   * Set the unique id of the document.
   *
   * @param id
   * @return this
   */
  public BoxDocument setId(String id) {
    this.id = Objects.requireNonNull(id);
    return this;
  }

  /** @return the internal document */
  public ObjectNode getDocument() {
    return document;
  }

  /**
   * Sets the internal document and modifies the status to READY if state not explicitly set
   * elsewhere.
   *
   * @param document the document to set
   */
  public BoxDocument setDocument(ObjectNode document) {
    this.document = Objects.requireNonNull(document);

    if (!statusSet) {
      this.status = Status.READY;
    }

    return this;
  }

  /**
   * Sets the status to READY if state not explicitly set elsewhere and returns the internal
   * document. The internal documents is initially empty if not already set elsewhere.
   */
  public ObjectNode withDocument() {
    if (!statusSet) {
      this.status = Status.READY;
    }

    return this.document;
  }

  /** @return the cursor */
  public long getCursor() {
    return cursor;
  }

  /**
   * Sets the cursor.
   *
   * <p>When processing a document, this should only be updated if the box document actually changed
   * since last time. This should generally be left blank because Box will make that determination
   * and set it appropriately when saving. If it's not blank, Box will honor the set value.
   *
   * @param cursor the cursor to set
   * @return this
   */
  public BoxDocument setCursor(long cursor) {
    this.cursor = cursor;
    return this;
  }

  /** @return when the document was last modified */
  public Instant getModified() {
    return modified;
  }

  /**
   * Sets the modified date.
   *
   * <p>When processing a document, this should only be updated if the box document actually changed
   * since last time. This should generally be left blank because Box will make that determination
   * and set it appropriately when saving. If it's not blank, Box will honor the set value.
   *
   * @param modified the modified to set
   * @return this
   */
  public BoxDocument setModified(Instant modified) {
    this.modified = modified;
    return this;
  }

  /** @return the processed */
  @JsonProperty
  public Instant getProcessed() {
    return processed;
  }

  /**
   * Sets the processed date.
   *
   * <p>When processing a document, this can be left blank because Box will set it appropriately
   * when saving. If it's not blank, Box will honor the set value.
   *
   * @param processed the processed to set
   * @return this
   */
  public BoxDocument setProcessed(Instant processed) {
    this.processed = processed;
    return this;
  }

  /** @return the error */
  public String getMessage() {
    return message;
  }

  /**
   * @param message the message to set if an error occurred
   * @return this
   */
  public BoxDocument setMessage(String message) {
    this.message = message;
    return this;
  }

  /** @return the facets */
  public Set<Facet> getFacets() {
    return facets;
  }

  /**
   * Clears current facets and adds these facets.
   *
   * @param facets the facets to set
   * @return this
   */
  public BoxDocument setFacets(Collection<Facet> facets) {
    this.facets.clear();
    this.facets.addAll(facets);
    return this;
  }

  /** @return the dependencies */
  public Set<DocumentId> getDependencies() {
    return dependencies;
  }

  /**
   * Clears current dependencies and adds these dependencies.
   *
   * @param dependencies the dependencies to set
   * @return this
   */
  public BoxDocument setDependencies(Collection<DocumentId> dependencies) {
    this.dependencies.clear();
    this.dependencies.addAll(dependencies);
    return this;
  }

  /** @return the groupId */
  public String getGroupId() {
    return groupId;
  }

  /**
   * Sets this document's group (used for orphan cleanup).
   *
   * @param groupId the groupId to set
   * @return this
   */
  public BoxDocument setGroupId(String groupId) {
    this.groupId = groupId;
    return this;
  }

  /** @return the status */
  public Status getStatus() {
    return status;
  }

  /**
   * Explicitly sets the status.
   *
   * @param status the status to set
   * @return this
   */
  public BoxDocument setStatus(Status status) {
    this.status = status;
    this.statusSet = true;
    return this;
  }

  /**
   * Clears the set of facets.
   *
   * @return this
   */
  public BoxDocument clearFacets() {
    this.facets.clear();
    return this;
  }

  /**
   * Clears the set of dependencies.
   *
   * @return this
   */
  public BoxDocument clearDependencies() {
    this.dependencies.clear();
    return this;
  }

  @Override
  public String toString() {
    return toJson().toString();
  }

  /**
   * Status of the document.
   *
   * @author Charles Draper
   */
  public static enum Status {
    /** The document is new and has not yet been processed. */
    UNPROCESSED,

    /** The document has been processed and is ready to use. */
    READY,

    /** The document has been deleted. */
    DELETED,

    /** There was an error in the processing. */
    ERROR
  }
}
