/** */
package edu.byu.hbll.box.client.internal.util;

import java.io.IOException;
import java.net.URI;
import java.nio.ByteBuffer;
import javax.websocket.ContainerProvider;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

/** @author Charles Draper */
public class SocketsConnection implements AutoCloseable {

  private Object clientEndpoint;

  private URI uri;

  private WebSocketContainer container;

  private Session session;

  private volatile boolean closed;

  private Exception exception;

  private Thread thread;

  /** @param uri */
  public SocketsConnection(Object clientEndpoint, URI uri) {
    this.clientEndpoint = clientEndpoint;
    this.uri = uri;
    this.container = ContainerProvider.getWebSocketContainer();
    this.thread = new Thread(() -> test());
    this.thread.start();
  }

  /** */
  private void test() {
    while (!closed) {

      if (session != null) {
        try {
          session.getBasicRemote().sendPing(ByteBuffer.allocate(0));
        } catch (Exception e) {
          this.exception = e;

          try {
            session.close();
          } catch (IOException e1) {
          }

          session = null;
        }
      }

      if (session == null) {
        try {
          session = container.connectToServer(clientEndpoint, uri);
        } catch (Exception e) {
          this.exception = e;
        }
      }

      try {
        Thread.sleep(10000);
      } catch (InterruptedException e) {
        return;
      }
    }
  }

  @Override
  public void close() throws IOException {
    closed = true;
    this.thread.interrupt();

    if (session != null) {
      session.close();
    }
  }

  /** @return the session */
  public Session getSession() {
    return session;
  }

  /** @return the exception */
  public Exception getException() {
    return exception;
  }
}
