/** */
package edu.byu.hbll.box.client;

import java.io.Closeable;
import java.io.IOException;
import java.net.URI;
import javax.websocket.ClientEndpoint;
import javax.websocket.OnMessage;
import javax.ws.rs.core.UriBuilder;
import edu.byu.hbll.box.client.internal.util.SocketsConnection;

/**
 * Listens for updates inside a Box and runs the given runnable when there is one. Note that Box
 * only sends out update signals at most once per second.
 *
 * @author Charles Draper
 */
@ClientEndpoint
public class BoxUpdatesClient implements Closeable {

  private Runnable runnable;
  private SocketsConnection c;

  /**
   * @param uri the base uri of the box source (eg, http://localhost:8080/app/box)
   * @param runnable the runnable to run when there are updates
   */
  public BoxUpdatesClient(String uri, Runnable runnable) {
    this.runnable = runnable;

    String httpScheme = URI.create(uri).getScheme();
    String wsScheme = httpScheme.equals("https") ? "wss" : "ws";
    URI u = UriBuilder.fromUri(uri).scheme(wsScheme).path("updates").build();
    this.c = new SocketsConnection(this, u);
  }

  @OnMessage
  public void handleMessage(String message) {
    runnable.run();
  }

  @Override
  public void close() throws IOException {
    if (c != null) {
      c.close();
    }
  }
}
