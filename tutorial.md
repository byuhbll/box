#HELLO WORLD
The following procedure will walk you through a Hello World! example of Box. This will represent the bare minimum to start using Box. A familiarity with Java EE is requisite. This example uses Maven.

**Instructions:**

Create a new Java EE project with hello as the artifactId. See https://bitbucket.org/byuhbll/java-templates for setting one up quickly.

Add Box as a dependency to your Maven POM. Get the latest version at https://bitbucket.org/byuhbll/box.
######
```xml
<dependency>
  <groupId>edu.byu.hbll.box</groupId>
  <artifactId>box-core</artifactId>
  <version>1.2.2</version>
</dependency>
<dependency>
  <groupId>edu.byu.hbll.box</groupId>
  <artifactId>box-web</artifactId>
  <version>1.2.2</version>
</dependency>
```
    
Create an EJB singleton class that starts up when the application is deployed. This will be used to configure Box. If using the java-ee-project archetype, you can use the existing Configuration class.
######
```java
@Singleton
@Startup
public class Configuration {
    ...
}
```
    
Inject the Box EJB into your class.
######
```java
@Inject
private Box box;
```

Initialize Box in the loadConfigFrom(...) method.
######
```java
box.initialize(config.path("box"));
```

Every Box instance requires at least one source. We'll create a source called hello and have it do some simple process. In order for the source to do some kind of processing, you need to implement Processor or SingleProcessor. In this case we will implement SingleProcessor (SingleProcessor removes some of the complexity of Processor for more basic cases).
######
```java
public class HelloWorld implements SingleProcessor {

  @Override
  public ProcessResult process(ProcessContext context) {
    ProcessResult result = new ProcessResult();
    ObjectNode document = JsonNodeFactory.instance.objectNode();
    document.put("id", context.getId());
    document.put("message", "Hello World!");
    result.add(new BoxDocument(context.getId(), document));
    return result;
  }
}
```

Create a config directory in the root folder of the project and add a config.yml file. Change the package path of the process type to match the path to your own Hello World class.
######
``` yaml
# Box configuration section
box:
 # Source definitions
 sources:
   # Source 1 (required, any name matching [0-9A-Za-z_]+)
   hello:
     process:
       # Class that implements edu.byu.hbll.box.Processor (required)
       type: edu.byu.hbll.hello.HelloWorld
 # Database definition (can be overridden per source)
 db:
   # class that implements edu.byu.hbll.box.BoxDatabase (default: edu.byu.hbll.box.impl.InMemoryDatabase)
   # other pre-implemented options: edu.byu.hbll.box.impl.InMemoryDatabase
   type: edu.byu.hbll.box.impl.MongoDatabase
   # any additional parameters needed to configure the database client
   params:
     # ...
     #
     # for MongoDatabase
     # Database name for this instance of box (required)
     database: hello
```

Deploy your application to a Java EE application server.

Go to localhost:4848 and add the config property from the Configuration class to the server-config System Properties with the value being the path to the config file we just created.

Restart the server.

Hit http://localhost:8080/hello/box/documents/ben and you'll get back a 404 with the following payload.
######
```json
{
  "@box": {
    "id": "ben",
    "status": "UNPROCESSED"
  }
}
```

Wait a brief moment and hit it again to get the processed document.
######
```json
{
  "id": "ben",
  "message": "Hello World!",
  "@box": {
    "id": "ben",
    "status": "READY"
  }
}
```

#WEB SERVICE CACHE TUTORIAL

This tutorial walks you through a web service cache example. This Box instance will essentially act as a proxy caching server. Calls to it will trigger calls to the real web service in an asynchronous fashion and cache the response for quick retrieval later.

Follow the Hello World! tutorial.
Create a new class called BooksProcessor.java to be a client that hits another web service and returns the responses.

Your BooksProcessor should implement the SingleProcessor, and should look like this:
######
```java
public class BooksProcessor implements SingleProcessor {

   private static final String URI_TEMPLATE = "http://openlibrary.org/books/{id}.json";
   private final Client client = ClientBuilder.newClient();
   private final ObjectMapper mapper = new ObjectMapper();

   @Override
   public ProcessResult process(ProcessContext processContext) {
       ProcessResult result = new ProcessResult();
       String id = processContext.getId();

       ObjectNode document = null;

       try {
           URI uri = UriBuilder.fromUri(URI_TEMPLATE).build(id);
           String response = client.target(uri).request().get(String.class);
           document = (ObjectNode) mapper.readTree(response);
       } catch (IOException e) {
           e.printStackTrace();
       }

       if(document != null) {
           JsonNode title = document.path("title");
           JsonNode description = document.path("description");
           JsonNode notes = document.path("notes");

           ObjectNode resultDocument = mapper.createObjectNode();
           resultDocument.put("id", id);
           resultDocument.set("title", title);
           resultDocument.set("description", description);
           resultDocument.set("notes", notes);

           BoxDocument boxDocument = new BoxDocument(id, resultDocument);
           result.addDocument(boxDocument);
       }

       return result;
   }
}
```

In the config.yml, add the following source below the hello source:
######
```yaml
books:
  process:
    type: edu.byu.hbll.hello.BooksProcessor
```

Hit http://localhost:8080/hello/books/documents/OL7789431M twice to get the response.
######
```json
{
  "id": "OL7789431M",
  "title": "Harry Potter and the Deathly Hallows", 
  "description": "Burdened with the dark, dangerous, and seemingly impossible task of locating and destroying Voldemort's remaining Horcruxes, Harry, feeling alone and uncertain about his future, struggles to find the inner strength he needs to follow the path set out before him.", 
  "notes": "Sequel to: Harry Potter and the Half-Blood Prince.", 
  "@box": {
    "id": "OL7789431M", "status": "READY"
  }
}
```
