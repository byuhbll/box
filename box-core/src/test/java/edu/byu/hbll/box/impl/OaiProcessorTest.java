/** */
package edu.byu.hbll.box.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.Facet;
import edu.byu.hbll.box.InitConfig;
import edu.byu.hbll.box.ProcessContext;
import edu.byu.hbll.xml.XmlUtils;

/** @author Charles Draper */
@RunWith(MockitoJUnitRunner.class)
public class OaiProcessorTest {

  @Mock Client client;
  @Mock WebTarget webTarget;
  @Mock Invocation.Builder invocationBuilder;

  OaiProcessor oai;

  /** @throws java.lang.Exception */
  @Before
  public void setUp() throws Exception {
    oai = new OaiProcessor();
  }

  /** Test method for {@link edu.byu.hbll.box.impl.OaiProcessor#OaiProcessor()}. */
  @Test
  public void testOaiProcessor() {
    new OaiProcessor();
  }

  /** Test method for {@link edu.byu.hbll.box.impl.OaiProcessor#OaiProcessor(java.lang.String)}. */
  @Test
  public void testOaiProcessorString() {
    OaiProcessor oai = new OaiProcessor("https://www.example.com/");
    assertEquals("https://www.example.com/", oai.getUri());
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.box.impl.OaiProcessor#postConstruct(com.fasterxml.jackson.databind.JsonNode)}.
   */
  @Test
  public void testPostConstruct() {
    ObjectNode config = JsonNodeFactory.instance.objectNode();
    config.put("uri", "https://www.example.com/");
    config.put("metadataPrefix", "oai_dc_2");
    config.put("idRegex", "oai(.*)");
    config.put("idReplacement", "$1");

    OaiProcessor oai = new OaiProcessor();
    oai.postConstruct(new InitConfig(config));

    assertEquals("https://www.example.com/", oai.getUri());
    assertEquals("oai_dc_2", oai.getMetadataPrefix());
    assertEquals("oai(.*)", oai.getIdRegex());
    assertEquals("$1", oai.getIdReplacement());
  }

  /**
   * Test method for {@link edu.byu.hbll.box.impl.OaiProcessor#process(java.lang.String)}.
   *
   * @throws IOException
   * @throws SAXException
   * @throws FileNotFoundException
   */
  @Test
  public void testProcess() throws Exception {
    Document doc =
        XmlUtils.parse(
            new FileInputStream("src/test/resources/edu/byu/hbll/box/impl/oai.process.xml"));
    when(client.target(
            new URI("https://www.example.com/?verb=GetRecord&identifier=1&metadataPrefix=oai")))
        .thenReturn(webTarget);
    when(webTarget.request()).thenReturn(invocationBuilder);
    when(invocationBuilder.get(Document.class)).thenReturn(doc);

    oai.setUri("https://www.example.com/");
    oai.setMetadataPrefix("oai");
    oai.setClient(client);
    oai.setIdRegex("a(.)b");
    oai.setIdReplacement("$1");

    BoxDocument resultDocument = oai.process(new ProcessContext("a1b")).get(0);

    assertEquals("a1b", resultDocument.getId());

    ObjectNode document = resultDocument.getDocument();
    assertEquals(
        "oai:scholarsarchive.byu.edu:byufamilyhistorian-1040",
        document.path("header").path("identifier").asText());

    Set<Facet> actualFacets = new HashSet<>(resultDocument.getFacets());
    Set<Facet> expectedFacets =
        new HashSet<>(
            Arrays.asList(
                Facet.parse("set:publication:journals"),
                Facet.parse("set:publication:byufamilyhistorian")));
    assertEquals(expectedFacets, actualFacets);
  }

  /** Test method for {@link edu.byu.hbll.box.impl.OaiProcessor#getClient()}. */
  @Test
  public void testGetClient() {
    assertNotNull(oai.getClient());
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.box.impl.OaiProcessor#setClient(javax.ws.rs.client.Client)}.
   */
  @Test
  public void testSetClient() {
    oai.setClient(client);
    assertEquals(client, oai.getClient());

    try {
      oai.setClient(null);
      fail("should throw exception");
    } catch (Exception e) {
    }
  }

  /** Test method for {@link edu.byu.hbll.box.impl.OaiProcessor#setUri(java.lang.String)}. */
  @Test
  public void testSetUri() {
    oai.setClient(client);
    assertEquals(client, oai.getClient());

    try {
      oai.setClient(null);
      fail("should throw exception");
    } catch (Exception e) {
    }
  }

  /** Test method for {@link edu.byu.hbll.box.impl.OaiProcessor#getMetadataPrefix()}. */
  @Test
  public void testGetMetadataPrefix() {
    assertEquals("oai_dc", oai.getMetadataPrefix());
  }

  /**
   * Test method for {@link edu.byu.hbll.box.impl.OaiProcessor#setMetadataPrefix(java.lang.String)}.
   */
  @Test
  public void testSetMetadataPrefix() {
    oai.setMetadataPrefix("asdf");
    assertEquals("asdf", oai.getMetadataPrefix());

    try {
      oai.setMetadataPrefix(null);
      fail("should throw exception");
    } catch (Exception e) {
    }
  }

  /** Test method for {@link edu.byu.hbll.box.impl.OaiProcessor#setSets(java.util.Set)}. */
  @Test
  public void testSetSets() {
    oai.setSets(new HashSet<>(Arrays.asList("a", "b")));
    assertEquals(new HashSet<>(Arrays.asList("a", "b")), oai.getSets());

    try {
      oai.setSets(null);
      fail("should throw exception");
    } catch (Exception e) {
    }
  }
}
