/** */
package edu.byu.hbll.box.util;

import static org.junit.Assert.*;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import org.junit.Test;
import edu.byu.hbll.box.internal.util.CursorUtils;

/** @author Charles Draper */
public class CursorUtilsTest {

  /**
   * Test method for {@link
   * edu.byu.hbll.box.internal.util.CursorUtils#getCursor(java.time.Instant)}.
   */
  @Test
  public void testGetCursor() {
    assertEquals(0, CursorUtils.getCursor(Instant.EPOCH));
    assertEquals(123000000123l, CursorUtils.getCursor(Instant.ofEpochSecond(123, 123)));
  }

  /** Test method for {@link edu.byu.hbll.box.internal.util.CursorUtils#nextCursor()}. */
  @Test
  public void testNextCursor() {
    long cursorBefore =
        CursorUtils.getCursor(
            Instant.now().truncatedTo(ChronoUnit.MILLIS).plusNanos(System.nanoTime() % 1000000));
    long cursor0 = CursorUtils.nextCursor();

    // because System.nanotime() can return the same value multiple times, we make sure cursor the
    // next cursor is
    // always greater than the previous
    long cursor1 = CursorUtils.nextCursor();
    long cursor2 = CursorUtils.nextCursor();
    long cursor3 = CursorUtils.nextCursor();
    long cursor4 = CursorUtils.nextCursor();
    long cursor5 = CursorUtils.nextCursor();
    long cursor6 = CursorUtils.nextCursor();
    long cursor7 = CursorUtils.nextCursor();
    long cursor8 = CursorUtils.nextCursor();
    long cursor9 = CursorUtils.nextCursor();
    long cursorAfter =
        CursorUtils.getCursor(
            Instant.now().truncatedTo(ChronoUnit.MILLIS).plusNanos(System.nanoTime() % 1000000));

    assertTrue(cursorBefore < cursor0);
    assertTrue(cursor0 < cursor1);
    assertTrue(cursor1 < cursor2);
    assertTrue(cursor2 < cursor3);
    assertTrue(cursor3 < cursor4);
    assertTrue(cursor4 < cursor5);
    assertTrue(cursor5 < cursor6);
    assertTrue(cursor6 < cursor7);
    assertTrue(cursor7 < cursor8);
    assertTrue(cursor8 < cursor9);
    assertTrue(cursor9 < cursorAfter);
  }
}
