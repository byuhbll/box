/** */
package edu.byu.hbll.box.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;
import java.io.FileInputStream;
import java.net.URI;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.w3c.dom.Document;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.Facet;
import edu.byu.hbll.box.HarvestContext;
import edu.byu.hbll.box.HarvestResult;
import edu.byu.hbll.box.InitConfig;
import edu.byu.hbll.xml.XmlUtils;

/** @author Charles Draper */
@RunWith(MockitoJUnitRunner.class)
public class OaiHarvestClientTest {

  @Mock Client client;
  @Mock WebTarget webTarget;
  @Mock Invocation.Builder invocationBuilder;

  OaiHarvester oai;

  /** @throws java.lang.Exception */
  @Before
  public void setUp() throws Exception {
    oai = new OaiHarvester();

    Document doc1 =
        XmlUtils.parse(
            new FileInputStream("src/test/resources/edu/byu/hbll/box/impl/oai.harvest.1.xml"));
    Document doc2 =
        XmlUtils.parse(
            new FileInputStream("src/test/resources/edu/byu/hbll/box/impl/oai.harvest.2.xml"));

    String uri1 =
        "https://www.example.com/?verb=ListRecords&from=1970-01-02&until="
            + LocalDate.now()
            + "&metadataPrefix=oai_dc";

    String uri2 = "https://www.example.com/?verb=ListRecords&resumptionToken=5485626%2Foai_dc%2F100%2F%2F";

    when(client.target(new URI(uri1))).thenReturn(webTarget);
    when(client.target(new URI(uri2))).thenReturn(webTarget);
    when(webTarget.request()).thenReturn(invocationBuilder);
    when(invocationBuilder.get(Document.class)).thenReturn(doc1).thenReturn(doc2);
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.box.impl.OaiHarvester#postConstruct(com.fasterxml.jackson.databind.JsonNode)}.
   */
  @Test
  public void testPostConstruct() {
    ObjectNode config = JsonNodeFactory.instance.objectNode();
    config.put("uri", "https://www.example.com/");
    config.put("metadataPrefix", "oai_dc_2");
    config.put("idRegex", "oai(.*)");
    config.put("idReplacement", "$1");
    config.put("idsOnly", true);
    config.put("chunked", true);
    config.putArray("sets").add("a").add("b");

    OaiHarvester oai = new OaiHarvester();
    oai.postConstruct(new InitConfig(config));

    assertEquals("https://www.example.com/", oai.getUri());
    assertEquals("oai_dc_2", oai.getMetadataPrefix());
    assertEquals("oai(.*)", oai.getIdRegex());
    assertEquals("$1", oai.getIdReplacement());
    assertTrue(oai.isIdsOnly());
    assertTrue(oai.isChunked());
    assertEquals(new HashSet<>(Arrays.asList("a", "b")), oai.getSets());
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.box.impl.OaiHarvester#harvest(com.fasterxml.jackson.databind.node.ObjectNode)}.
   */
  @Test
  public void testNext() throws Exception {

    oai.setUri("https://www.example.com/");
    oai.setClient(client);

    HarvestResult result = oai.harvest(new HarvestContext());
    assertEquals(
        "oai:scholarsarchive.byu.edu:byufamilyhistorian-1040", result.iterator().next().getId());
    assertEquals(2, result.size());
    assertTrue(result.hasMore());

    result = oai.harvest(new HarvestContext(result.getCursor()));
    BoxDocument resultDocument = result.iterator().next();
    assertEquals("oai:scholarsarchive.byu.edu:mphs-1079", resultDocument.getId());
    assertEquals(2, result.size());
    assertFalse(result.hasMore());

    Set<Facet> actualFacets = new HashSet<>(resultDocument.getFacets());
    Set<Facet> expectedFacets =
        new HashSet<>(
            Arrays.asList(
                Facet.parse("set:publication:journals"), Facet.parse("set:publication:mphs")));
    assertEquals(expectedFacets, actualFacets);
  }

  /** Test method for {@link edu.byu.hbll.box.impl.OaiHarvester#getClient()}. */
  @Test
  public void testGetClient() {
    assertNotNull(oai.getClient());
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.box.impl.OaiHarvester#setClient(javax.ws.rs.client.Client)}.
   */
  @Test
  public void testSetClient() {
    oai.setClient(client);
    assertEquals(client, oai.getClient());

    try {
      oai.setClient(null);
      fail("should throw exception");
    } catch (Exception e) {
    }
  }

  /** Test method for {@link edu.byu.hbll.box.impl.OaiHarvester#setUri(java.lang.String)}. */
  @Test
  public void testSetUri() {
    oai.setClient(client);
    assertEquals(client, oai.getClient());

    try {
      oai.setClient(null);
      fail("should throw exception");
    } catch (Exception e) {
    }
  }

  /** Test method for {@link edu.byu.hbll.box.impl.OaiHarvester#getMetadataPrefix()}. */
  @Test
  public void testGetMetadataPrefix() {
    assertEquals("oai_dc", oai.getMetadataPrefix());
  }

  /**
   * Test method for {@link edu.byu.hbll.box.impl.OaiHarvester#setMetadataPrefix(java.lang.String)}.
   */
  @Test
  public void testSetMetadataPrefix() {
    oai.setMetadataPrefix("asdf");
    assertEquals("asdf", oai.getMetadataPrefix());

    try {
      oai.setMetadataPrefix(null);
      fail("should throw exception");
    } catch (Exception e) {
    }
  }

  /** Test method for {@link edu.byu.hbll.box.impl.OaiHarvester#setSets(java.util.Set)}. */
  @Test
  public void testSetSets() {
    oai.setSets(new HashSet<>(Arrays.asList("a", "b")));
    assertEquals(new HashSet<>(Arrays.asList("a", "b")), oai.getSets());

    try {
      oai.setSets(null);
      fail("should throw exception");
    } catch (Exception e) {
    }
  }
}
