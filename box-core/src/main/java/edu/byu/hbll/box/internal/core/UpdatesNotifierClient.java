/** */
package edu.byu.hbll.box.internal.core;

import javax.websocket.ClientEndpoint;
import javax.websocket.OnMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Triggers a harvest for the configured source when server sends an update notification.
 *
 * @author Charles Draper
 */
@ClientEndpoint
public class UpdatesNotifierClient {

  static final Logger logger = LoggerFactory.getLogger(UpdatesNotifierClient.class);

  private Registry registry;
  private String sourceName;

  /**
   * @param registry
   * @param sourceName
   */
  public UpdatesNotifierClient(Registry registry, String sourceName) {
    this.registry = registry;
    this.sourceName = sourceName;
  }

  /** @param message */
  @OnMessage
  public void handleMessage(String message) {
    registry.triggerHarvest(sourceName);
  }
}
