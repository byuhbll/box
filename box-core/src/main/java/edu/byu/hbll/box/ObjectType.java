/**
 * 
 */
package edu.byu.hbll.box;

/**
 * The type of configuration object.
 * 
 * @author Charles Draper
 */
public enum ObjectType {
  
  PROCESSOR, HARVESTER, BOX_DATABASE, CURSOR_DATABASE, OTHER;

}
