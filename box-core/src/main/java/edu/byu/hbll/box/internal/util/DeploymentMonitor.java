/** */
package edu.byu.hbll.box.internal.util;

import java.util.concurrent.CountDownLatch;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Timer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** @author Charles Draper */
@Singleton
public class DeploymentMonitor {

  static final Logger logger = LoggerFactory.getLogger(DeploymentMonitor.class);

  private CountDownLatch latch = new CountDownLatch(1);

  @Schedule(second = "*", minute = "*", hour = "*", persistent = false)
  public void timeout(Timer timer) {
    latch.countDown();
    logger.debug("deployment complete");
    timer.cancel();
  }

  /**
   * @return true if deployment is complete, false otherwise
   */
  public boolean isDeployed() {
    return latch.getCount() == 0;
  }
}
