/** */
package edu.byu.hbll.box;

import edu.byu.hbll.box.BoxDocument;

/** @author Charles Draper */
public class StartGroupDocument extends BoxDocument {

  public StartGroupDocument(String groupId) {
    super(groupId);
  }
}
