/** */
package edu.byu.hbll.box.impl;

import javax.enterprise.inject.Instance;
import javax.enterprise.inject.spi.CDI;
import edu.byu.hbll.box.HarvestContext;
import edu.byu.hbll.box.HarvestResult;
import edu.byu.hbll.box.Harvester;

/**
 * A harvester that injects another harvester using CDI and uses the injected harvester as the main
 * harvester.
 *
 * @author Charles Draper
 */
public class CdiHarvester implements Harvester {

  /** */
  private Class<? extends Harvester> injectable;

  /** @param injectable */
  public CdiHarvester(Class<? extends Harvester> injectable) {
    this.injectable = injectable;
  }

  /**
   * @param context
   * @return
   */
  @SuppressWarnings({"rawtypes", "unchecked"})
  @Override
  public HarvestResult harvest(HarvestContext context) {
    Instance instance = CDI.current().select(injectable);
    Harvester client = (Harvester) instance.get();

    try {
      HarvestResult harvestResult = client.harvest(context);
      return harvestResult;
    } finally {
      instance.destroy(client);
    }
  }
}
