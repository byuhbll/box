/** */
package edu.byu.hbll.box.internal.util;

import java.io.IOException;
import java.io.UncheckedIOException;
import org.bson.Document;
import com.fasterxml.jackson.databind.JsonNode;
import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;

/** */
public class JsonUtils {

  /** */
  private static UncheckedObjectMapper mapper = ObjectMapperFactory.newUnchecked();

  /**
   * @param document
   * @return
   */
  public static String serialize(Object document) {
    return mapper.writeValueAsString(document);
  }

  /**
   * @param value
   * @return
   */
  public static JsonNode deserialize(String value) {
    return mapper.readTree(value);
  }

  /**
   * @param document
   * @return
   */
  public static Document toDocument(Object document) {
    return Document.parse(serialize(document));
  }

  /**
   * @param document
   * @return
   */
  public static JsonNode toJsonNode(Object document) {
    return mapper.valueToTree(document);
  }

  public static void bind(JsonNode node, Object object) {
    try {
      mapper.readerForUpdating(object).readValue(node);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  /**
   * @param node
   * @param dotPath
   * @return
   */
  public static JsonNode dotPath(JsonNode node, String dotPath) {
    String[] path = dotPath.split("\\.");
    JsonNode currentNode = node;

    for (String fieldName : path) {
      currentNode = currentNode.path(fieldName);
    }

    return currentNode;
  }
}
