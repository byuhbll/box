/** */
package edu.byu.hbll.box;

import edu.byu.hbll.box.internal.util.JsonUtils;

/**
 * Any extension classes (ie, classes that implement {@link Processor}, {@link Harvester}, {@link
 * BoxDatabase}, etc) must implement this. This is very similar to the PostConstruct and PreDestroy
 * annotations in JavaEE managed classes. When Box instantiates an extension class, it calls
 * postConstruct and passes it configuration information included when {@link BoxConfiguration} was
 * created. By default this method binds the values in params to the fields in the newly constructed
 * object. The binding is handled by Jackson.
 *
 * <p>When the application is undeployed, preDestroy() is called so the client can perform any
 * cleanup necessary. By default this method does nothing.
 *
 * <p>Note: postConstruct and preDestroy are only called if Box constructs the instance.
 *
 * @author Charles Draper
 */
public interface BoxConfigurable {

  /**
   * Called when Box constructs a new instance of the class.
   *
   * @param config configuration for this client as set up in {@link BoxConfiguration}.
   */
  default void postConstruct(InitConfig config) {
    JsonUtils.bind(config.getParams(), this);
  }

  /**
   * Called when the application is undeployed to allow the client to cleanup resources. Default is
   * to do nothing.
   */
  default void preDestroy() {}
}
