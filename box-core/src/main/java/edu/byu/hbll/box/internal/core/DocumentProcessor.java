/** */
package edu.byu.hbll.box.internal.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.DocumentId;
import edu.byu.hbll.box.EndGroupDocument;
import edu.byu.hbll.box.ProcessBatch;
import edu.byu.hbll.box.ProcessContext;
import edu.byu.hbll.box.ProcessResult;
import edu.byu.hbll.box.Source;
import edu.byu.hbll.box.StartGroupDocument;
import edu.byu.hbll.box.internal.util.SmoothedAverage;
import edu.byu.hbll.misc.BatchExecutorService;
import edu.byu.hbll.misc.BatchRunnable;
import edu.byu.hbll.stats.time.Benchmark;
import edu.byu.hbll.stats.time.Times;
import edu.byu.hbll.stats.time.Times.Marker;

/** @author Charles Draper */
public class DocumentProcessor implements AutoCloseable {

  static final Logger logger = LoggerFactory.getLogger(DocumentProcessor.class);

  /** Processes documents in asynchronous batches. */
  private BatchExecutorService<String, BoxDocument> executor;

  private Registry registry;

  private QuotaSemaphore quotaSemaphore;

  private Source source;

  private AtomicLong numDocuments = new AtomicLong();
  private AtomicLong numBatches = new AtomicLong();
  private Times<Benchmark> t = Times.benchmark();

  private SmoothedAverage averageBatchSize = new SmoothedAverage();

  /** */
  private volatile boolean shutdown;

  /**
   * @param registry
   * @param source
   * @param quotaSemaphore
   */
  public DocumentProcessor(Registry registry, Source source, QuotaSemaphore quotaSemaphore) {
    this.registry = registry;
    this.source = source;
    this.quotaSemaphore = quotaSemaphore;

    BatchRunnable<String, BoxDocument> runnable = batch -> run(batch);

    this.executor =
        new BatchExecutorService.Builder(runnable)
            .threadFactory(registry.getThreadFactory())
            .threadCount(source.getThreadCount())
            .batchCapacity(source.getBatchCapacity())
            .queueCapacity(source.getBatchCapacity() * source.getThreadCount())
            .batchDelay(source.getBatchDelay())
            .suspend(source.getSuspend())
            .build();
  }

  /** @param ids */
  public void process(String... ids) {
    process(Arrays.asList(ids));
  }

  /** @param ids */
  public void process(Collection<String> ids) {
    try {
      executor.submitAll(ids);
    } catch (InterruptedException e) {
      return;
    }
  }

  /**
   * @param ids
   * @return
   */
  public List<BoxDocument> processAndWait(List<String> ids) {
    List<BoxDocument> documents = new ArrayList<>();

    try {
      List<Future<BoxDocument>> futures = executor.submitAll(ids);

      for (Future<BoxDocument> future : futures) {
        try {
          documents.add(future.get());
        } catch (ExecutionException e) {
          logger.error(e.toString(), e);
          documents.add(null);
        }
      }
    } catch (InterruptedException e) {
    }

    return documents;
  }

  /**
   * @param ids
   * @return
   */
  private List<BoxDocument> run(List<String> ids) {

    List<BoxDocument> results = new ArrayList<>();

    try {
      logger.info("Processing " + source.getName() + ":" + ids);

      // block if quota reached
      while (!quotaSemaphore.tryAquire(1, TimeUnit.SECONDS) && !shutdown) {}

      DocumentHandler handler = registry.getDocumentHandler();

      // if processor is not configured to run, then stop processing here
      if (!source.isProcessEnabled() || source.getProcessor() == null) {
        return Collections.emptyList();
      }

      Marker m = t.marker();

      // find all registered dependencies
      Map<String, Set<DocumentId>> dependencyMap = source.getDb().findDependencies(ids);

      // group dependencies by source for separate requests
      Map<String, BoxQuery> queryMap = new HashMap<>();

      for (Set<DocumentId> docIds : dependencyMap.values()) {
        for (DocumentId d : docIds) {
          queryMap.computeIfAbsent(d.getSourceName(), k -> new BoxQuery()).getIds().add(d.getId());
        }
      }

      // get dependencies
      Map<DocumentId, BoxDocument> depMap = new HashMap<>();

      for (String depSourceName : queryMap.keySet()) {
        List<BoxDocument> dependencies;

        try {
          dependencies = handler.find(depSourceName, queryMap.get(depSourceName));
        } catch (Exception e) {
          throw new RuntimeException(
              "failed to retrieve dependencies from source " + depSourceName);
        }

        for (BoxDocument d : dependencies) {

          if (d.getMessage() != null) {
            throw new RuntimeException(
                "failed to retrieve dependency " + depSourceName + "." + d.getId());
          }

          // only include processed documents
          if (d.isProcessed()) {
            depMap.put(new DocumentId(depSourceName, d.getId()), d);
          }
        }
      }

      m.mark("getDependencies");

      // prepare contexts
      List<ProcessContext> contexts = new ArrayList<>();

      for (String id : ids) {
        Map<DocumentId, BoxDocument> dependencies = new LinkedHashMap<>();

        Set<DocumentId> registeredDependencies =
            dependencyMap.getOrDefault(id, Collections.emptySet());

        for (DocumentId registeredDependency : registeredDependencies) {
          BoxDocument dependency = depMap.get(registeredDependency);

          if (dependency != null) {
            dependencies.put(registeredDependency, dependency);
          }
        }

        contexts.add(new ProcessContext(id, dependencies));
      }

      ProcessBatch batch = new ProcessBatch(contexts);

      // process documents
      ProcessResult result = source.getProcessor().process(batch);
      List<BoxDocument> processResults = result;

      m.mark("processBatch");

      // save results
      processResults.forEach(d -> registry.getDocumentHandler().save(source.getName(), d));

      m.mark("saveBatch");

      averageBatchSize.add(batch.size());

      // prepare returned results
      // num results needs to match ids argument

      Map<String, BoxDocument> resultMap = new HashMap<>();
      processResults.forEach(
          d -> {
            // only include real results
            if (!(d instanceof StartGroupDocument || d instanceof EndGroupDocument)) {
              resultMap.put(d.getId(), d);
            }
          });

      for (String id : ids) {
        if (resultMap.containsKey(id)) {
          results.add(new BoxDocument(resultMap.get(id)));
        } else {
          results.add(new BoxDocument(id));
        }
      }

      numBatches.addAndGet(batch.size());
      numDocuments.addAndGet(processResults.size());

      m.mark("prepareResults");

    } catch (Exception e) {
      logger.error(
          "Error while processing " + source.getName() + ":" + ids + ": " + e.toString(), e);

      for (String id : ids) {
        BoxDocument errorDocument = new BoxDocument(id);
        errorDocument.setAsError(e.toString());
        results.add(errorDocument);
      }
    }

    return results;
  }

  /**
   * Prepares a snapshot of timing statistics for the processor.
   *
   * @return
   */
  public Map<String, Benchmark.Data> getStats() {
    Map<String, Benchmark.Data> benchmarks = new LinkedHashMap<>();

    for (Object label : t.getLabels()) {
      benchmarks.put((String) label, t.getStatistic(label).snapshot());
    }

    return benchmarks;
  }

  /** @return the averageBatchSize */
  public double getAverageBatchSize() {
    return averageBatchSize.get();
  }

  /** @return the numDocuments */
  public long getNumDocuments() {
    return numDocuments.get();
  }

  /** @return the numBatches */
  public long getNumBatches() {
    return numBatches.get();
  }

  @Override
  public void close() throws Exception {
    shutdown = true;

    /* temporary hack, there must be a race condition possibly in the BatchExecutorService that is
     * preventing the executor from shutting down under heavy load, clearing the internal queue first
     * eliminates this condition
     */
    executor.getQueue().clear();

    executor.shutdownAndWait();
  }
}
