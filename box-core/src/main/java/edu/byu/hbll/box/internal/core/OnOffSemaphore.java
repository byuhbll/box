/** */
package edu.byu.hbll.box.internal.core;

import java.time.Instant;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/** */
public class OnOffSemaphore {

  /** */
  private Semaphore on;

  /** @return */
  public boolean isOn() {
    return on == null || on.availablePermits() > 0;
  }

  /**
   * @param timeout
   * @param unit
   * @return
   * @throws InterruptedException
   */
  public boolean awaitOn(long timeout, TimeUnit unit) throws InterruptedException {
    synchronized (this) {
      if (this.on == null) {
        return true;
      }

      boolean on = this.on.tryAcquire(timeout, unit);

      if (on) {
        this.on.release();
      }

      return on;
    }
  }

  /**
   * @param on
   * @return
   */
  boolean setOn(boolean on) {
    synchronized (this) {
      if (this.on == null) {
        this.on = new Semaphore(0);
      }

      if (on) {
        this.on.release();
      } else {
        this.on.drainPermits();
      }
    }

    return false;
  }

  /**
   * Only initialize if not already initialized.
   *
   * @param nextOn
   * @param nextOff
   */
  void init(Instant nextOn, Instant nextOff) {
    synchronized (this) {
      if (this.on == null) {
        if (nextOn.isBefore(nextOff)) {
          setOn(false);
        } else {
          setOn(true);
        }
      }
    }
  }
}
