/** */
package edu.byu.hbll.box.internal.core;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.inject.Inject;
import edu.byu.hbll.box.BoxHealth;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.Source;
import edu.byu.hbll.box.SourceHealth;

/** @author Charles Draper */
@Singleton
public class BoxHealthCheck {

  @Inject private Registry registry;

  private BoxHealth boxHealth = new BoxHealth(false, Collections.unmodifiableMap(new HashMap<>()));

  @Schedule(second = "*/10", minute = "*", hour = "*", persistent = false)
  public void timeout() {
    boolean healthy = true;
    Map<String, SourceHealth> sourceHealthMap = new HashMap<>();

    for (Source source : registry.getSources()) {
      try {
        registry.getDocumentHandler().find(source.getName(), new BoxQuery().setLimit(1));
        sourceHealthMap.put(source.getName(), new SourceHealth(source.getName(), true, null));
      } catch (Exception e) {
        sourceHealthMap.put(
            source.getName(), new SourceHealth(source.getName(), false, e.getMessage()));
        healthy = false;
      }
    }

    sourceHealthMap = Collections.unmodifiableMap(sourceHealthMap);

    this.boxHealth = new BoxHealth(healthy, sourceHealthMap);
  }

  public BoxHealth getHealth() {
    return boxHealth;
  }
}
