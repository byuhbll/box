/** */
package edu.byu.hbll.box.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxConfigurable;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.Facet;
import edu.byu.hbll.box.InitConfig;
import edu.byu.hbll.box.MetadataLevel;
import edu.byu.hbll.box.QueryResult;
import edu.byu.hbll.box.Source;
import edu.byu.hbll.box.client.BoxClient;

/**
 * A view that translates a facet value into an id and includes all documents with that facet in the
 * final document.
 *
 * <p>IMPORTANT: {@link FacetView} has some weaknesses. First, you cannot force the process of
 * upstream documents based on a facet. A facet is one-way. Documents get tagged with a facet once
 * they've been processed another way. Second, when a document drops its facet value recognized by
 * this view, it is impossible to know which group it once belonged to in order to signal the
 * reprocess of the group. Therefore it is good practice to occasionally reprocess documents
 * dependent on this view. Third, all documents corresponding to a facet id are brought in. If there
 * are many many documents per facet id, the resulting facet documents could be too large to fit in
 * memory.
 *
 * @author Charles Draper
 */
public class FacetView extends View {

  private Source source;
  private String facetName;
  private int documentLimit;
  private MetadataLevel metadataLevel;

  protected FacetView() {}

  /**
   * @param boxClient the client for communicating with the remote box
   * @param facetName the facet group name to use as the id for documents
   */
  public FacetView(BoxClient boxClient, String facetName) {
    super(boxClient);
    this.facetName = facetName;
  }

  /**
   * See {@link BoxConfigurable#postConstruct(InitConfig)}.
   *
   * @param config
   */
  public FacetView(InitConfig config) {
    postConstruct(config);
  }

  @Override
  public void postConstruct(InitConfig config) {
    super.postConstruct(config);
    this.source = config.getSource();
    ObjectNode params = config.getParams();
    this.facetName = Objects.requireNonNull(params.path("facetName").asText(null));
    this.documentLimit = params.path("documentLimit").asInt(Integer.MAX_VALUE);
    this.metadataLevel =
        MetadataLevel.valueOf(
            params.path("metadataLevel").asText(MetadataLevel.CORE.name()).toUpperCase());
  }

  @Override
  public QueryResult rawFind(BoxQuery query) {
    Set<String> facetValues = new LinkedHashSet<>();

    long upstreamCursor = query.getCursor();

    if (query.isHarvest()) {
      // find candidate documents
      BoxQuery seedQuery = new BoxQuery(query);
      seedQuery.setLimit(getLimit());
      seedQuery.setMetadataLevel(MetadataLevel.FULL);
      seedQuery.setMetadataOnly(true);

      long limit = query.getLimit();
      limit = limit == -1 ? source.getDefaultLimit() : limit;
      boolean more = true;
      long nextCursor = query.getCursor();

      while (more) {
        QueryResult response = super.rawFind(seedQuery.setCursor(nextCursor));

        nextCursor = response.getNextCursor();
        more = !response.isEmpty();

        for (BoxDocument document : response) {
          // must get all the facets from a document even if it pushes us over the limit in order
          // for the cursor to work out in subsequent calls
          if (facetValues.size() < limit) {
            for (Facet facet : document.getFacets()) {
              if (facet.getName().equals(facetName)) {
                facetValues.add(facet.getValue());
                upstreamCursor = document.getCursor();
              }
            }
          }
        }

        more = more && facetValues.size() < limit;
      }
    } else {
      query.getIds().forEach(i -> facetValues.add(i));
    }

    BoxQuery facetQuery = new BoxQuery().setFields(query.getFields());

    QueryResult result = new QueryResult();

    if (facetValues.isEmpty()) {
      // do nothing
    } else if (documentLimit < 0) {
      facetValues.forEach(f -> facetQuery.addFacet(facetName, f));
      result = super.rawFind(facetQuery, true);
    } else {
      // process each facet one at a time if documentLimit being used
      for (String facetValue : facetValues) {
        BoxQuery singleFacetQuery = new BoxQuery(facetQuery);
        singleFacetQuery.addFacet(facetName, facetValue);
        QueryResult singleResult = super.rawFind(singleFacetQuery, documentLimit);
        result.addAll(singleResult);
      }
    }

    Map<Facet, List<BoxDocument>> documentMap = new LinkedHashMap<>();

    // group documents
    for (BoxDocument document : result) {
      for (Facet facet : document.getFacets()) {
        documentMap.computeIfAbsent(facet, k -> new ArrayList<>()).add(document);
      }
    }

    QueryResult finalResult = new QueryResult();
    finalResult.setNextCursor(upstreamCursor + 1);

    for (String facetValue : facetValues) {
      BoxDocument facetDocument = new BoxDocument(facetValue);

      ArrayNode documents = facetDocument.withDocument().withArray("documents");
      Facet facet = new Facet(facetName, facetDocument.getId());

      for (BoxDocument document : documentMap.getOrDefault(facet, Collections.emptyList())) {
        documents.add(document.toJson(metadataLevel));
      }

      if (documents.size() == 0) {
        facetDocument.getDocument().removeAll();
        facetDocument.setAsDeleted();
      }

      finalResult.add(facetDocument);
    }
    
    return finalResult;
  }
}
