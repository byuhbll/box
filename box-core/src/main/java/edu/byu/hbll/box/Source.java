/** */
package edu.byu.hbll.box;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javax.ejb.ScheduleExpression;
import edu.byu.hbll.box.impl.MemoryDatabase;
import edu.byu.hbll.box.internal.util.ScheduleParser;

/** */
public class Source {
  
  /**
   * Always points to the principal source.
   */
  public static final String SOURCE_NAME_BOX = "box";
  
  /**
   * For participating operations, this refers to all sources. 
   */
  public static final String SOURCE_NAME_ALL = "all";
  
  public static final Set<String> RESERVED_SOURCE_NAMES =
      Collections.unmodifiableSet(new HashSet<>(Arrays.asList(SOURCE_NAME_BOX, SOURCE_NAME_ALL)));

  private String name;

  private boolean enabled = true;

  private boolean principal;

  private Processor processor;

  private Duration batchDelay = Duration.ZERO;

  private int batchCapacity = 1;

  private int threadCount = 1;

  private Duration suspend = Duration.ZERO;

  private int quota;

  private ScheduleExpression quotaReset = ScheduleParser.parse("0 0 0 * * * *");

  private ScheduleExpression on;

  private ScheduleExpression off;

  private boolean processEnabled = true;

  private Harvester harvester;

  private boolean harvestEnabled = true;

  private ScheduleExpression harvestSchedule = ScheduleParser.parse("0 * * * * * *");

  private ScheduleExpression harvestResetSchedule;

  private boolean saveDeleted = false;

  private Duration reprocessAge;

  private ScheduleExpression reprocessSchedule = ScheduleParser.parse("0 * * * * * *");

  private Duration removeAge;

  private ScheduleExpression removeSchedule = ScheduleParser.parse("0 * * * * * *");

  private String idField;

  private Map<String, Set<String>> facetFields = new LinkedHashMap<>();

  private boolean save = true;

  private boolean process = false;

  private boolean dependencyOnly = false;

  private int defaultLimit = 10;

  private BoxDatabase db = new MemoryDatabase();

  private BoxDatabase cursorDb;

  private List<BoxConfigurable> others = new ArrayList<>();

  private boolean overwrite;

  /** @param name the name of the source */
  public Source(String name) {
    
    if(RESERVED_SOURCE_NAMES.contains(name)) {
      throw new IllegalArgumentException(name + " is a reserved source name");
    }
    
    this.name = name;
  }

  /**
   * Whether to enable or disable this source. A disabled source behaves like it doesn't even exist.
   * Default true.
   *
   * @param enabled true if enabled, false if disabled
   * @return this
   */
  public Source setEnabled(boolean enabled) {
    this.enabled = enabled;
    return this;
  }

  /**
   * Whether to mark this source as the principal source or in other words the main source of the
   * box. Also some apis are simplified for the principal source because no source name has to be
   * specified. If no source is explicitly marked as principal, the first one added to the
   * configuration will be considered as principal. Default false.
   *
   * @param principal true if principal, otherwise false
   * @return this
   */
  public Source setPrincipal(boolean principal) {
    this.principal = principal;
    return this;
  }

  /**
   * The processor to use for this source. Setting this enables direct processing of documents.
   *
   * @param processor
   * @return this
   */
  public Source setProcessor(Processor processor) {
    this.processor = processor;
    return this;
  }

  /**
   * How long to wait before running a batch at less than capacity. Only valid for the processor.
   * Default Duration.ZERO.
   *
   * @param batchDelay
   * @return this
   */
  public Source setBatchDelay(Duration batchDelay) {
    this.batchDelay = Objects.requireNonNull(batchDelay);
    return this;
  }

  /**
   * The maximum size a batch can grow before processing occurs. Default 1.
   *
   * @param batchCapacity
   * @return this
   */
  public Source setBatchCapacity(int batchCapacity) {
    this.batchCapacity = batchCapacity;
    return this;
  }

  /**
   * How many threads should be processing batches. Default 1.
   *
   * @param threadCount
   * @return this
   */
  public Source setThreadCount(int threadCount) {
    this.threadCount = threadCount;
    return this;
  }

  /**
   * Whether or not to enable or disable this processor. If disabled, this source behaves as if it
   * doesn't have a processor. Default true.
   *
   * @param processEnabled
   * @return this
   */
  public Source setProcessEnabled(boolean processEnabled) {
    this.processEnabled = processEnabled;
    return this;
  }

  /**
   * The harvester to use for this source. Setting this enables harvesting of documents. Default
   * null.
   *
   * @param harvester
   * @return this
   */
  public Source setHarvester(Harvester harvester) {
    this.harvester = harvester;
    return this;
  }

  /**
   * Whether or not to enable or disable this harvester. If disabled, this source behaves as if it
   * doesn't have a harvester. Default true.
   *
   * @param harvestEnabled
   * @return this
   */
  public Source setHarvestEnabled(boolean harvestEnabled) {
    this.harvestEnabled = harvestEnabled;
    return this;
  }

  /**
   * A cron-like schedule of when to run the harvester. Default every minute.
   *
   * @param harvestSchedule
   * @return this
   */
  public Source setHarvestSchedule(ScheduleExpression harvestSchedule) {
    this.harvestSchedule = harvestSchedule;
    return this;
  }

  /**
   * A cron-like schedule of when to reset the harvester and cause it to start from the very
   * beginning. Default null (never).
   *
   * @param harvestResetSchedule
   * @return this
   */
  public Source setHarvestResetSchedule(ScheduleExpression harvestResetSchedule) {
    this.harvestResetSchedule = harvestResetSchedule;
    return this;
  }

  /**
   * Save new deleted documents during harvests. Default false.
   *
   * @return this
   */
  public Source setSaveDeleted() {
    return setSaveDeleted(true);
  }

  /**
   * Whether or not to save new deleted documents during harvests. Default false.
   *
   * @param saveDeleted
   * @return this
   */
  public Source setSaveDeleted(boolean saveDeleted) {
    this.saveDeleted = saveDeleted;
    return this;
  }

  /**
   * How long to suspend processing threads after each batch. Default Duration.ZERO.
   *
   * @param suspend
   * @return this
   */
  public Source setSuspend(Duration suspend) {
    this.suspend = suspend;
    return this;
  }

  /**
   * Stop processing when quota is reached. Default 0 (no quota).
   *
   * @param quota
   * @return this
   */
  public Source setQuota(int quota) {
    this.quota = quota;
    return this;
  }

  /**
   * When to reset the processed count to zero to allow more processing to occur until the quota is
   * reached again. Default midnight each day.
   *
   * @param quotaReset
   * @return this
   */
  public Source setQuotaReset(ScheduleExpression quotaReset) {
    this.quotaReset = quotaReset;
    return this;
  }

  /**
   * When to turn on processing for a window of time. Default null (always on).
   *
   * @param on
   * @return this
   */
  public Source setOn(ScheduleExpression on) {
    this.on = on;
    return this;
  }

  /**
   * When to turn of processing for a window of time. Default null (always on).
   *
   * @param off
   * @return this
   */
  public Source setOff(ScheduleExpression off) {
    this.off = off;
    return this;
  }

  /**
   * Reprocess old documents that haven't been processed for this long. Default null (never).
   *
   * @param reprocessAge
   * @return this
   */
  public Source setReprocessAge(Duration reprocessAge) {
    this.reprocessAge = reprocessAge;
    return this;
  }

  /**
   * When to go looking for old documents to reprocess. Default every minute.
   *
   * @param reprocessSchedule
   * @return this
   */
  public Source setReprocessSchedule(ScheduleExpression reprocessSchedule) {
    this.reprocessSchedule = reprocessSchedule;
    return this;
  }

  /**
   * Remove deleted documents that have been deleted for this long. Default null (never).
   *
   * @param removeAge
   * @return this
   */
  public Source setRemoveAge(Duration removeAge) {
    this.removeAge = removeAge;
    return this;
  }

  /**
   * When to run the remove process. Default every minute.
   *
   * @param removeSchedule
   * @return this
   */
  public Source setRemoveSchedule(ScheduleExpression removeSchedule) {
    this.removeSchedule = removeSchedule;
    return this;
  }

  /**
   * Each time a document is saved, fields that match this dot-notated path will be added as a facet
   * with the given name.
   *
   * @param name
   * @param fieldPath
   * @return this
   */
  public Source addFacetField(String name, String fieldPath) {
    return addFacetField(name, Arrays.asList(fieldPath));
  }

  /**
   * Each time a document is saved, fields that match the dot-notated paths will be added as facets
   * with the given name.
   *
   * @param name
   * @param fieldPaths
   * @return this
   */
  public Source addFacetFields(String name, String... fieldPaths) {
    return addFacetField(name, Arrays.asList(fieldPaths));
  }

  /**
   * Each time a document is saved, fields that match the dot-notated paths will be added as facets
   * with the given name.
   *
   * @param name
   * @param fieldPaths
   * @return this
   */
  public Source addFacetField(String name, Collection<String> fieldPaths) {
    this.facetFields.computeIfAbsent(name, k -> new LinkedHashSet<>()).addAll(fieldPaths);
    return this;
  }

  /**
   * Whether or not to save documents to the database. This is important for sources that need to
   * always process in real-time. Default true.
   *
   * @param save
   * @return this
   */
  public Source setSave(boolean save) {
    this.save = save;
    return this;
  }

  /**
   * Whether or not to always process documents synchronously in real-time. Default false.
   *
   * @param process
   * @return this
   */
  public Source setProcess(boolean process) {
    this.process = process;
    return this;
  }

  /**
   * Whether or not this source is only a dependency for other sources. When set to true, documents
   * from this source are only saved if they are dependencies for other sources. Default false.
   *
   * @param dependencyOnly
   * @return this
   */
  public Source setDependencyOnly(boolean dependencyOnly) {
    this.dependencyOnly = dependencyOnly;
    return this;
  }

  /**
   * The default limit of documents to return for harvest type queries. Default 10.
   *
   * @param defaultLimit
   * @return this
   */
  public Source setDefaultLimit(int defaultLimit) {
    this.defaultLimit = defaultLimit;
    return this;
  }

  /**
   * Use this database for storing documents and other info for Box. Databases are source specific
   * so do not use the same one for multiple sources. Default MemoryDatabase.
   *
   * @param db
   * @return this
   */
  public Source setDb(BoxDatabase db) {
    this.db = db;
    return this;
  }

  /**
   * In the case that the database is read-only database, it's still important to know where a
   * harvest left off. This will override the regular db if set. Default null.
   *
   * @param cursorDb
   * @return this
   */
  public Source setCursorDb(BoxDatabase cursorDb) {
    this.cursorDb = cursorDb;
    return this;
  }

  /**
   * If there is any other extension class that you want Box to manage.
   *
   * @param other
   * @return this
   */
  public Source addOther(BoxConfigurable other) {
    this.others.add(other);
    return this;
  }

  /** @return the name */
  public String getName() {
    return name;
  }

  /** @return the enabled */
  public boolean isEnabled() {
    return enabled;
  }

  /** @return the principal */
  public boolean isPrincipal() {
    return principal;
  }

  /** @return the processor */
  public Processor getProcessor() {
    return processor;
  }

  /** @return the batchDelay */
  public Duration getBatchDelay() {
    return batchDelay;
  }

  /** @return the batchCapacity */
  public int getBatchCapacity() {
    return batchCapacity;
  }

  /** @return the threadCount */
  public int getThreadCount() {
    return threadCount;
  }

  /** @return the suspend */
  public Duration getSuspend() {
    return suspend;
  }

  /** @return the quota */
  public int getQuota() {
    return quota;
  }

  /** @return the quotaReset */
  public ScheduleExpression getQuotaReset() {
    return quotaReset;
  }

  /** @return the on */
  public ScheduleExpression getOn() {
    return on;
  }

  /** @return the off */
  public ScheduleExpression getOff() {
    return off;
  }

  /** @return the processEnabled */
  public boolean isProcessEnabled() {
    return processEnabled;
  }

  /** @return the harvestProcessor */
  public Harvester getHarvester() {
    return harvester;
  }

  /** @return the harvestEnabled */
  public boolean isHarvestEnabled() {
    return harvestEnabled;
  }

  /** @return the harvestSchedule */
  public ScheduleExpression getHarvestSchedule() {
    return harvestSchedule;
  }

  /** @return the harvestResetSchedule */
  public ScheduleExpression getHarvestResetSchedule() {
    return harvestResetSchedule;
  }

  /** @return the saveDeleted */
  public boolean isSaveDeleted() {
    return saveDeleted;
  }

  /** @return the reprocessAge */
  public Duration getReprocessAge() {
    return reprocessAge;
  }

  /** @return the reprocessSchedule */
  public ScheduleExpression getReprocessSchedule() {
    return reprocessSchedule;
  }

  /** @return the removeAge */
  public Duration getRemoveAge() {
    return removeAge;
  }

  /** @return the removeSchedule */
  public ScheduleExpression getRemoveSchedule() {
    return removeSchedule;
  }

  /**
   * Getter for the idField.
   *
   * @return String idField
   */
  public String getIdField() {
    return idField;
  }

  /** @param idField the idField to set */
  public void setIdField(String idField) {
    this.idField = idField;
  }

  /** @return the facetFields */
  public Map<String, Set<String>> getFacetFields() {
    return facetFields;
  }

  /** @return the save */
  public boolean isSave() {
    return save;
  }

  /** @return the process */
  public boolean isProcess() {
    return process;
  }

  /** @return the dependencyOnly */
  public boolean isDependencyOnly() {
    return dependencyOnly;
  }

  /** @return the defaultLimit */
  public int getDefaultLimit() {
    return defaultLimit;
  }

  /** @return */
  @Override
  public String toString() {
    return name;
  }

  /** @return the db */
  public BoxDatabase getDb() {
    return db;
  }

  /** @return the cursorDb */
  public BoxDatabase getCursorDb() {
    return cursorDb;
  }

  /** @return the others */
  public List<BoxConfigurable> getOthers() {
    return others;
  }

  /** @return cursorDb if not null, otherwise db */
  public BoxDatabase getPreferredCursorDb() {
    return cursorDb != null ? cursorDb : db;
  }

  /** @return the overwrite */
  public boolean isOverwrite() {
    return overwrite;
  }

  /**
   * Whether or not to overwrite existing documents on every save attempt. If false Box will detect
   * if a saved document is different than the currently processed or harvested document. If the
   * document has not changed since last time, the document will not be overwritten and only the
   * processed date is updated. The document keeps its current modified date and cursor. When set to
   * true, the document is always detected as modified and the full save process will occur. Default
   * false.
   *
   * <p>Note: Even with this set to true, unprocessed documents will never overwrite processed
   * documents.
   *
   * @param overwrite the overwrite to set
   */
  public Source setOverwrite(boolean overwrite) {
    this.overwrite = overwrite;
    return this;
  }
}
