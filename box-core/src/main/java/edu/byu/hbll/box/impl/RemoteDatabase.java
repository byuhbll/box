/** */
package edu.byu.hbll.box.impl;

import java.util.Objects;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.InitConfig;
import edu.byu.hbll.box.QueryResult;
import edu.byu.hbll.box.ReadOnlyDatabase;
import edu.byu.hbll.box.client.BoxClient;
import edu.byu.hbll.box.client.internal.util.BasicAuthenticationtFilter;

/**
 * A read only database that pulls all documents from a remote Box using HTTP.
 *
 * @author Charles Draper
 */
public class RemoteDatabase implements ReadOnlyDatabase {

  static final Logger logger = LoggerFactory.getLogger(RemoteDatabase.class);

  private BoxClient boxClient;
  private ObjectNode cursor = JsonNodeFactory.instance.objectNode();

  private String sourceName;

  @Override
  public void postConstruct(InitConfig config) {
    ObjectNode params = config.getParams();
    String uri = Objects.requireNonNull(params.get("uri").asText(null));
    Client client = ClientBuilder.newClient();

    String username = params.path("username").asText(null);
    String password = params.path("password").asText(null);

    if (username != null && password != null) {
      client =
          ClientBuilder.newClient().register(new BasicAuthenticationtFilter(username, password));
    }

    boxClient = new BoxClient(uri, client);
  }

  @Override
  public QueryResult find(BoxQuery query) {
    return boxClient.find(query);
  }

  @Override
  public ObjectNode getHarvestCursor() {
    return cursor.deepCopy();
  }

  @Override
  public void setHarvestCursor(ObjectNode cursor) {
    this.cursor = cursor.deepCopy();
  }

  /** @return the sourceName */
  public String getSourceName() {
    return sourceName;
  }

  /** @param sourceName the sourceName to set */
  public void setSourceName(String sourceName) {
    this.sourceName = sourceName;
  }
}
