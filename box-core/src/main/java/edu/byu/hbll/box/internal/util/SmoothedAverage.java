/** */
package edu.byu.hbll.box.internal.util;

/** @author Charles Draper */
public class SmoothedAverage {

  public static final double DEFAULT_SMOOTHING_FACTOR = 0.1;

  private double average = 0;
  private double smoothingFactor = DEFAULT_SMOOTHING_FACTOR;
  private boolean initialized = false;

  public SmoothedAverage() {}

  public SmoothedAverage(double smoothingFactor) {
    this.smoothingFactor = smoothingFactor;
  }

  public void add(double addend) {
    synchronized (this) {
      if (initialized) {
        average = smoothingFactor * addend + (1 - smoothingFactor) * average;
      } else {
        average = addend;
        initialized = true;
      }
    }
  }

  public double get() {
    synchronized (this) {
      return average;
    }
  }
}
