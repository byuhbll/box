/** */
package edu.byu.hbll.box.internal.core;

import edu.byu.hbll.box.BoxConfigurable;

/** */
@FunctionalInterface
public interface BoxRunnable extends BoxConfigurable {

  /** */
  public boolean run() throws Exception;
}
