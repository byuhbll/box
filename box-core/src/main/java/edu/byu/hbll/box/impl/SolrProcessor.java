package edu.byu.hbll.box.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.ProcessBatch;
import edu.byu.hbll.box.ProcessContext;
import edu.byu.hbll.box.ProcessResult;
import edu.byu.hbll.box.Processor;
import edu.byu.hbll.solr.SolrDocumentBuilder;

public class SolrProcessor implements Processor {

  private static final Logger logger = LoggerFactory.getLogger(SolrProcessor.class);

  public String solrUrl = "http://localhost:8983/solr/solr";

  private final String idField = "id";

  private SolrDocumentBuilder solrDocBuilder = new SolrDocumentBuilder();

  @Override
  public ProcessResult process(ProcessBatch batch) {
    SolrClient solr = new HttpSolrClient.Builder(solrUrl).build();
    List<BoxDocument> docs = new ArrayList<>();
    batch.forEach(c -> docs.addAll(handleContext(c, solr)));
    return new ProcessResult().addDocuments(docs);
  }

  /**
   * Method which pushes the input documents to solr. Then returns the result documents list.
   *
   * @param context {@link ProcessContext}.
   * @param solr {@link SolrClient}
   * @return List of {@link BoxDocument}
   */
  private List<BoxDocument> handleContext(ProcessContext context, SolrClient solr) {
    List<SolrInputDocument> solrDocs =
        context
            .getDependencies()
            .values()
            .stream()
            .map(sd -> generateSolrDoc(sd.getDocument()))
            .collect(Collectors.toList());
    try {
      if (!solrDocs.isEmpty()) {
        solr.add(solrDocs);
        logger.info("Documents added to solr");
      }
    } catch (SolrServerException | IOException e) {
      logger.error("Unable to add documents to solr.", e);
    }
    return context
        .getDependencies()
        .values()
        .stream()
        .map(sd -> new BoxDocument(sd.getId()))
        .collect(Collectors.toList());
  }

  /**
   * Method which generates {@link SolrInputDocument}s from an objectNode.
   *
   * @param node {@link ObjectNode}
   * @return {@link SolrInputDocument}
   */
  private SolrInputDocument generateSolrDoc(ObjectNode node) {
    String id = node.path(idField).asText();
    SolrInputDocument doc = solrDocBuilder.buildSolrInputDocument(node);
    doc.addField("id", id);
    return doc;
  }
}
