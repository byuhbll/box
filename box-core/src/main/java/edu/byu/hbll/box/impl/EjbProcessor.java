/** */
package edu.byu.hbll.box.impl;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import edu.byu.hbll.box.ProcessBatch;
import edu.byu.hbll.box.ProcessResult;
import edu.byu.hbll.box.Processor;

/**
 * A processor that looks up another processor EJB and uses the EJB as the main processor.
 *
 * @author Charles Draper
 */
public class EjbProcessor implements Processor {

  /** */
  private Processor processor;

  /**
   * @param processorClass
   * @param mappedName
   * @throws NamingException
   */
  public EjbProcessor(Class<? extends Processor> processorClass, String mappedName)
      throws NamingException {
    InitialContext initialContext = new InitialContext();

    if (mappedName == null || mappedName.isEmpty()) {
      String appName = (String) initialContext.lookup("java:app/AppName");
      mappedName = "java:global/" + appName + "/" + processorClass.getSimpleName();
    }

    this.processor = (Processor) initialContext.lookup(mappedName);
  }

  /**
   * @param batch
   * @return
   */
  @Override
  public ProcessResult process(ProcessBatch batch) {
    return processor.process(batch);
  }
}
