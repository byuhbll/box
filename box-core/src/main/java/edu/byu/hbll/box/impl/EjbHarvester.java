/** */
package edu.byu.hbll.box.impl;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import edu.byu.hbll.box.HarvestContext;
import edu.byu.hbll.box.HarvestResult;
import edu.byu.hbll.box.Harvester;

/**
 * A harvester that looks up another harvester EJB and uses the EJB as the main harvester.
 *
 * @author Charles Draper
 */
public class EjbHarvester implements Harvester {

  /** */
  private Harvester harvestProcessor;

  /**
   * @param harvestProcessorClass
   * @param mappedName
   * @throws NamingException
   */
  public EjbHarvester(Class<? extends Harvester> harvestProcessorClass, String mappedName)
      throws NamingException {
    InitialContext initialContext = new InitialContext();

    if (mappedName == null || mappedName.isEmpty()) {
      String appName = (String) initialContext.lookup("java:app/AppName");
      mappedName = "java:global/" + appName + "/" + harvestProcessorClass.getSimpleName();
    }

    this.harvestProcessor = (Harvester) new InitialContext().lookup(mappedName);
  }

  /**
   * @param context
   * @return
   */
  @Override
  public HarvestResult harvest(HarvestContext context) {
    return harvestProcessor.harvest(context);
  }
}
