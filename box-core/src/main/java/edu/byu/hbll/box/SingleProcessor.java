/** */
package edu.byu.hbll.box;

/**
 * A {@link Processor} that only processes one id/context at a time.
 *
 * @author Charles Draper
 */
public interface SingleProcessor extends Processor {

  /**
   * Processes a single context (ie, no batching). Important: Be sure the batch capacity is set to
   * one or Box will execute the batch sequentially.
   *
   * @param context the context containing the id, dependencies and other info for processing
   * @return the result of processing the id including newly processed documents to be saved
   */
  ProcessResult process(ProcessContext context);

  @Override
  default ProcessResult process(ProcessBatch batch) {
    ProcessResult result = new ProcessResult();

    for (ProcessContext context : batch) {
      result.addDocuments(process(context));
    }

    return result;
  }
}
