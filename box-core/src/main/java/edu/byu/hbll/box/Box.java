/** */
package edu.byu.hbll.box;

import java.io.OutputStream;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import com.fasterxml.jackson.databind.JsonNode;
import edu.byu.hbll.box.internal.core.BoxHealthCheck;
import edu.byu.hbll.box.internal.core.Registry;

/**
 * The principal API into the Box system. Start by injecting this class and executing initialize.
 * Once initialized, Box is ready for use.
 *
 * <pre>
 * {@literal @}Inject Box box;
 * </pre>
 *
 * @author Charles Draper
 */
@ApplicationScoped
public class Box {

  @Inject private Registry registry;

  @Inject private BoxHealthCheck health;

  private volatile boolean initialized;

  /**
   * Initializes Box according to the given configuration. Box should only be initialized once and
   * prior to other API calls. Any subsequent calls to initialize or other initializing methods will
   * be ignored.
   *
   * @param config the configuration to use to initialize Box
   */
  public void initialize(BoxConfiguration config) {
    synchronized (this) {
      if (!initialized) {
        registry.set(this, config);
        initialized = true;
      }
    }
  }

  /**
   * Initializes Box according to the configuration in the given {@link JsonNode}. Box should only
   * be initialized once and prior to other API calls. Any subsequent calls to initialize or other
   * initializing methods will be ignored.
   *
   * @param config the configuration to use to initialize Box
   */
  public void initialize(JsonNode config) {
    initialize(new BoxConfiguration(this, config));
  }

  /**
   * Finds and returns documents according to the given query for the principal source.
   *
   * @param query the query
   * @return found documents
   */
  public QueryResult find(BoxQuery query) {
    return find(registry.getPrincipalSource().getName(), query);
  }

  /**
   * Returns documents according to the given query for the given source.
   *
   * @param sourceName the source to query
   * @param query the query
   * @return found documents
   */
  public QueryResult find(String sourceName, BoxQuery query) {
    return registry.getDocumentHandler().find(sourceName, query);
  }

  /**
   * Finds and writes documents as UTF-8 JSON to the given {@link OutputStream} according to the
   * given query for the principal source. Box streams the documents so large very large limits are
   * reasonable.
   *
   * <p>If out is null, results are returned as normal.
   *
   * <p>Note: Writing to an output stream for ID type queries is currently not supported and will
   * throw an exception.
   *
   * @param query the query
   * @param out the output stream
   * @return an empty result set with the nextCursor populated
   * @throws UnsupportedOperationException if query is of type ID
   */
  public QueryResult find(BoxQuery query, OutputStream out) throws Exception {
    return find(registry.getPrincipalSource().getName(), query, out);
  }

  /**
   * Finds and writes documents as UTF-8 JSON to the given {@link OutputStream} according to the
   * given query for the given source. Box streams the documents so large very large limits are
   * reasonable.
   *
   * <p>If out is null, results are returned as normal.
   *
   * <p>Note: Writing to an output stream for ID type queries is currently not supported and will
   * throw an exception.
   *
   * @param sourceName the source to query
   * @param query the query
   * @param out the output stream
   * @return an empty result set with the nextCursor populated
   * @throws UnsupportedOperationException if query is of type ID
   */
  public QueryResult find(String sourceName, BoxQuery query, OutputStream out) {
    return registry.getDocumentHandler().find(sourceName, query, out);
  }

  /**
   * Saves the given document to the database under the principal source. The document is only saved
   * and subsequently the modified and cursor fields are only updated if it is new or has been
   * updated.
   *
   * @param document the document to save
   */
  public void save(BoxDocument document) {
    save(registry.getPrincipalSource().getName(), document);
  }

  /**
   * Saves the given document to the database under the given source. The document is only saved and
   * subsequently the modified and cursor fields are only updated if it is new or has been updated.
   *
   * @param sourceName the name of the source to use
   * @param document the document to save
   */
  public void save(String sourceName, BoxDocument document) {
    registry.getDocumentHandler().save(sourceName, document);
  }

  /**
   * Registers a listener with the updates notification system for the principal source. The update
   * notification system executes all registered listeners at most once per second whenever there is
   * an update within a source.
   *
   * @param listener the runnable to execute when there's an update detected
   */
  public void registerForUpdateNotifications(Runnable listener) {
    registerForUpdateNotifications(registry.getPrincipalSource().getName(), listener);
  }

  /**
   * Registers a listener with the updates notification system for the given source. The update
   * notification system executes all registered listeners at most once per second whenever there is
   * an update within a source.
   *
   * @param sourceName the source to listen to
   * @param listener the runnable to execute when there's an update detected
   */
  public void registerForUpdateNotifications(String sourceName, Runnable listener) {
    registry.getUpdatesNotifier().register(sourceName, listener);
  }

  /** Triggers the harvester of the principal source to run. */
  public void triggerHarvest() {
    triggerHarvest(registry.getPrincipalSource().getName());
  }

  /**
   * Triggers the harvester of the given source to run.
   *
   * @param sourceName trigger this source
   */
  public void triggerHarvest(String sourceName) {
    registry.triggerHarvest(sourceName);
  }

  /**
   * Returns a snapshot of the health of the Box system.
   *
   * @return the health of Box
   */
  public BoxHealth getHealth() {
    return health.getHealth();
  }

  /**
   * Returns the registered principal source.
   *
   * <p>IMPORTANT: Once a source is registered it should not be modified.
   *
   * @return the registered principal source
   */
  public Source getSource() {
    return registry.getPrincipalSource();
  }

  /**
   * Returns the registered source denoted by the name.
   *
   * <p>IMPORTANT: Once a source is registered it should not be modified.
   *
   * @param sourceName the name of the source
   * @return the registered source
   */
  public Source getSource(String sourceName) {
    return registry.getSource(sourceName);
  }

  /**
   * Returns a list of the registered sources.
   *
   * <p>IMPORTANT: Once a source is registered it should not be modified.
   *
   * @return the list of registered sources
   */
  public List<Source> getSources() {
    return Collections.unmodifiableList(registry.getSources());
  }

  /**
   * Returns a list of names of the registered sources.
   *
   * @return list of source names
   */
  public List<String> getSourceNames() {
    return registry.getSources().stream().map(s -> s.getName()).collect(Collectors.toList());
  }
  
  /**
   * Clears the database for the principal source. All documents, metadata, cursor, etc are removed.
   */
  public void clear() {
    clear(registry.getPrincipalSource().getName());
  }
  
  /**
   * Clears the database for the given source. All documents, metadata, cursor, etc are removed.
   * 
   * @param sourceName
   */
  public void clear(String sourceName) {
    registry.getSource(sourceName).getDb().clear();
  }
}
