/** */
package edu.byu.hbll.box;

import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * A database to hold processed documents for later retrieval and all other metadata and controls
 * used for processing the documents.
 *
 * <p>Implementations should have a document repository with Box metadata, a queue for documents to
 * be processed, and maintain harvest cursors and group start times and end times.
 *
 * @author Charles Draper
 */
public interface BoxDatabase extends BoxConfigurable {

  /**
   * Used to mark old documents for reprocessing. Any documents that were last processed before (NOW
   * - olderThan) will be placed on the queue.
   *
   * @param olderThan target documents older than this age
   * @return number of documents added to queue
   */
  int addToQueue(Duration olderThan);

  /**
   * Adds a collection of ids to the queue for processing. If the id is already on the queue, this
   * method has no effect. If resetAttempt is set to true however, the attempt flag in the queue
   * will be set to NOW.
   *
   * @param ids the ids to be added
   * @param resetAttempt true to force an update on the attempt time of an existing entry, false to
   *     keep the existing attempt time
   */
  void addToQueue(Collection<String> ids, boolean resetAttempt);

  /**
   * Deletes the given document ids from the queue signifying that the processing of the documents
   * was successful.
   *
   * @param ids the document ids to delete
   */
  void deleteFromQueue(Collection<String> ids);

  /**
   * Finds documents dependent on the given dependencies.
   *
   * @param dependencies the dependencies
   * @return all dependent documents per dependency, key is dependency, value is dependents
   */
  Map<DocumentId, Set<String>> findDependents(Collection<DocumentId> dependencies);

  /**
   * Finds documents in the database according to the given query. If the query specifies ids,
   * documents should be returned in the same order as ids and an unprocessed document should be
   * created for missing documents. For id queries, all corresponding documents are returned in one
   * page so limit is ignored. The database is not responsible for processing documents so the
   * process and wait directives are ignored. The metadataLevel and metadataOnly directives are
   * honored.
   *
   * @param query the query to use
   * @return matching documents
   */
  QueryResult find(BoxQuery query);

  /**
   * Returns the harvest cursor for this source. Whatever is set using {@link
   * #setHarvestCursor(ObjectNode)} should be returned here.
   *
   * @return the harvest cursor
   */
  ObjectNode getHarvestCursor();

  /**
   * Finds the unique set of all sources this source is dependent on.
   *
   * @return the set of dependency source names
   */
  Set<String> listSourceDependencies();

  /**
   * Return the next batch of ids from the queue.
   *
   * @param limit size of the batch to return
   * @return the next ids in the queue
   */
  List<String> nextFromQueue(int limit);

  /**
   * Using the timestamp saved in the database when {@link #startGroup(String)} was called, executes
   * the given function on each document processed before that group start time.
   *
   * @param groupId documents belonging to this group should be processed
   * @param function the function to run on each document
   */
  void processOrphans(String groupId, Consumer<BoxDocument> function);

  /**
   * Removes all traces of documents that were deleted more than olderThan ago.
   *
   * @param olderThan the age of the deleted documents to remove
   */
  void removeDeleted(Duration olderThan);

  /**
   * Save this collection of documents to the database.
   *
   * @param documents documents to save
   */
  void save(Collection<? extends BoxDocument> documents);

  /**
   * Updates this collection of documents with their specified dependencies. Nothing else is
   * updated.
   *
   * @param documents documents to update
   */
  void updateDependencies(Collection<? extends BoxDocument> documents);

  /**
   * Stores this cursor in the database for later retrieval by {@link #getHarvestCursor()}.
   *
   * @param cursor the cursor object to store
   */
  void setHarvestCursor(ObjectNode cursor);

  /**
   * Marks time in the database that the given group has started.
   *
   * @param groupId the id of the group
   */
  void startGroup(String groupId);

  /**
   * Simply sets the processed date of the documents identified by the given ids to NOW. Does
   * nothing if the document doesn't exist.
   *
   * @param ids the ids of the document
   */
  void updateProcessed(Collection<String> ids);

  /**
   * Finds the dependencies for the given document ids.
   *
   * @param ids the document ids
   * @return the dependencies for the given documents, key is the document id, value is the
   *     dependencies
   */
  Map<String, Set<DocumentId>> findDependencies(Collection<String> ids);

  /**
   * Retrieves a single entry from the registry. The registry is a general place for individual
   * entries important for the normal function of box. IDs should uniquely identify the type of
   * entry.
   *
   * @param id the id of the entry
   * @return the entry value denoted by the id or null if not found
   */
  JsonNode findRegistryValue(String id);

  /**
   * Saves an entry to the registry denoted by id. The registry is a general place for individual
   * entries important for the normal function of box. IDs should uniquely identify the type of
   * entry.
   *
   * @param id the id of the entry
   * @param value the value of the entry
   */
  void saveRegistryValue(String id, JsonNode value);

  /**
   * Clears the box database for the source.
   *
   * <p>Default operation is to throw an {@link UnsupportedOperationException}.
   */
  default void clear() {
    throw new UnsupportedOperationException();
  }
}
