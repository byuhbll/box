/** */
package edu.byu.hbll.box.internal.core;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import edu.byu.hbll.box.Harvester;

/**
 * Annotate a field or getter method to indicate that this {@link Harvester} should connect to a
 * parent box server in order to receive communication via web sockets. The only communication
 * currently enabled is that of a ping to let the client know there are updated documents available.
 * The annotated field should be a String containing the baseUri of the box instance. The method
 * should take no arguments and return a String containing the baseUri of the box server.
 *
 * @author Charles Draper
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})
public @interface BoxSourceBaseUri {}
