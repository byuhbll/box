/** */
package edu.byu.hbll.box.internal.core;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/** */
public class QuotaSemaphore implements BoxRunnable {

  /** */
  private int quota;

  /** */
  private Semaphore quotaSemaphore;

  /** @param quota */
  public QuotaSemaphore(int quota) {
    this.quota = quota;
    this.quotaSemaphore = new Semaphore(quota);
  }

  /** */
  @Override
  public boolean run() throws Exception {
    quotaSemaphore.drainPermits();
    quotaSemaphore.release(quota);

    return false;
  }

  /**
   * @param timeout
   * @param unit
   * @return
   * @throws InterruptedException
   */
  public boolean tryAquire(long timeout, TimeUnit unit) throws InterruptedException {
    if (quota < 1) {
      return true;
    }

    return quotaSemaphore.tryAcquire(timeout, unit);
  }
}
