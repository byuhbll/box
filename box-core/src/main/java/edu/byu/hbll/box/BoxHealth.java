/** */
package edu.byu.hbll.box;

import java.util.HashMap;
import java.util.Map;

/**
 * A snapshot of the health of the Box system.
 *
 * @author Charles Draper
 */
public class BoxHealth {

  private boolean healthy;

  private Map<String, SourceHealth> sourceHealthMap = new HashMap<>();

  /**
   * @param healthy whether or not the overall health of box is good
   * @param sourceHealthMap the health information for all sources keyed by sourceName
   */
  public BoxHealth(boolean healthy, Map<String, SourceHealth> sourceHealthMap) {
    this.healthy = healthy;
    this.sourceHealthMap = sourceHealthMap;
  }

  /** @return whether or not the overall health of box is good */
  public boolean isHealthy() {
    return healthy;
  }

  /**
   * @param sourceName the sourceName
   * @return the health for a particular source
   */
  public SourceHealth getSourceHealth(String sourceName) {
    return sourceHealthMap.get(sourceName);
  }

  /** @return the health information for all sources keyed by sourceName */
  public Map<String, SourceHealth> getSourceHealthMap() {
    return sourceHealthMap;
  }
}
