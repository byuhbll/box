/** */
package edu.byu.hbll.box;

import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.MissingNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.impl.CdiHarvester;
import edu.byu.hbll.box.impl.CdiProcessor;
import edu.byu.hbll.box.impl.EjbHarvester;
import edu.byu.hbll.box.impl.EjbProcessor;
import edu.byu.hbll.box.internal.util.ScheduleParser;
import edu.byu.hbll.json.YamlLoader;
import edu.byu.hbll.json.JsonField;

/**
 * The configuration used to instantiate Box. Primarily it holds the source definitions. It can also
 * build the configuration from a {@link JsonNode}.
 *
 * @author Charles Draper
 */
public class BoxConfiguration {

  static final Logger logger = LoggerFactory.getLogger(BoxConfiguration.class);

  private List<Source> sources = new ArrayList<>();

  private ThreadFactory threadFactory = Executors.defaultThreadFactory();

  private static ObjectMapper mapper = new ObjectMapper();

  /** */
  public BoxConfiguration() {}

  /**
   * @param box the application scoped (singleton) instance of Box
   * @param config the config used to build this configuration
   */
  public BoxConfiguration(Box box, JsonNode config) {
    configure(box, config);
  }

  /**
   * Adds the given source to this configuration.
   *
   * @param source the {@link Source} to add
   * @return this
   */
  public BoxConfiguration addSource(Source source) {
    this.sources.add(source);
    return this;
  }

  /**
   * Sets the thread factory to use for any multithreaded harvesting/processing inside of Box.
   *
   * @param threadFactory the thread factory to use
   * @return this
   */
  public BoxConfiguration threadFactory(ThreadFactory threadFactory) {
    this.threadFactory = threadFactory;
    return this;
  }

  /**
   * Sets all parameters held in the {@link JsonNode}.
   *
   * @param config
   * @return
   */
  @SuppressWarnings("unchecked")
  private BoxConfiguration configure(Box box, JsonNode config) {
    logger.info("Configuring sources");

    try {
      // make immediate copy to prevent modifying the actual parameter
      config = config == null ? MissingNode.getInstance() : config.deepCopy();

      // read in defaults
      JsonNode defaults =
          new YamlLoader()
              .load(
                  new InputStreamReader(
                      this.getClass()
                          .getClassLoader()
                          .getResourceAsStream("edu/byu/hbll/box/defaults.yml"),
                      "utf8"));

      // merge default values
      config = merge(defaults, config);

      Iterator<String> sourceNames = config.path("sources").fieldNames();

      while (sourceNames.hasNext()) {
        String sourceName = sourceNames.next();
        JsonNode srcCfg = config.path("sources").path(sourceName);
        Source source = new Source(sourceName);

        // template if exists
        if (srcCfg.has("template")) {
          JsonNode templateCfg = srcCfg.path("template");
          String templateId = templateCfg.path("id").asText(null);
          ObjectNode template = (ObjectNode) config.path("templates").path(templateId).deepCopy();

          Map<String, JsonNode> values = new HashMap<>();

          // get default param values
          for (JsonField field : new JsonField(template.path("templateParams"))) {
            values.put(field.getKey(), field.getValue());
          }

          // get set param values
          for (JsonField field : new JsonField(templateCfg.path("params"))) {
            values.put(field.getKey(), field.getValue());
          }

          template.remove("templateParams");

          bind(template, values);

          srcCfg = merge(template, srcCfg);
        }

        logger.info(
            "Found source `"
                + sourceName
                + "` with the following configuration:\n"
                + mapper.writerWithDefaultPrettyPrinter().writeValueAsString(srcCfg));

        if (srcCfg.has("enabled")) {
          source.setEnabled(srcCfg.path("enabled").asBoolean());
        }

        if (!source.isEnabled()) {
          logger.warn("Source `" + sourceName + "` is disabled");
          continue;
        }

        if (srcCfg.has("principal")) {
          source.setPrincipal(srcCfg.path("principal").asBoolean());
        }

        if (srcCfg.has("reprocessAge")) {
          source.setReprocessAge(Duration.parse(srcCfg.path("reprocessAge").asText()));
        }

        if (srcCfg.has("reprocessSchedule")) {
          source.setReprocessSchedule(
              ScheduleParser.parse(srcCfg.path("reprocessSchedule").asText()));
        }

        if (srcCfg.has("removeAge")) {
          source.setRemoveAge(Duration.parse(srcCfg.path("removeAge").asText()));
        }

        if (srcCfg.has("removeSchedule")) {
          source.setRemoveSchedule(ScheduleParser.parse(srcCfg.path("removeSchedule").asText()));
        }

        if (srcCfg.has("facetField")) {
          Iterator<String> facetNames = srcCfg.path("facetField").fieldNames();

          while (facetNames.hasNext()) {
            String facetName = facetNames.next();

            for (JsonNode value : srcCfg.path("facetField").path(facetName)) {
              source.addFacetField(facetName, value.asText());
            }
          }
        }

        if (srcCfg.has("save")) {
          source.setSave(srcCfg.path("save").asBoolean());
        }

        if (srcCfg.has("dependencyOnly")) {
          source.setDependencyOnly(srcCfg.path("dependencyOnly").asBoolean());
        }

        if (srcCfg.has("defaultLimit")) {
          source.setDefaultLimit(srcCfg.path("defaultLimit").asInt());
        }

        if (srcCfg.has("overwrite")) {
          source.setOverwrite(srcCfg.path("overwrite").asBoolean());
        }

        if (srcCfg.has("process") && srcCfg.path("process").path("enabled").asBoolean(true)) {

          JsonNode processCfg = srcCfg.path("process").deepCopy();

          Class<? extends Processor> processorClass = null;

          if (!processCfg.has("type")) {
            throw new IllegalArgumentException(
                "missing 'type' attribute for source." + sourceName + ".process");
          }

          processorClass =
              (Class<? extends Processor>) Class.forName(processCfg.path("type").asText());

          Processor processor;

          if (processCfg.path("instance").asText("new").equals("cdi")) {
            processor = new CdiProcessor(processorClass);
          } else if (processCfg.path("instance").asText("new").equals("ejb")) {
            processor =
                new EjbProcessor(processorClass, processCfg.path("mappedName").asText(null));
          } else {
            Constructor<? extends Processor> constructor = processorClass.getDeclaredConstructor();
            constructor.setAccessible(true);
            processor = constructor.newInstance();
          }

          putCommonParameters(processor, box, source);

          if (processCfg.has("batchDelay")) {
            source.setBatchDelay(Duration.parse(processCfg.path("batchDelay").asText()));
          }

          source.setBatchCapacity(processCfg.path("batchCapacity").asInt(1));

          if (processCfg.has("threadCount")) {
            source.setThreadCount(processCfg.path("threadCount").asInt());
          }

          if (processCfg.has("suspend")) {
            source.setSuspend(Duration.parse(processCfg.path("suspend").asText()));
          }

          if (processCfg.has("quota")) {
            source.setQuota(processCfg.path("quota").asInt());
          }

          if (processCfg.has("quotaReset")) {
            source.setQuotaReset(ScheduleParser.parse(processCfg.path("quotaReset").asText()));
          }

          if (processCfg.has("on")) {
            source.setOn(ScheduleParser.parse(processCfg.path("on").asText()));
          }

          if (processCfg.has("off")) {
            source.setOff(ScheduleParser.parse(processCfg.path("off").asText()));
          }

          if (processCfg.has("enabled")) {
            source.setProcessEnabled(processCfg.path("enabled").asBoolean());
          }

          if (processCfg.has("idField")) {
            source.setIdField(processCfg.path("idField").asText());
          }

          if (processCfg.has("process")) {
            source.setProcess(processCfg.path("process").asBoolean());
          }

          InitConfig initConfig = new InitConfig(getParams(processCfg));
          initConfig.setBox(box);
          initConfig.setSource(source);
          initConfig.setObjectType(ObjectType.PROCESSOR);

          processor.postConstruct(initConfig);
          source.setProcessor(processor);

        } else {
          source.setProcessEnabled(false);
        }

        if (srcCfg.has("harvest") && srcCfg.path("harvest").path("enabled").asBoolean(true)) {

          JsonNode harvestCfg = srcCfg.path("harvest").deepCopy();

          Class<? extends Harvester> harvestClientClass = null;

          if (!harvestCfg.has("type")) {
            throw new IllegalArgumentException(
                "missing 'type' attribute for source." + sourceName + ".harvest");
          }

          harvestClientClass =
              (Class<? extends Harvester>) Class.forName(harvestCfg.path("type").asText());

          Harvester harvestProcessor;

          if (harvestCfg.path("instance").asText("new").equals("cdi")) {
            harvestProcessor = new CdiHarvester(harvestClientClass);
          } else if (harvestCfg.path("instance").asText("new").equals("ejb")) {
            harvestProcessor =
                new EjbHarvester(harvestClientClass, harvestCfg.path("mappedName").asText(null));
          } else {
            Constructor<? extends Harvester> constructor =
                harvestClientClass.getDeclaredConstructor();
            constructor.setAccessible(true);
            harvestProcessor = constructor.newInstance();
          }

          putCommonParameters(harvestProcessor, box, source);

          if (harvestCfg.has("schedule") && !harvestCfg.path("schedule").isNull()) {
            source.setHarvestSchedule(ScheduleParser.parse(harvestCfg.path("schedule").asText()));
          }

          if (harvestCfg.has("resetSchedule")) {
            source.setHarvestResetSchedule(
                ScheduleParser.parse(harvestCfg.path("resetSchedule").asText(null)));
          }

          source.setSaveDeleted(harvestCfg.path("saveDeleted").asBoolean(source.isSaveDeleted()));

          if (harvestCfg.has("enabled") && !harvestCfg.path("enabled").isNull()) {
            source.setHarvestEnabled(harvestCfg.path("enabled").asBoolean());
          }

          InitConfig initConfig = new InitConfig(getParams(harvestCfg));
          initConfig.setBox(box);
          initConfig.setSource(source);
          initConfig.setObjectType(ObjectType.HARVESTER);

          harvestProcessor.postConstruct(initConfig);
          source.setHarvester(harvestProcessor);
        } else {
          source.setHarvestEnabled(false);
        }

        if (srcCfg.has("db") || config.has("db")) {

          JsonNode dbConfig = srcCfg.path("db");

          if (dbConfig.isMissingNode()) {
            dbConfig = config.path("db");
          }

          if (!dbConfig.has("type")) {
            throw new IllegalArgumentException("missing 'type' attribute for db");
          }

          Class<? extends BoxDatabase> dbClass =
              (Class<? extends BoxDatabase>) Class.forName(dbConfig.path("type").asText());

          Constructor<? extends BoxDatabase> constructor = dbClass.getDeclaredConstructor();
          constructor.setAccessible(true);
          BoxDatabase db = constructor.newInstance();

          putCommonParameters(db, box, source);

          InitConfig initConfig = new InitConfig(getParams(dbConfig));
          initConfig.setBox(box);
          initConfig.setSource(source);
          initConfig.setObjectType(ObjectType.BOX_DATABASE);

          db.postConstruct(initConfig);

          source.setDb(db);
        }

        if (srcCfg.has("cursor")
            && (srcCfg.has("type") || !srcCfg.has("type") && config.has("db"))) {

          JsonNode cursorConfig = srcCfg.path("cursor");

          if (!cursorConfig.has("type")) {
            cursorConfig = config.path("db");
          }

          if (!cursorConfig.has("type")) {
            throw new IllegalArgumentException("missing 'type' attribute for cursor");
          }

          Class<? extends BoxDatabase> cursorClass =
              (Class<? extends BoxDatabase>) Class.forName(cursorConfig.path("type").asText());

          Constructor<? extends BoxDatabase> constructor = cursorClass.getDeclaredConstructor();
          constructor.setAccessible(true);
          BoxDatabase cursor = constructor.newInstance();

          putCommonParameters(cursor, box, source);

          InitConfig initConfig = new InitConfig(getParams(cursorConfig));
          initConfig.setBox(box);
          initConfig.setSource(source);
          initConfig.setObjectType(ObjectType.CURSOR_DATABASE);

          cursor.postConstruct(initConfig);

          source.setCursorDb(cursor);
        }

        if (srcCfg.has("other")) {
          JsonNode otherConfig = srcCfg.path("other");

          if (!otherConfig.has("type")) {
            throw new IllegalArgumentException("missing 'type' attribute for other");
          }

          Class<? extends BoxConfigurable> otherClass =
              (Class<? extends BoxConfigurable>) Class.forName(otherConfig.path("type").asText());

          BoxConfigurable other = otherClass.newInstance();

          putCommonParameters(other, box, source);

          InitConfig initConfig = new InitConfig(getParams(otherConfig));
          initConfig.setBox(box);
          initConfig.setSource(source);
          initConfig.setObjectType(ObjectType.OTHER);

          other.postConstruct(initConfig);

          source.addOther(other);
        }

        if (!source.isProcessEnabled()) {
          logger.warn("Source `" + sourceName + "` has disabled processor");
        }

        if (!source.isHarvestEnabled()) {
          logger.warn("Source `" + sourceName + "` has disabled harvester");
        }

        addSource(source);
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    }

    return this;
  }

  /** @return the sources */
  public List<Source> getSources() {
    return sources;
  }

  /** @return the threadFactory */
  public ThreadFactory getThreadFactory() {
    return threadFactory;
  }

  /**
   * Merges data from the second argument into the JsonNode provided as the first argument.
   *
   * <p>Derived from a method contributed to http://stackoverflow.com/a/11459962 by StackOverflow
   * users arne (http://stackoverflow.com/users/838776/arne) and c4k
   * (http://stackoverflow.com/users/1259118/c4k).
   *
   * @param mainNode the node containing the baseline, original data
   * @param updateNode the node containing new data to merge
   */
  private JsonNode merge(JsonNode mainNode, JsonNode updateNode) {

    // get rid of nulls
    mainNode = mainNode == null ? MissingNode.getInstance() : mainNode;
    updateNode = updateNode == null ? MissingNode.getInstance() : updateNode;

    if (mainNode.isMissingNode()) {
      return updateNode;
    } else if (updateNode.isMissingNode()) {
      return mainNode;
    }

    if (!mainNode.isObject() || !updateNode.isObject()) {
      return updateNode;
    }

    // Update node is an object
    Iterator<String> fieldNames = updateNode.fieldNames();
    while (fieldNames.hasNext()) {
      String fieldName = fieldNames.next();
      JsonNode currentMainNode = mainNode.path(fieldName);
      JsonNode currentUpdateNode = updateNode.path(fieldName);
      if (currentMainNode.isObject() && currentUpdateNode.isObject()) {
        // Recursive case
        merge(currentMainNode, currentUpdateNode);
      } else if (mainNode.isObject()) {
        // Base Case
        ((ObjectNode) mainNode).replace(fieldName, currentUpdateNode);
      }
    }
    return mainNode;
  }

  /**
   * @param configurable
   * @param box
   * @param source
   * @throws IllegalAccessException
   * @throws InvocationTargetException
   */
  private void putCommonParameters(BoxConfigurable configurable, Box box, Source source)
      throws IllegalAccessException, InvocationTargetException {
    Map<String, Object> properties = new HashMap<>();
    properties.put("box", box);
    properties.put("source", source);
    properties.put("sourceName", source.getName());

    BeanUtils.populate(configurable, properties);
  }

  private void bind(JsonNode node, Map<String, JsonNode> values) {

    int i = 0;

    for (JsonField field : new JsonField(node)) {
      JsonNode value = field.getValue();

      if (value.isTextual()) {
        String textValue = value.asText(null);

        if (textValue != null && textValue.startsWith("${") && textValue.endsWith("}")) {
          String key = textValue.substring(2, textValue.length() - 1);
          JsonNode finalValue = values.getOrDefault(key, value);

          if (node.isArray()) {
            ((ArrayNode) node).remove(i);
            ((ArrayNode) node).insert(i, finalValue);
          } else if (node.isObject()) {
            ((ObjectNode) node).set(field.getKey(), finalValue);
          }
        }
      } else if (value.isContainerNode()) {
        bind(value, values);
      }

      i++;
    }
  }

  private ObjectNode getParams(JsonNode config) {
    ObjectNode params =
        config.has("params")
            ? (ObjectNode) config.path("params")
            : JsonNodeFactory.instance.objectNode();
    return params;
  }
}
