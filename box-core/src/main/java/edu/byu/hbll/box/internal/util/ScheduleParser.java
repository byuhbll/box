/** */
package edu.byu.hbll.box.internal.util;

import javax.ejb.ScheduleExpression;

/** */
public class ScheduleParser {

  /**
   * Creates a new {@link ScheduleExpression} given the string expression. The string expression is
   * whitespace delimited and requires the following 7 components in order:
   *
   * <ul>
   *   <li>second
   *   <li>minute
   *   <li>hour
   *   <li>dayOfMonth
   *   <li>month
   *   <li>dayOfWeek
   *   <li>year
   * </ul>
   *
   * <p>The syntax for each component can be found here
   * https://docs.oracle.com/javaee/7/api/javax/ejb/ScheduleExpression.html
   *
   * <p>Does not throw an NPE if expression is null. Simply returns null.
   *
   * @param expression the string expression
   * @return a parsed {@link ScheduleExpression} equivalent of the string expression
   */
  public static ScheduleExpression parse(String expression) {

    if (expression == null) {
      return null;
    }

    ScheduleExpression se = new ScheduleExpression();

    String[] components = expression.split("\\s+");

    se.second(components[0]);
    se.minute(components[1]);
    se.hour(components[2]);
    se.dayOfMonth(components[3]);
    se.month(components[4]);
    se.dayOfWeek(components[5]);
    se.year(components[6]);

    return se;
  }
}
