/** */
package edu.byu.hbll.box;

import java.util.Objects;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.internal.util.JsonUtils;

/**
 * Configuration for initializing newly instantiated Box extension classes.
 *
 * @author Charles Draper
 */
public class InitConfig {

  private ObjectNode params = JsonNodeFactory.instance.objectNode();

  private Box box;

  private Source source;

  private ObjectType objectType;

  /** */
  public InitConfig() {}

  /**
   * @param params the instance specific parameters (found in the params section of the
   *     configuration)
   */
  public InitConfig(ObjectNode params) {
    this.params = Objects.requireNonNull(params);
  }

  /**
   * Binds the params to the target object using Jackson's binding mechanism and rules.
   *
   * @param target the target object
   */
  public void bind(Object target) {
    JsonUtils.bind(params, target);
  }

  /**
   * @return the the instance specific parameters (found in the params section of the configuration)
   */
  public ObjectNode getParams() {
    return params;
  }

  /**
   * @param params the the instance specific parameters to set (found in the params section of the
   *     configuration)
   */
  public void setParams(ObjectNode params) {
    this.params = Objects.requireNonNull(params);
  }

  /** @return the box */
  public Box getBox() {
    return box;
  }

  /** @param box the box to set */
  public void setBox(Box box) {
    this.box = box;
  }

  /** @return the source */
  public Source getSource() {
    return source;
  }

  /** @param source the source to set */
  public void setSource(Source source) {
    this.source = source;
  }

  /** @return the sourceName */
  public String getSourceName() {
    return source.getName();
  }

  /** @return the objectType */
  public ObjectType getObjectType() {
    return objectType;
  }

  /** @param objectType the objectType to set */
  public void setObjectType(ObjectType objectType) {
    this.objectType = objectType;
  }

  /** @return */
  public boolean isProcessor() {
    return objectType == ObjectType.PROCESSOR;
  }

  /** @return */
  public boolean isHarvester() {
    return objectType == ObjectType.HARVESTER;
  }

  /** @return */
  public boolean isBoxDatabase() {
    return objectType == ObjectType.BOX_DATABASE;
  }

  /** @return */
  public boolean isCursorDatabase() {
    return objectType == ObjectType.CURSOR_DATABASE;
  }

  /** @return */
  public boolean isOther() {
    return objectType == ObjectType.OTHER;
  }
}
