/** */
package edu.byu.hbll.box.internal.core;

import java.time.Instant;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import edu.byu.hbll.box.internal.util.DeploymentMonitor;

/** */
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class Scheduler {

  static final Logger logger = LoggerFactory.getLogger(Scheduler.class);

  /** */
  @Resource private TimerService timerService;

  @Inject private DeploymentMonitor deployMonitor;

  /** */
  private AtomicInteger nextId = new AtomicInteger();

  /** */
  private Map<Integer, BoxRunnable> runnables = new ConcurrentHashMap<>();

  /** */
  private Map<Integer, Semaphore> semaphores = new ConcurrentHashMap<>();

  /** */
  private volatile boolean shutdown;

  /** */
  private volatile boolean enabled;

  /**
   * Method to run when the timer times out. This runs the tasks in the maintenance queues. It will
   * only run when the timer times out and only for the max duration.
   */
  @Timeout
  public void timeout(Timer timer) {
    try {

      // only run if scheduler is enabled
      if (!enabled || !deployMonitor.isDeployed()) {
        return;
      }

      int runnableId = (int) timer.getInfo();
      BoxRunnable runnable = runnables.get(runnableId);

      // invalid id
      if (runnable == null) {
        return;
      }

      Semaphore semaphore = semaphores.get(runnableId);

      // if already running and another is waiting, just fall out and do nothing
      if (semaphore.tryAcquire()) {
        try {

          // wait for the already running one to finish
          synchronized (runnable) {

            // This probably looks weird to have two levels of synchronization (ie, the semaphore
            // and the
            // synchronized block). The reason for it is one we don't want a runnable executing in
            // multiple
            // threads and so the synchronized block is there. Two, we don't want a potentially very
            // large
            // number of backed up scheduled runs and so that's why the semaphore is there to say
            // drop out
            // and
            // don't bother running if one is already running. Three, there are two permits in the
            // semaphore
            // so
            // as to allow exactly one backed up run to avoid race conditions. The race condition
            // being the
            // moment that a runnable completes, but before it releases the lock and another run is
            // scheduled.
            // If we didn't have two in the semaphore, the one running could have finished, but not
            // released,
            // while another run is scheduled and drops out because the first has not released the
            // lock yet
            // potentially dropping a needed run.

            boolean more = false;

            do {
              more = runnable.run();
            } while (more && !shutdown);
          }
        } finally {
          semaphore.release();
        }
      }
    } catch (Exception e) {
      logger.error(e.toString(), e);
    }
  }

  /**
   * @param runnable
   * @param schedule
   */
  public int schedule(BoxRunnable runnable, ScheduleExpression schedule) {
    int id = nextId.getAndIncrement();
    runnables.put(id, runnable);
    semaphores.put(id, new Semaphore(2));
    timerService.createCalendarTimer(schedule, new TimerConfig(id, false));
    return id;
  }

  /**
   * @param schedule
   * @return
   */
  public Instant getNextTimeout(ScheduleExpression schedule) {
    Timer timer = timerService.createCalendarTimer(schedule, new TimerConfig(-1, false));
    Instant nextTimeout = timer.getNextTimeout().toInstant();
    timer.cancel();
    return nextTimeout;
  }

  /** @param id */
  public void trigger(int id) {
    timerService.createSingleActionTimer(0, new TimerConfig(id, false));
  }

  /**
   * Scheduled tasks can start executing before Box is completely initialized. So this scheduler has
   * the enabled flag which is initially set to false and which only gets set to true when the
   * initializing is complete. Until then, any tasks that execute according to schedule will simply
   * fall out and not run.
   */
  public void enable() {
    enabled = true;
  }

  /** */
  @PreDestroy
  public void preDestroy() {
    shutdown = true;
  }
}
