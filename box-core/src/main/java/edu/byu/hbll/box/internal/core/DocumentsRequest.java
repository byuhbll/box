/** */
package edu.byu.hbll.box.internal.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/** */
public class DocumentsRequest {

  /** */
  private String sourceName;

  /** */
  private List<String> ids = new ArrayList<>();

  /** */
  private List<String> fields = new ArrayList<>();

  /** */
  private boolean wait;

  /** */
  private boolean process;

  /** */
  public DocumentsRequest() {}

  /** @param sourceName */
  public DocumentsRequest(String sourceName) {
    this.sourceName = sourceName;
  }

  /**
   * @param sourceName
   * @return
   */
  public DocumentsRequest sourceName(String sourceName) {
    this.sourceName = sourceName;
    return this;
  }

  /**
   * @param id
   * @return
   */
  public DocumentsRequest id(String id) {
    this.ids.add(id);
    return this;
  }

  /**
   * @param ids
   * @return
   */
  public DocumentsRequest ids(Collection<String> ids) {
    this.ids.addAll(ids);
    return this;
  }

  /**
   * @param fields
   * @return
   */
  public DocumentsRequest fields(Collection<String> fields) {
    this.fields.addAll(fields);
    return this;
  }

  /** @return */
  public DocumentsRequest setWait() {
    this.wait = true;
    return this;
  }

  /**
   * @param wait
   * @return
   */
  public DocumentsRequest setWait(boolean wait) {
    this.wait = wait;
    return this;
  }

  /** @return */
  public DocumentsRequest process() {
    this.process = true;
    return this;
  }

  /** @return */
  public DocumentsRequest process(boolean process) {
    this.process = process;
    return this;
  }

  /**
   * Returns a new {@link DocumentsRequest} based on this one. It is a copy with the exception of
   * the sourceName and ids.
   *
   * @param sourceName
   * @return
   */
  public DocumentsRequest reuse(String sourceName) {
    DocumentsRequest request = new DocumentsRequest(sourceName).process(process).setWait(wait);
    return request;
  }

  /** @return the sourceName */
  public String getSourceName() {
    return sourceName;
  }

  /** @param sourceName the sourceName to set */
  public void setSourceName(String sourceName) {
    this.sourceName = sourceName;
  }

  /** @return the ids */
  public List<String> getIds() {
    return ids;
  }

  /** @param ids the ids to set */
  public void setIds(List<String> ids) {
    this.ids = ids;
  }

  /** @return the fields */
  public List<String> getFields() {
    return fields;
  }

  /** @param fields the fields to set */
  public void setFields(List<String> fields) {
    this.fields = fields;
  }

  /** @return the process */
  public boolean isProcess() {
    return process;
  }

  /** @param process the process to set */
  public void setProcess(boolean process) {
    this.process = process;
  }

  /** @return the wait */
  public boolean isWait() {
    return wait;
  }
}
