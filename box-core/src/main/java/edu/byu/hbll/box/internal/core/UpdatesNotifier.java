/** */
package edu.byu.hbll.box.internal.core;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** @author Charles Draper */
@Singleton
@TransactionManagement(TransactionManagementType.BEAN)
public class UpdatesNotifier {

  static final Logger logger = LoggerFactory.getLogger(UpdatesNotifier.class);

  public static final int MAXIMUM_POOL_SIZE = 128;
  public static final long KEEP_ALIVE_TIME = 60;
  public static final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;

  private Map<String, Set<Runnable>> listeners = new ConcurrentHashMap<>();

  private Set<String> triggered = Collections.newSetFromMap(new ConcurrentHashMap<>());

  private ThreadPoolExecutor executor;

  /** */
  @PostConstruct
  public void postConstruct() {
    executor = new ScheduledThreadPoolExecutor(MAXIMUM_POOL_SIZE);
    executor.setKeepAliveTime(KEEP_ALIVE_TIME, KEEP_ALIVE_TIME_UNIT);
    executor.allowCoreThreadTimeOut(true);
    executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
  }

  /** */
  @PreDestroy
  public void preDestroy() {
    if (executor != null) {
      executor.shutdown();
    }
  }

  /** */
  @Schedule(second = "*", minute = "*", hour = "*", persistent = false)
  public void timeout() {

    Set<String> triggered = new HashSet<>(this.triggered);

    for (String sourceName : triggered) {
      this.triggered.remove(sourceName);

      Set<Runnable> listeners = this.listeners.get(sourceName);

      if (listeners != null) {
        listeners.forEach(l -> executor.submit(l));
      }
    }
  }

  /**
   * @param sourceName
   * @param listener
   */
  public void register(String sourceName, Runnable listener) {
    listeners
        .computeIfAbsent(Objects.requireNonNull(sourceName), k -> new HashSet<>())
        .add(Objects.requireNonNull(listener));
  }

  /**
   * @param sourceName
   * @param listener
   */
  public void unregister(String sourceName, Runnable listener) {
    listeners
        .computeIfAbsent(Objects.requireNonNull(sourceName), k -> new HashSet<>())
        .remove(Objects.requireNonNull(listener));
  }

  /** @param sourceName */
  public void trigger(String sourceName) {
    triggered.add(Objects.requireNonNull(sourceName));
  }
}
