/** */
package edu.byu.hbll.box.impl;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.lt;
import java.io.Closeable;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.ReplaceOptions;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.util.JSON;
import edu.byu.hbll.box.BoxDatabase;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.DocumentId;
import edu.byu.hbll.box.Facet;
import edu.byu.hbll.box.InitConfig;
import edu.byu.hbll.box.MetadataLevel;
import edu.byu.hbll.box.QueryResult;
import edu.byu.hbll.box.Source;
import edu.byu.hbll.box.internal.util.JsonUtils;
import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;

/** A box database that persists to MongoDB. */
public class MongoDatabase implements BoxDatabase, Closeable {

  static final Logger logger = LoggerFactory.getLogger(MongoDatabase.class);

  private static final UncheckedObjectMapper mapper = ObjectMapperFactory.newUnchecked();
  private static final long NEXT_ATTEMPT_JITTER = TimeUnit.HOURS.toMillis(12);

  private MongoClient mongo;

  private Random random = new Random();

  private String database;

  private String sourceName;

  private Source source;

  private String documentsName;

  private MongoCollection<Document> queue;
  private MongoCollection<Document> cursor;
  private MongoCollection<Document> metadata;
  private MongoCollection<Document> groups;
  private MongoCollection<Document> documents;
  private MongoCollection<Document> registry;

  MongoDatabase() {}

  /** @param database */
  public MongoDatabase(String database, String sourceName) {
    this(new MongoClient(), database, sourceName);
  }

  /**
   * @param uri
   * @param database
   */
  public MongoDatabase(String uri, String database, String sourceName) {
    this(new MongoClient(new MongoClientURI(uri)), database, sourceName);
  }

  /**
   * @param mongo
   * @param database
   */
  public MongoDatabase(MongoClient mongo, String database, String sourceName) {
    this.mongo = mongo;
    this.database = database;
    this.sourceName = sourceName;
    init();
  }

  /** @param config */
  @Override
  public void postConstruct(InitConfig config) {

    ObjectNode params = config.getParams();

    if (!params.has("database")) {
      throw new IllegalArgumentException(
          "The parameter `database` is required. This should be the name of the database to use.");
    }

    String uri = params.path("uri").asText("mongodb://localhost/");
    database = params.path("database").asText();

    MongoClientURI clientUri = new MongoClientURI(uri);
    mongo = new MongoClient(clientUri);

    init();
  }

  /**
   * @param name
   * @return
   */
  private MongoCollection<Document> getCollection(String name) {
    return mongo.getDatabase(database).getCollection(getCollectionName(name));
  }

  /**
   * @param name
   * @return
   */
  private String getCollectionName(String name) {
    return sourceName + "_" + name;
  }

  /** @param source */
  private void init() {

    documentsName = getCollectionName("documents");

    queue = getCollection("queue");
    cursor = getCollection("cursor");
    metadata = getCollection("metadata");
    documents = getCollection("documents");
    groups = getCollection("groups");
    registry = getCollection("registry");

    // find documents dependent on a source document
    createIndex(metadata, "dependencies");

    // find dependent sources (findDependentSources)
    createIndex(metadata, "dependencies.sourceName");

    // harvest documents
    createIndex(metadata, "status", "cursor");

    // reprocess old documents
    createIndex(metadata, "processed");

    // remove old deleted documents
    createIndex(metadata, "modified", "status");

    // harvest documents by facet
    createIndex(metadata, "facets", "status", "cursor");

    // for finding orphans
    createIndex(metadata, "groupId", "processed");

    // poll the next tasks from the queue
    createIndex(queue, "attempt");
  }

  /** @param source */
  private void createIndex(MongoCollection<Document> collection, String... fields) {
    Document index = new Document();
    IndexOptions options = new IndexOptions().background(true);

    for (String field : fields) {
      index.append(field, 1);
    }

    collection.createIndex(index, options);
  }

  @Override
  public void addToQueue(Collection<String> ids, boolean resetAttempt) {

    for (String id : ids) {
      Document updateDoc = new Document("_id", id).append("requested", new Date());
      Document insertDoc = new Document();

      Document attemptDoc = resetAttempt ? updateDoc : insertDoc;
      // attemptDoc.append("attempt", now ? new Date() : Date.from(Instant.now().plusSeconds(60)));
      attemptDoc.append("attempt", new Date());

      insertDoc.append("attempts", 0);

      Document entry = new Document("$set", updateDoc).append("$setOnInsert", insertDoc);

      queue.updateOne(eq("_id", id), entry, new UpdateOptions().upsert(true));
    }
  }

  @Override
  public void close() throws IOException {
    if (mongo != null) {
      mongo.close();
    }
  }

  @Override
  public void deleteFromQueue(Collection<String> ids) {
    queue.deleteOne(eq("_id", new Document("$in", ids)));
  }

  @Override
  public Map<String, Set<DocumentId>> findDependencies(Collection<String> ids) {

    Map<String, Set<DocumentId>> dependencies = new HashMap<>();

    for (Document registry : metadata.find(new Document("_id", new Document("$in", ids)))) {
      BoxDocument document = convert(registry);
      dependencies.put(document.getId(), document.getDependencies());
    }

    return dependencies;
  }

  @Override
  public QueryResult find(BoxQuery query) {

    List<Document> pipeline = new ArrayList<>();

    if (query.isId()) {
      pipeline.add(
          new Document("$match", new Document("_id", new Document("$in", query.getIds()))));
    } else {
      pipeline.add(
          new Document("$match", new Document("cursor", new Document("$gte", query.getCursor()))));
    }

    if (!query.getStatuses().isEmpty()) {
      List<String> statuses = new ArrayList<>();
      query.getStatuses().forEach(s -> statuses.add(s.toString()));
      pipeline.add(new Document("$match", new Document("status", new Document("$in", statuses))));
    }

    if (!query.getFacets().isEmpty()) {
      // group facets by facet name
      Map<String, List<Document>> facetMap = new HashMap<>();
      query
          .getFacets()
          .stream()
          .forEach(
              f ->
                  facetMap
                      .computeIfAbsent(f.getName(), k -> new ArrayList<>())
                      .add(new Document("name", f.getName()).append("value", f.getValue())));

      // query facets so that OR logic is used within a facet group, but AND logic is used between
      // groups
      facetMap
          .entrySet()
          .stream()
          .forEach(
              f ->
                  pipeline.add(
                      new Document(
                          "$match", new Document("facets", new Document("$in", f.getValue())))));
    }

    if (query.isHarvest()) {
      pipeline.add(new Document("$sort", new Document("cursor", 1)));
      pipeline.add(new Document("$limit", query.getLimit()));
    }

    if (!query.isMetadataOnly()) {
      Document lookup =
          new Document("from", documentsName)
              .append("localField", "_id")
              .append("foreignField", "_id")
              .append("as", "document");

      pipeline.add(new Document("$lookup", lookup));
    }

    pipeline.add(
        new Document("$project", projectFields(query.getMetadataLevel(), query.getFields())));

    Map<String, BoxDocument> documentMap = new LinkedHashMap<>();

    for (Document document : metadata.aggregate(pipeline)) {
      BoxDocument processDocument = convert(document);

      if (processDocument != null) {
        documentMap.put(processDocument.getId(), processDocument);
      }
    }

    QueryResult result = new QueryResult();

    if (query.isId()) {
      for (String id : query.getIds()) {
        BoxDocument processDocument = documentMap.get(id);

        if (processDocument == null) {
          processDocument = new BoxDocument(id);
        }

        result.add(processDocument);
      }
    } else {
      result.addAll(documentMap.values());
    }

    result.updateNextCursor(query);

    return result;
  }

  private Document projectFields(MetadataLevel level, Collection<String> fields) {
    Document fieldDoc = new Document("_id", 1);

    switch (level) {
      case FULL:
        fieldDoc.append("modified", 1);
        fieldDoc.append("processed", 1);
        fieldDoc.append("message", 1);
        fieldDoc.append("facets", 1);
        fieldDoc.append("dependencies", 1);
        fieldDoc.append("groupId", 1);
      case CORE:
        fieldDoc.append("status", 1);
      case NONE:
      default:
        fieldDoc.append("cursor", 1);
    }

    if (fields.isEmpty()) {
      fieldDoc.append("document", 1);
    } else {
      for (String field : fields) {
        fieldDoc.append("document." + field, 1);
      }
    }

    return fieldDoc;
  }

  @Override
  public ObjectNode getHarvestCursor() {
    for (Document doc : cursor.find(eq("_id", "cursor"))) {
      if (doc.get("cursor") != null) {
        return (ObjectNode) JsonUtils.deserialize(((Document) doc.get("cursor")).toJson());
      }
    }

    return mapper.createObjectNode();
  }

  @Override
  public List<String> nextFromQueue(int limit) {

    FindIterable<Document> tasks =
        queue
            .find(new Document("attempt", new Document("$lte", new Date())))
            .sort(new Document("attempt", 1))
            .limit(limit);

    List<String> ids = new ArrayList<>();

    for (Document task : tasks) {
      String id = task.getString("_id");

      int newAttempts = task.getInteger("attempts") + 1;

      // push next attempt `newAttempts` days into the future with some jitter
      Instant newAttempt =
          Instant.now()
              .plus(newAttempts, ChronoUnit.DAYS)
              .plusMillis(random.nextLong() % NEXT_ATTEMPT_JITTER);

      ids.add(id);

      queue.updateOne(
          eq("_id", id),
          new Document(
              "$set",
              new Document("attempt", Date.from(newAttempt)).append("attempts", newAttempts)));
    }

    return ids;
  }

  @Override
  public void save(Collection<? extends BoxDocument> documents) {
    for (BoxDocument boxDocument : documents) {
      ObjectNode document = boxDocument.toJson();
      ObjectNode metadata = (ObjectNode) document.remove("@box");

      String id = boxDocument.getId();

      document.put("_id", id);
      metadata.put("_id", id);
      metadata.remove("id");

      // handle the document
      if (boxDocument.isDeleted() || !boxDocument.isProcessed()) {
        this.documents.deleteOne(eq("_id", id));
      } else {
        this.documents.replaceOne(
            eq("_id", id), Document.parse(document.toString()), new ReplaceOptions().upsert(true));
      }

      // handle the metadata
      Document metadataDocument = Document.parse(metadata.toString());

      if (boxDocument.getModified() != null) {
        metadataDocument.put("modified", Date.from(boxDocument.getModified()));
      }

      if (boxDocument.getProcessed() != null) {
        metadataDocument.put("processed", Date.from(boxDocument.getProcessed()));
      }

      metadataDocument.put("cursor", boxDocument.getCursor());

      this.metadata.replaceOne(eq("_id", id), metadataDocument, new ReplaceOptions().upsert(true));
    }
  }

  @Override
  public void updateDependencies(Collection<? extends BoxDocument> documents) {
    for (BoxDocument boxDocument : documents) {
      ObjectNode document = boxDocument.toJson();
      Object dependencies = Document.parse(document.path("@box").toString()).get("dependencies");
      String operation = dependencies == null ? "$unset" : "$set";

      this.metadata.updateOne(
          eq("_id", boxDocument.getId()),
          new Document(operation, new Document("dependencies", dependencies)));
    }
  }

  @Override
  public void startGroup(String groupId) {
    groups.replaceOne(
        eq("_id", groupId),
        new Document("_id", groupId).append("start", new Date()),
        new ReplaceOptions().upsert(true));
  }

  @Override
  public void processOrphans(String groupId, Consumer<BoxDocument> function) {
    Document groupDocument = groups.find(eq("_id", groupId)).first();

    if (groupDocument != null) {
      Date start = groupDocument.getDate("start");

      for (Document document : metadata.find(and(eq("groupId", groupId), lt("processed", start)))) {
        function.accept(convert(document));
      }
    }
  }

  @Override
  public void updateProcessed(Collection<String> ids) {
    metadata.updateMany(
        eq("_id", new Document("$in", ids)),
        new Document("$set", new Document("processed", new Date())));
  }

  @Override
  public void setHarvestCursor(ObjectNode cursor) {
    this.cursor.replaceOne(
        eq("_id", "cursor"),
        new Document("_id", "cursor").append("cursor", Document.parse(cursor.toString())),
        new ReplaceOptions().upsert(true));
  }

  /**
   * @param document
   * @return
   */
  private BoxDocument convert(Document document) {
    if (document == null) {
      return null;
    }

    if (document.containsKey("modified")) {
      try {
        document.put("modified", ((Date) document.get("modified")).toInstant().toString());
      } catch (Exception e) {
        logger.warn(e.toString(), e);
      }
    }

    if (document.containsKey("processed")) {
      try {
        document.put("processed", ((Date) document.get("processed")).toInstant().toString());
      } catch (Exception e) {
        logger.warn(e.toString(), e);
      }
    }

    if (document.containsKey("document")) {
      try {
        @SuppressWarnings("unchecked")
        List<Document> docs = ((List<Document>) document.get("document"));

        if (!docs.isEmpty()) {
          Document doc = docs.get(0);
          doc.remove("_id");
          document.put("document", doc);
        } else {
          document.remove("document");
        }
      } catch (Exception e) {
        logger.warn(e.toString(), e);
        return null;
      }
    }

    if (document.containsKey("cursor")) {
      try {
        document.put("cursor", document.getLong("cursor").toString());
      } catch (Exception e) {
        logger.warn(e.toString(), e);
      }
    }

    BoxDocument fullDocument = mapper.readValue(JSON.serialize(document), BoxDocument.class);

    // for some reason jackson deserializes the facets into a HashSet instead of a LinkedHashSet, so
    // the facets get out of order, this corrects that
    @SuppressWarnings("unchecked")
    List<Document> facets = (List<Document>) document.get("facets");

    if (facets != null) {
      fullDocument.setFacets(new LinkedHashSet<>());

      for (Document facet : facets) {
        fullDocument.getFacets().add(mapper.readValue(JSON.serialize(facet), Facet.class));
      }
    }

    fullDocument.setId(document.getString("_id"));
    return fullDocument;
  }

  @Override
  public Set<String> listSourceDependencies() {
    Set<String> sourceDependencies = new HashSet<>();

    for (String dependency : metadata.distinct("dependencies.sourceName", String.class)) {
      // mongo 3.6 returns null when no matches are found
      if (dependency != null) {
        sourceDependencies.add(dependency);
      }
    }

    return sourceDependencies;
  }

  @Override
  public int addToQueue(Duration olderThan) {
    Date oldDate = Date.from(Instant.now().minus(olderThan));
    Document query =
        new Document("processed", new Document("$lt", oldDate)).append("parentId", null);
    int count = 0;

    for (Document document : metadata.find(query)) {
      addToQueue(Arrays.asList(document.getString("_id")), false);
      count++;
    }

    return count;
  }

  @Override
  public void removeDeleted(Duration olderThan) {
    Date oldDate = Date.from(Instant.now().minus(olderThan));
    Document query =
        new Document("modified", new Document("$lt", oldDate))
            .append("status", BoxDocument.Status.DELETED.toString());

    for (Document document : metadata.find(query)) {
      String id = document.getString("_id");
      queue.deleteOne(eq("_id", id));
      this.documents.deleteOne(eq("_id", id));
      metadata.deleteOne(eq("_id", id));
    }
  }

  @Override
  public JsonNode findRegistryValue(String id) {
    Document document = registry.find(eq("_id", id)).first();

    if (document != null) {
      Object data = document.get("value");

      if (data != null) {
        JsonNode dataNode = mapper.readTree(JSON.serialize(data));
        return dataNode;
      }
    }

    return null;
  }

  @Override
  public void saveRegistryValue(String id, JsonNode data) {
    Document document = new Document("_id", id);
    document.append("value", Document.parse(data.toString()));
    registry.replaceOne(eq("_id", id), document, new ReplaceOptions().upsert(true));
  }

  /** @return the sourceName */
  public String getSourceName() {
    return sourceName;
  }

  /** @param sourceName the sourceName to set */
  public void setSourceName(String sourceName) {
    this.sourceName = sourceName;
  }

  /** @return the source */
  public Source getSource() {
    return source;
  }

  /** @param source the source to set */
  public void setSource(Source source) {
    this.source = source;
  }

  @Override
  public Map<DocumentId, Set<String>> findDependents(Collection<DocumentId> dependencies) {
    Map<DocumentId, Set<String>> map = new HashMap<>();

    for (DocumentId dependency : dependencies) {
      Set<String> ids = new HashSet<>();
      String id = dependency.getId();
      String sourceName = dependency.getSourceName();

      Document query =
          new Document("dependencies", new Document("sourceName", sourceName).append("id", id));

      for (Document document : metadata.find(query)) {
        ids.add(document.getString("_id"));
      }

      map.put(dependency, ids);
    }

    return map;
  }

  @Override
  public void clear() {
    // drop all collections
    queue.drop();
    cursor.drop();
    metadata.drop();
    groups.drop();
    documents.drop();
    registry.drop();

    // recreate collections with proper indexes
    init();
  }
}
