/** */
package edu.byu.hbll.box;

import edu.byu.hbll.box.BoxDocument;

/** @author Charles Draper */
public class EndGroupDocument extends BoxDocument {

  public EndGroupDocument(String groupId) {
    super(groupId);
  }
}
