/** */
package edu.byu.hbll.box;

/**
 * A snapshot of the health of a particular source.
 *
 * @author Charles Draper
 */
public class SourceHealth {

  private String sourceName;

  private boolean healthy;

  private String message;

  /**
   * @param sourceName the source name
   * @param healthy whether the source is healthy or not
   * @param message a status message (generally null if healthy)
   */
  public SourceHealth(String sourceName, boolean healthy, String message) {
    this.sourceName = sourceName;
    this.healthy = healthy;
    this.message = message;
  }

  /** @return the source name */
  public String getSourceName() {
    return sourceName;
  }

  /** @return whether the source is healthy or not */
  public boolean isHealthy() {
    return healthy;
  }

  /** @return a status message (generally null if healthy) */
  public String getMessage() {
    return message;
  }
}
