/** */
package edu.byu.hbll.box.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.Box;
import edu.byu.hbll.box.BoxConfigurable;
import edu.byu.hbll.box.BoxDatabase;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.DocumentId;
import edu.byu.hbll.box.Facet;
import edu.byu.hbll.box.HarvestContext;
import edu.byu.hbll.box.HarvestResult;
import edu.byu.hbll.box.Harvester;
import edu.byu.hbll.box.InitConfig;
import edu.byu.hbll.box.MetadataLevel;
import edu.byu.hbll.box.ProcessBatch;
import edu.byu.hbll.box.ProcessContext;
import edu.byu.hbll.box.ProcessResult;
import edu.byu.hbll.box.Processor;
import edu.byu.hbll.box.QueryResult;
import edu.byu.hbll.box.ReadOnlyDatabase;
import edu.byu.hbll.box.client.BoxClient;
import edu.byu.hbll.box.internal.core.BoxSourceBaseUri;
import edu.byu.hbll.json.JsonField;

/**
 * A read-only and realtime view into an internal or external box source. All communication occurs
 * over http(s). The view does not cache anything with the exception of the cursor. The cursor is
 * used to know when updates occur at the underlying source and to be able to notify dependents of
 * updates. This implements both {@link BoxDatabase} and {@link Harvester}.
 *
 * <pre>
 * SOURCENAME:
 *   template:
 *     id: view
 *     params:
 *       uri: https://www.example.com/box
 * </pre>
 *
 * @author Charles Draper
 */
public class View implements Processor, Harvester, ReadOnlyDatabase {

  static final Logger logger = LoggerFactory.getLogger(View.class);

  private static final int NO_SPECIFIED_LIMIT = -1;

  /** */
  private BoxClient boxClient;

  /** The baseUri of the other box. */
  private String uri;

  /** The local source name of the upon which the view is built. */
  private String local;

  /** Only return these document fields. */
  private Set<String> fields = new LinkedHashSet<>();

  /** Include documents with these statuses. */
  private Set<BoxDocument.Status> statuses = new BoxQuery().getStatuses();

  /** Filter by these facets only. */
  private Set<Facet> facets = new LinkedHashSet<>();

  /** Set of facet groups represented by facets. */
  private Set<String> facetGroups = new LinkedHashSet<>();

  /** Limit of documents per request. */
  private int limit = NO_SPECIFIED_LIMIT;

  /** */
  private String sourceName;

  /** If documents come in through harvester, they should be marked as unprocessed. */
  private boolean harvestUnprocessed;

  /**
   * For a typical view, harvesting still takes place in order to know what has been updated. This
   * is important for a box that uses the view as a dependency. Dependents of the view need to know
   * when a document has been updated so that their corresponding dependent documents can also be
   * updated. When set to true, this only harvests metadata so as to know what's been updated.
   */
  private boolean harvestForView;

  private Box box;

  /** */
  protected View() {}

  /**
   * Creates a new View with the given baseUri to the box instance.
   *
   * @param uri base URI for the box instance, the path should end with /box
   */
  public View(String uri) {
    this(new BoxClient(uri));
  }

  /**
   * Creates a new View with the given baseUri to the box instance.
   *
   * @param boxClient the client for communicating with the remote box
   */
  public View(BoxClient boxClient) {
    this.boxClient = Objects.requireNonNull(boxClient);
    this.uri = boxClient.getUri();
  }

  /**
   * Creates a new View with the given local source for which this view is based.
   *
   * @param box the singleton box instance
   * @param sourceName source name of this source
   * @param local the name of the local source that this is based on
   */
  public View(Box box, String sourceName, String local) {
    this.box = box;
    this.sourceName = sourceName;
    this.local = local;
    registerForLocalUpdates();
  }

  /**
   * See {@link BoxConfigurable#postConstruct(InitConfig)}.
   *
   * @param config
   */
  public View(InitConfig config) {
    postConstruct(config);
  }

  /**
   * {@inheritDoc}
   *
   * <p>uri: base URI for the box instance, the path should end with /box or a specific source
   * /SOURCE_NAME (required if local is also null)
   *
   * <p>local: source name for underlying local source, uri is ignored if this is set.
   *
   * <p>fields: array of the particular fields to return for each document (optional)
   *
   * <p>statuses: only include documents with these statuses. Default: [READY,DELETED] (optional)
   *
   * <p>facets: filter to these collections only, key value pairs in the form of "name:value"
   * (optional)
   *
   * <p>limit: limit of documents per request (optional)
   *
   * <p>username: username to use with basic authentication. (optional)
   *
   * <p>password: password to use with basic authentication. (optional)
   */
  @Override
  public void postConstruct(InitConfig config) {
    this.box = config.getBox();

    ObjectNode params = config.getParams();

    local = params.path("local").asText(null);

    if (local == null) {
      uri = Objects.requireNonNull(params.path("uri").asText(params.path("baseUri").asText(null)));

      String username = params.path("username").asText(null);
      String password = params.path("password").asText(null);

      if (username != null || password != null) {
        this.boxClient = new BoxClient(uri, username, password);
      } else {
        this.boxClient = new BoxClient(uri);
      }
    } else {
      registerForLocalUpdates();
    }

    for (JsonNode field : params.path("fields")) {
      fields.add(field.asText());
    }

    for (JsonNode status : params.path("statuses")) {
      statuses.add(BoxDocument.Status.valueOf(status.asText().toUpperCase()));
    }

    for (JsonField facet : new JsonField(params.path("facets"))) {
      if (!facet.getValue().isNull()) {
        facets.add(Facet.parse(facet.getValue().asText()));
      }
    }

    updateFacetGroups();

    limit = params.path("limit").asInt(NO_SPECIFIED_LIMIT);

    harvestUnprocessed = params.path("harvestUnprocessed").asBoolean();
    harvestForView = params.path("harvestForView").asBoolean();
  }

  /**
   * {@inheritDoc}
   *
   * <p>Requests documents denoted by the {@link ProcessBatch} from the underlying source. Documents
   * are pared according to the `fields` parameter and the full metadata is retrieved (minus groups
   * and dependencies). Existing documents at the underlying source that do not match the
   * preconfigured facets will be marked as deleted. Optionally, subclasses can transform the list
   * of documents before being returned in the {@link ProcessResult}. First {@link
   * #transform(ProcessBatch)} is called followed by {@link #transform(List)}. When overriding
   * {@link #transform(ProcessBatch)}, the document is put into the {@link ProcessContext} as a
   * dependency with this sourceName and the corresponding document's id.
   */
  @Override
  public ProcessResult process(ProcessBatch batch) {
    List<String> ids = batch.getIds();
    BoxQuery query = new BoxQuery().addIds(ids).setMetadataLevel(MetadataLevel.FULL);
    QueryResult queryResult = findAndTransform(query, batch);
    return new ProcessResult().addDocuments(queryResult);
  }

  /**
   * {@inheritDoc}
   *
   * <p>Requests documents denoted by the {@link HarvestContext} from the underlying source.
   * Documents are pared according to the `fields` parameter and the full metadata is retrieved
   * (minus groups and dependencies). Only documents matching the preconfigured facets and statuses
   * are returned (default to no facets and statuses of ready and deleted). Optionally, subclasses
   * can transform the list of documents before being returned in the {@link HarvestResult}. First
   * {@link #transform(ProcessBatch)} is called followed by {@link #transform(List)}. When
   * overriding {@link #transform(ProcessBatch)}, the document is put into the {@link
   * ProcessContext} as a dependency with this sourceName and the corresponding document's id.
   */
  @Override
  public HarvestResult harvest(HarvestContext context) {
    HarvestResult result = new HarvestResult();

    long cursor = context.getCursor().path("cursor").asLong();

    BoxQuery query =
        new BoxQuery().setCursor(cursor).setLimit(limit).setMetadataLevel(MetadataLevel.FULL);

    if (harvestForView || harvestUnprocessed) {
      query.setMetadataOnly(true);

      // can be removed shortly. this is here because not all boxes are using metadataonly yet
      query.setFields(Arrays.asList("id"));
    }

    InfoQueryResult queryResult = findAndTransform(query);

    result.withCursor().put("cursor", queryResult.getNextCursor() + "");

    if (harvestUnprocessed) {
      for (BoxDocument transformedDocument : queryResult) {
        result.add(new BoxDocument(transformedDocument.getId()));
      }
    } else {
      result.addDocuments(queryResult);
    }

    result.setMore(queryResult.more);

    return result;
  }

  /**
   * {@inheritDoc}
   *
   * <p>Requests documents denoted by the {@link BoxQuery} from the underlying source. Documents are
   * pared according to the `fields` parameter and the full metadata is retrieved (minus groups and
   * dependencies). Only documents matching the preconfigured facets and statuses are returned
   * (default to no facets and statuses of ready and deleted). When requesting by id, existing
   * documents at the underlying source that do not match the preconfigured facets will be marked as
   * deleted. Optionally, subclasses can transform the list of documents before being returned in
   * the {@link HarvestResult}. First {@link #transform(ProcessBatch)} is called followed by {@link
   * #transform(List)}. When overriding {@link #transform(ProcessBatch)}, the document is put into
   * the {@link ProcessContext} as a dependency with this sourceName and the corresponding
   * document's id.
   */
  @Override
  public QueryResult find(BoxQuery query) {
    return findAndTransform(new BoxQuery(query).setMetadataLevel(MetadataLevel.FULL));
  }

  /**
   * @param query
   * @return
   */
  private InfoQueryResult findAndTransform(BoxQuery query) {
    return findAndTransform(query, new ProcessBatch());
  }

  /**
   * @param query
   * @param batch
   * @return
   */
  private InfoQueryResult findAndTransform(BoxQuery query, ProcessBatch batch) {
    InfoQueryResult result = new InfoQueryResult(rawFind(query));
    List<BoxDocument> documents = transform(transform(makeBatch(result, batch)));
    result.clear();
    result.addAll(documents);
    return result;
  }

  /**
   * Prepares {@link ProcessBatch} with appropriate dependencies.
   *
   * @param documents
   * @param batch
   * @return
   */
  private ProcessBatch makeBatch(List<BoxDocument> documents, ProcessBatch batch) {
    ProcessBatch newBatch = new ProcessBatch();

    for (BoxDocument document : documents) {
      Map<DocumentId, BoxDocument> dependencies = new HashMap<>();
      ProcessContext context = batch.get(document.getId());

      if (context != null) {
        dependencies.putAll(context.getDependencies());
      }

      dependencies.put(new DocumentId(sourceName, document.getId()), document);
      newBatch.add(new ProcessContext(document.getId(), dependencies));
    }

    return newBatch;
  }

  /**
   * Same as {@link #find(BoxQuery)} except it does not perform any transformation other than
   * applying the preconfigured `fields`, `facets`, and `statuses`.
   *
   * @param query the query to run against the underlying source
   * @return the query result
   */
  public QueryResult rawFind(BoxQuery query) {
    return rawFind(query, false);
  }

  /**
   * Same as {@link #find(BoxQuery)} except it does not perform any transformation other than
   * applying the preconfigured `fields`, `facets`, and `statuses`. It additionally pages to the end
   * of the result set gathering all results if desired.
   *
   * @param query the query to run against the underlying source
   * @param pageToEnd whether or not to page to the end of the result set
   * @return the query result
   */
  public QueryResult rawFind(BoxQuery query, boolean pageToEnd) {
    return rawFind(query, pageToEnd ? Integer.MAX_VALUE : -1);
  }

  /**
   * Same as {@link #find(BoxQuery)} except it does not perform any transformation other than
   * applying the preconfigured `fields`, `facets`, and `statuses`. It additionally pages to a
   * certain number of documents.
   *
   * @param query the query to run against the underlying source
   * @param pageToLimit if number of documents found is less than pageToLimit, continue paging until
   *     pageToLimit documents is found. If the number of documents found is greater than
   *     pageToLimit, then the document list will be truncated. A negative value causes pageToLimit
   *     to be completely ignored and so the limit on the query itself is honored.
   * @return the query result
   */
  public QueryResult rawFind(BoxQuery query, int pageToLimit) {
    try {
      query = new BoxQuery(query);

      // if we're not careful, a query specifying particular facets and/or fields can cause the view
      // to divulge information it shouldn't. These next steps make sure facets or fields in the query
      // will not expose documents or document fields that should be hidden.

      // make sure the OR logic within a facet group does not expose documents being hidden by
      // this.facets
      boolean forceEmptyResults = false;

      // if this view and the query both specify facets
      if (!this.facets.isEmpty() && !query.getFacets().isEmpty()) {
        Set<String> facetGroups = new HashSet<>();

        // get all facet groups represented from the query
        for (Facet facet : query.getFacets()) {
          facetGroups.add(facet.getName());
        }

        // final facets to use
        List<Facet> facets = new ArrayList<>();
        Set<String> finalFacetGroups = new HashSet<>();

        // only add facets from query that appear in this view's set of facets or whose group is not
        // represented by the view
        for (Facet facet : query.getFacets()) {
          if (this.facets.contains(facet) || !this.facetGroups.contains(facet.getName())) {
            facets.add(facet);
            finalFacetGroups.add(facet.getName());
          }
        }

        // only add facets from this view that are not part of the query's facet groups
        for (Facet facet : this.facets) {
          if (!facetGroups.contains(facet.getName())) {
            facets.add(facet);
          }
        }

        // only return results if all facet groups from query are represented in final facets
        if (facetGroups.size() == finalFacetGroups.size()) {
          query.setFacets(facets);
        } else {
          forceEmptyResults = true;
          query.setFacets(this.facets);
        }
      } else {
        query.addFacets(this.facets);
      }

      // if there are preconfigured statuses
      if (!this.statuses.isEmpty()) {
        if (query.getStatuses().isEmpty()) {
          // add preconfigured statuses if query doesn't specify any
          query.setStatuses(this.statuses);
        } else {
          // only return documents with intersection of preconfigured statuses and query statuses
          query.getStatuses().retainAll(this.statuses);

          // if there is no intersection
          forceEmptyResults |= query.getStatuses().isEmpty();
        }
      }

      // fields specified in query may only be activated if they are subfields of view fields
      boolean forceEmptyDocuments = false;

      // if there are preconfigured fields
      if (!this.fields.isEmpty()) {
        List<String> fields = new ArrayList<>();

        for (String field : query.getFields()) {
          for (String masterField : this.fields) {
            // use fields from query that are subfields of this view's fields
            if (field.equals(masterField) || field.startsWith(masterField + ".")) {
              fields.add(field);
            }

            // use masterField if it is a subfield of the query field
            if (masterField.startsWith(field + ".")) {
              fields.add(masterField);
            }
          }
        }

        if (query.getFields().isEmpty()) {
          fields.addAll(this.fields);
        }

        if (fields.isEmpty()) {
          // if there are no fields left, then documents' contents will be empty
          forceEmptyDocuments = true;
          query.setFields(this.fields);
        } else {
          query.setFields(fields);
        }
      }

      QueryResult result;

      if (local != null) {
        if (pageToLimit >= 0) {
          query.setLimit(pageToLimit);
        }

        result = box.find(local, query);
      } else {
        result = boxClient.find(query, pageToLimit);
      }

      if (forceEmptyResults) {
        result = new QueryResult();
      }

      // remove dependencies and groupId
      for (int i = 0; i < result.size(); i++) {
        BoxDocument document = result.get(i);
        document.setDependencies(Collections.emptyList());
        document.setGroupId(null);

        // for id type queries, mark as deleted if document doesn't have required facet(s)
        if (document.isReady() && !document.matches(facets)) {
          result.set(i, new BoxDocument(document.getId()).setAsDeleted());
        }
      }

      if (forceEmptyDocuments) {
        result.forEach(d -> d.setDocument(JsonNodeFactory.instance.objectNode()));
      }

      result = new QueryResult(result).setNextCursor(result.getNextCursor());

      return result;
    } catch (Exception e) {
      String message;

      if (e instanceof WebApplicationException) {
        Response response = ((WebApplicationException) e).getResponse();
        message = response.readEntity(String.class) + ", " + response;
      } else {
        message = e.getMessage();
      }

      logger.error(
          "Error in view "
              + sourceName
              + " while querying "
              + (local != null ? local : uri)
              + ": "
              + message);
      throw e;
    }
  }

  /**
   * Extend this class and override this method in order to transform the result documents in any
   * way from the direct processor. You can ignore certain documents by simply not including them in
   * the resulting collection. By default the processed flag is set to false (this flag tells box to
   * simply put the id on the queue).
   *
   * <p>Note: You may modify the documents in place.
   *
   * @param batch batch to transform
   * @return list of {@link BoxDocument}s to return to box
   */
  protected List<BoxDocument> transform(ProcessBatch batch) {
    return batch
        .stream()
        .map(c -> c.getDependency(sourceName, c.getId()))
        .collect(Collectors.toList());
  }

  /**
   * Extend this class and override this method in order to transform the result documents in any
   * way from the direct processor. You can ignore certain documents by simply not including them in
   * the resulting collection. By default the processed flag is set to false (this flag tells box to
   * simply put the id on the queue).
   *
   * <p>Note: You may modify the documents in place.
   *
   * @param documents documents to transform
   * @return list of {@link BoxDocument}s to return to box
   */
  protected List<BoxDocument> transform(List<BoxDocument> documents) {
    return documents;
  }

  /** */
  private void registerForLocalUpdates() {
    box.registerForUpdateNotifications(local, () -> box.triggerHarvest(sourceName));
  }

  /** */
  private void updateFacetGroups() {
    facets.forEach(f -> facetGroups.add(f.getName()));
  }

  /**
   * @deprecated use {@link #getUri()} instead.
   * @return the baseUri
   */
  @Deprecated
  public String getBaseUri() {
    return uri;
  }

  /**
   * @deprecated use the constructor instead.
   * @param baseUri the baseUri to set
   */
  @Deprecated
  public void setBaseUri(String baseUri) {
    this.uri = Objects.requireNonNull(baseUri);
  }

  /** @return the uri */
  @BoxSourceBaseUri
  public String getUri() {
    return uri;
  }

  /** @return the fields */
  public Set<String> getFields() {
    return fields;
  }

  /** @param fields the particular fields to return for each document */
  public void setFields(Collection<String> fields) {
    this.fields.clear();
    this.fields.addAll(fields);
  }

  /** @return the statuses */
  public Set<BoxDocument.Status> getStatuses() {
    return statuses;
  }

  /** @param statuses the statuses to set */
  public void setStatuses(Collection<BoxDocument.Status> statuses) {
    this.statuses.clear();
    this.statuses.addAll(statuses);
  }

  /** @return the facets */
  public Set<Facet> getFacets() {
    return facets;
  }

  /** @param facets the facets to set */
  public void setFacets(Collection<Facet> facets) {
    this.facets.clear();
    this.facets.addAll(facets);
    updateFacetGroups();
  }

  /** @return the limit */
  public int getLimit() {
    return limit;
  }

  /** @param limit the limit of documents per request to set */
  public void setLimit(int limit) {
    this.limit = limit;
  }

  /** @return the sourceName */
  public String getSourceName() {
    return sourceName;
  }

  /** @param sourceName the sourceName to set */
  public void setSourceName(String sourceName) {
    this.sourceName = sourceName;
  }

  /** @return the harvestUnprocessed */
  public boolean isHarvestUnprocessed() {
    return harvestUnprocessed;
  }

  /** @param harvestUnprocessed the harvestUnprocessed to set */
  public void setHarvestUnprocessed(boolean harvestUnprocessed) {
    this.harvestUnprocessed = harvestUnprocessed;
  }

  /** @return the harvestForView */
  public boolean isHarvestForView() {
    return harvestForView;
  }

  /** @param harvestForView the harvestForView to set */
  public void setHarvestForView(boolean harvestForView) {
    this.harvestForView = harvestForView;
  }

  /** @return the local */
  public String getLocal() {
    return local;
  }

  /** @return the boxClient */
  public BoxClient getBoxClient() {
    return boxClient;
  }

  /** @return the box */
  public Box getBox() {
    return box;
  }
  
  private class InfoQueryResult extends QueryResult {

    private static final long serialVersionUID = 1L;
    
    private boolean more;
    
    private InfoQueryResult(QueryResult result) {
      super(result);
      setNextCursor(result.getNextCursor());
      more = !result.isEmpty();
    }
  }
}
