/** */
package edu.byu.hbll.box.impl;

import java.net.URI;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.UriBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxDocument.Status;
import edu.byu.hbll.box.HarvestContext;
import edu.byu.hbll.box.HarvestResult;
import edu.byu.hbll.box.Harvester;
import edu.byu.hbll.xml.XmlJsonConverter;
import edu.byu.hbll.xml.XmlUtils;

/**
 * A client used to harvest records using the OAI protocol. This harvester has one additional
 * feature in that it can harvest in date chunks. This is vital when harvesting from contentdm. By
 * grouping records by date, we overcome a bug in contentdm that can't handle paging through a lot
 * of results.
 *
 * @author Charles Draper
 */
public class OaiHarvester implements Harvester {

  static final Logger logger = LoggerFactory.getLogger(OaiHarvester.class);

  /** A default metadata prefix */
  public static final String DEFAULT_METADATA_PREFIX = "oai_dc";

  /** The jax-rs client to use to communicate with Dacapo */
  private Client client = ClientBuilder.newClient();

  /** The base oai uri */
  private String uri;

  /** The oai metada prefix */
  private String metadataPrefix = DEFAULT_METADATA_PREFIX;

  /**
   * Whether or not to query by finite date ranges in this case one month at a time. This is vital
   * when harvesting from contentdm. By grouping records by date, we overcome a bug in contentdm
   * that can't handle paging through a lot of results.
   */
  private boolean chunked;

  /**
   * Whether or not to harvest ids only instead of full records. This can be useful if certain
   * records are crashing the record harvests due to invalid xml. The full records will be pulled
   * down individually later during maintenance if set to true.
   */
  private boolean idsOnly;

  /** */
  private String idRegex;

  /** */
  private String idReplacement;

  /** A white list of setSpecs to harvest */
  private Set<String> sets = new HashSet<>();

  private boolean removeNamespaces;

  @Override
  public HarvestResult harvest(HarvestContext context) {

    // extract cursor information
    ObjectNode cursor = context.getCursor();
    String resumptionToken = cursor.path("resumptionToken").asText(null);
    String fromString = cursor.path("from").asText(null);

    // need to start on day two to get off of the boundary of valid dates and non valid dates
    LocalDate from = LocalDate.ofEpochDay(1);

    // parse from
    if (fromString != null) {
      try {
        from = LocalDate.parse(fromString);
      } catch (Exception e) {
        throw new IllegalArgumentException("Cannot parse from date in cursor : " + e);
      }
    }

    LocalDate until = LocalDate.now();

    // if chunked, set until only one month in the future
    if (chunked && until.isAfter(from.plusMonths(1))) {
      until = from.plusMonths(1);
    }

    // build target
    String verb = idsOnly ? "ListIdentifiers" : "ListRecords";
    UriBuilder uriBuilder = UriBuilder.fromUri(uri).queryParam("verb", verb);

    // use resumption token if it is not empty
    if (resumptionToken != null && !resumptionToken.isEmpty()) {
      uriBuilder = uriBuilder.queryParam("resumptionToken", resumptionToken);
    } else {
      // otherwise use dates
      uriBuilder =
          uriBuilder
              .queryParam("from", from.toString())
              .queryParam("until", until.toString())
              .queryParam("metadataPrefix", metadataPrefix);
    }

    URI uri = uriBuilder.build();
    logger.info(uri.toString());

    // execute request
    Document response = client.target(uri).request().get(Document.class);

    // extract any errors
    List<Element> errors = XmlUtils.getElements(response, "OAI-PMH", "error");

    // end processing here if there are errors
    if (!errors.isEmpty()) {
      Element error = errors.get(0);

      if (error.getAttribute("code").equals("noRecordsMatch")) {
        // ignore error and continue
      } else {
        throw new RuntimeException(
            error.getAttribute("code") + " : " + error.getTextContent() + " : " + uri);
      }
    }

    // if set to only retrieve ids, rebuild the document to match the structure of the request to
    // retrieve
    // records
    if (idsOnly) {
      rebuild(response);
    }

    HarvestResult result = new HarvestResult();
    result.setCursor(cursor);

    // extract harvest records from the result document
    List<BoxDocument> resultDocuments = extract(response);
    result.addDocuments(resultDocuments);

    // get the resumption token
    resumptionToken = getResumptionToken(response);

    cursor.put("resumptionToken", resumptionToken);
    cursor.put("from", from.toString());

    // if there is a resumption token then there is more
    if (resumptionToken != null && !resumptionToken.isEmpty()) {
      result.setMore(true);
    } else {
      cursor.put("from", until.toString());

      if (until.isBefore(LocalDate.now())) {
        result.setMore(true);
      }
    }

    return result;
  }

  /**
   * In the case of listing identifiers only, this reconstructs the resulting document to mimic the
   * records listings so we don't have to repeat so much code later on.
   *
   * @param doc the resulting document
   */
  private void rebuild(Document doc) {
    Element root = XmlUtils.getElement(doc, "OAI-PMH");
    Element listRecords = doc.createElementNS("dummy", "ListRecords");
    root.appendChild(listRecords);

    logger.debug("LocalName: \"" + listRecords.getLocalName() + "\"");

    for (Node header : XmlUtils.getElements(doc, "OAI-PMH", "ListIdentifiers", "header")) {
      Element record = doc.createElementNS("dummy", "record");
      listRecords.appendChild(record);
      record.appendChild(header);
    }
  }

  /**
   * Creates {@link ProcessRecord}s out of the resulting document. This is what is ultimately sent
   * back to the harvest task.
   *
   * @param doc the resulting document
   * @return a list of {@link ProcessRecord}s to be harvested
   * @throws Exception if something goes wrong
   */
  private List<BoxDocument> extract(Document doc) {
    List<Document> records = new ArrayList<>();
    List<BoxDocument> boxDocuments = new ArrayList<>();
    List<Element> nodes = XmlUtils.getElements(doc, "OAI-PMH", "ListRecords", "record");

    for (Element node : nodes) {

      Document record = XmlUtils.toDocument(node);

      String identifier =
          XmlUtils.getElement(record, "record", "header", "identifier").getTextContent();
      String id = idRegex != null ? identifier.replaceAll(idRegex, idReplacement) : identifier;

      List<Element> setNodes = XmlUtils.getElements(record, "record", "header", "setSpec");
      List<String> sets = new ArrayList<>();

      for (Element setNode : setNodes) {
        sets.add(setNode.getTextContent());
      }

      if (this.sets != null && !this.sets.isEmpty()) {
        // if set is not part of the white list of sets then ignore it
        if (Collections.disjoint(this.sets, sets)) {
          continue;
        }
      }

      BoxDocument resultDocument = new BoxDocument(id);

      // status is an optional attribute in the header that can state that the
      // record is deleted, continue if it is
      if ("deleted".equals(XmlUtils.getElement(node, "header").getAttribute("status"))) {
        resultDocument = new BoxDocument(id, Status.DELETED);
      } else if (!idsOnly) {
        Document recordToConvert = record;

        if (removeNamespaces) {
          recordToConvert = XmlUtils.removeNamespaces(recordToConvert);
        }
        
        ObjectNode document = XmlJsonConverter.toJsonDoc(recordToConvert);
        resultDocument = new BoxDocument(id, document);

        for (String set : sets) {
          resultDocument.addFacet("set", set);
        }
      }

      records.add(record);
      boxDocuments.add(resultDocument);
    }
    
    return transform(records, boxDocuments);
  }
  
  /**
   * @param records
   * @param boxDocuments
   * @return
   */
  protected List<BoxDocument> transform(List<Document> records, List<BoxDocument> boxDocuments) {
    return boxDocuments;
  }

  /**
   * Retrieves the resumption token from the result.
   *
   * @param result the oai response
   * @return the resumption token if there is one, null otherwise
   */
  private String getResumptionToken(Document response) {
    Element resumptionToken =
        XmlUtils.getElement(response, "OAI-PMH", "ListRecords", "resumptionToken");
    resumptionToken =
        resumptionToken == null
            ? XmlUtils.getElement(response, "OAI-PMH", "ListIdentifiers", "resumptionToken")
            : resumptionToken;
    return resumptionToken == null ? null : resumptionToken.getTextContent();
  }

  /** @return the client */
  public Client getClient() {
    return client;
  }

  /** @param client the client to set */
  public void setClient(Client client) {
    Objects.requireNonNull(client);
    this.client = client;
  }

  /** @return the uri */
  public String getUri() {
    return uri;
  }

  /** @param uri the uri to set */
  public void setUri(String uri) {
    Objects.requireNonNull(uri);
    this.uri = uri;
  }

  /** @return the metadataPrefix */
  public String getMetadataPrefix() {
    return metadataPrefix;
  }

  /** @param metadataPrefix the metadataPrefix to set */
  public void setMetadataPrefix(String metadataPrefix) {
    Objects.requireNonNull(metadataPrefix);
    this.metadataPrefix = metadataPrefix;
  }

  /** @return the idRegex */
  public String getIdRegex() {
    return idRegex;
  }

  /** @param idRegex the idRegex to set */
  public void setIdRegex(String idRegex) {
    this.idRegex = idRegex;
  }

  /** @return the idReplacement */
  public String getIdReplacement() {
    return idReplacement;
  }

  /** @param idReplacement the idReplacement to set */
  public void setIdReplacement(String idReplacement) {
    this.idReplacement = idReplacement;
  }

  /** @return the chunked */
  public boolean isChunked() {
    return chunked;
  }

  /** @param chunked the chunked to set */
  public void setChunked(boolean chunked) {
    this.chunked = chunked;
  }

  /** @return the idsOnly */
  public boolean isIdsOnly() {
    return idsOnly;
  }

  /** @param idsOnly the idsOnly to set */
  public void setIdsOnly(boolean idsOnly) {
    this.idsOnly = idsOnly;
  }

  /** @return the sets */
  public Set<String> getSets() {
    return sets;
  }

  /** @param sets the sets to set */
  public void setSets(Set<String> sets) {
    Objects.requireNonNull(sets);
    this.sets = sets;
  }

  /** @return the removeNamespaces */
  public boolean isRemoveNamespaces() {
    return removeNamespaces;
  }

  /** @param removeNamespaces the removeNamespaces to set */
  public void setRemoveNamespaces(boolean removeNamespaces) {
    this.removeNamespaces = removeNamespaces;
  }
}
