/** */
package edu.byu.hbll.box.internal.core;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxDatabase;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxDocument.Status;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.DocumentId;
import edu.byu.hbll.box.EndGroupDocument;
import edu.byu.hbll.box.HarvestContext;
import edu.byu.hbll.box.HarvestResult;
import edu.byu.hbll.box.QueryResult;
import edu.byu.hbll.box.Source;
import edu.byu.hbll.box.StartGroupDocument;
import edu.byu.hbll.box.internal.util.CursorUtils;

/** */
public class DocumentHandler {

  private static final int QUERY_BATCH_SIZE = 10;

  /** */
  static final Logger logger = LoggerFactory.getLogger(DocumentHandler.class);

  /** */
  private Registry registry;

  /** */
  private UpdatesNotifier updatesNotifier;

  /** */
  public DocumentHandler(Registry registry) {
    this.registry = registry;
    this.updatesNotifier = registry.getUpdatesNotifier();
  }

  public QueryResult find(String sourceName, BoxQuery query, OutputStream out) {
    if (out == null) {
      return find(sourceName, query);
    } else {
      Queue<String> ids = new LinkedList<>(query.getIds());
      QueryResult result = null;
      BoxQuery batchQuery = new BoxQuery(query);
      boolean first = true;
      boolean isId = query.isId();
      long total = 0;

      do {
        if (isId) {
          batchQuery.setIds(nextIds(ids));
        } else {
          batchQuery.setLimit(Math.min(query.getLimit() - total, QUERY_BATCH_SIZE));
        }

        result = find(sourceName, batchQuery);
        total += result.size();
        batchQuery.setCursor(result.getNextCursor());

        write(batchQuery, out, result, first);
        first = false;
      } while (!isId && !result.isEmpty() && total < query.getLimit() || isId && !ids.isEmpty());

      return new QueryResult().setNextCursor(result.getNextCursor());
    }
  }

  private List<String> nextIds(Queue<String> ids) {
    List<String> nextIds = new ArrayList<>();

    for (int i = 0; i < QUERY_BATCH_SIZE && !ids.isEmpty(); i++) {
      nextIds.add(ids.poll());
    }

    return nextIds;
  }

  private void write(BoxQuery query, OutputStream out, QueryResult result, boolean first) {
    for (BoxDocument document : result) {
      if (!first) {
        try {
          out.write(',');
        } catch (IOException e) {
          throw new UncheckedIOException(e);
        }
      }

      document.write(out, query.getMetadataLevel());
      first = false;
    }
  }

  /**
   * Retrieves documents from the database and returns them. If the process flag is set, documents
   * are processed and then returned rather than retrieving the cached versions. If the wait flag is
   * set, cached documents are retrieved, any unprocessed documents are processed, and all are
   * returned. If no flag is set, all documents are retrieved from the database. Any unprocessed
   * ones are indicated so.
   *
   * @param sourceName the source to query
   * @param query the query
   * @return documents in the same number and order as requested
   */
  public QueryResult find(String sourceName, BoxQuery query) {

    sourceName = registry.verifySource(sourceName);
    Source source = registry.getSource(sourceName);

    if (query.getLimit() < 0) {
      query = new BoxQuery(query).setLimit(source.getDefaultLimit());
    }

    QueryResult result;

    // if process is true, (re)process the documents and wait
    if (query.isId() && (query.isProcess() || source.isProcess()) && source.isProcessEnabled()) {
      result = new QueryResult(process(sourceName, query.getIds()));
      result.updateNextCursor(query);
    } else {
      // pull documents from the database
      result = source.getDb().find(query);

      if (query.isId()) {

        // gather unprocessed document ids
        List<String> unprocessed =
            result
                .stream()
                .filter(d -> !d.isProcessed())
                .map(d -> d.getId())
                .collect(Collectors.toList());

        // if wait is true, process the unprocessed documents and wait
        if (query.isWait() && !unprocessed.isEmpty() && source.isProcessEnabled()) {
          // process the unprocessed ones
          List<BoxDocument> processed = process(sourceName, unprocessed);

          // index by id for fast lookup
          Map<String, BoxDocument> indexed =
              processed.stream().collect(Collectors.toMap(d -> d.getId(), Function.identity()));

          // interleave the newly processed ones with the previously processed ones
          List<BoxDocument> latest =
              result
                  .stream()
                  .map(d -> d.isProcessed() ? d : indexed.get(d.getId()))
                  .collect(Collectors.toList());

          result = new QueryResult(latest);
          result.updateNextCursor(query);
        } else {
          queue(sourceName, unprocessed);
        }
      }
    }

    // if there is no processor or processor is disabled
    if (!source.isProcessEnabled()) {
      // mark every unprocessed document as deleted
      result.stream().filter(d -> d.isUnProcessed()).forEach(d -> d.setAsDeleted());
    }

    return result;
  }

  /**
   * Processes and saves the documents for the given sourceName and ids. Waits for the processing to
   * finish and returns the newly processed documents.
   *
   * @param sourceName the source name
   * @param ids the ids to be processed
   * @return the resulting newly processed documents
   */
  public List<BoxDocument> process(String sourceName, Collection<String> ids) {
    DocumentProcessor processor = registry.getDocumentProcessor(sourceName);
    List<BoxDocument> documents = processor.processAndWait(new ArrayList<>(ids));
    return documents;
  }

  /**
   * Queues up the ids for the given source to be processed in the background.
   *
   * @param sourceName the source name
   * @param ids the ids to queue
   */
  public void queue(String sourceName, Collection<String> ids) {
    QueueRunner queueRunner = registry.getQueue(sourceName);
    ids.forEach(id -> queueRunner.submit(id, false));
  }

  /**
   * Same as saveDocument, but pulls the oldDocument from the database first.
   *
   * @param document
   */
  public void save(String sourceName, BoxDocument document) {
    Source source = registry.getSource(sourceName);
    String id = document.getId();

    List<BoxDocument> oldDocuments;

    if (document instanceof StartGroupDocument || document instanceof EndGroupDocument) {
      oldDocuments = new ArrayList<>();
    } else {
      // ignore statuses because we need to include unprocessed documents that have dependencies
      oldDocuments = source.getDb().find(new BoxQuery().addId(id).setStatuses());
    }

    BoxDocument oldDocument = oldDocuments.isEmpty() ? new BoxDocument(id) : oldDocuments.get(0);

    save(sourceName, document, oldDocument);
  }

  /**
   * Saves the given document to the database. If the document already exists, it analyzes that
   * existing document to see what's been updated and takes the appropriate measures. These measures
   * include:
   *
   * <ul>
   *   <li>If the document has not changed, only the registry is updated with the new processed
   *       date.
   *   <li>If the document's dependencies have changed, the document is put back on the queue to be
   *       processed.
   *   <li>If this document is a dependency for other documents, those documents are put on the
   *       queue.
   * </ul>
   *
   * @param newDocument
   */
  private void save(String sourceName, BoxDocument newResultDocument, BoxDocument oldDocument) {

    Source source = registry.getSource(sourceName);
    String id = newResultDocument.getId();
    BoxDatabase db = source.getDb();

    if (newResultDocument instanceof StartGroupDocument) {
      db.startGroup(id);
    } else if (newResultDocument instanceof EndGroupDocument) {
      db.processOrphans(
          id, d -> save(source.getName(), new BoxDocument(d.getId(), Status.DELETED)));
    } else {

      BoxDocument newProcessDocument = new BoxDocument(newResultDocument);

      // validate dependency sources
      newResultDocument
          .getDependencies()
          .forEach(
              d -> {
                if (!registry.isSource(d.getSourceName())) {
                  throw new IllegalArgumentException("unrecognized source: " + d.getSourceName());
                }
              });

      // register facets
      newResultDocument.addFacetsByQuery(source.getFacetFields());

      if (!newResultDocument.isProcessed()) {
        if (!equals(newProcessDocument, oldDocument)) {
          if (oldDocument.isProcessed()) {
            // only update dependencies if old one is already processed
            db.updateDependencies(Arrays.asList(newResultDocument));
          } else {
            db.save(Arrays.asList(newResultDocument));
          }
        }
      } else if (!equals(newProcessDocument, oldDocument) || source.isOverwrite()) {
        // find documents dependent on this document
        Set<DocumentId> dependents = new HashSet<>();
        DocumentId dependency = new DocumentId(sourceName, id);

        for (String dependent : registry.getDependents(sourceName)) {

          registry
              .getSource(dependent)
              .getDb()
              .findDependents(Arrays.asList(dependency))
              .getOrDefault(dependency, Collections.emptySet())
              .forEach(d -> dependents.add(new DocumentId(dependent, d)));
        }

        newProcessDocument
            .getDependencies()
            .forEach(d -> registry.addDependency(sourceName, d.getSourceName()));

        if (newResultDocument.getModified() == null) {
          newResultDocument.setModified(Instant.now());
        }

        if (newResultDocument.getProcessed() == null) {
          newResultDocument.setProcessed(Instant.now());
        }

        if (newResultDocument.getCursor() == 0) {
          newResultDocument.setCursor(CursorUtils.nextCursor());
        }

        if (!source.isSave() || source.isDependencyOnly() && dependents.isEmpty()) {
          // do not save if false or dependency only
        } else {
          // save document
          db.save(Arrays.asList(newResultDocument));
        }

        // notify listeners that a new document is ready
        updatesNotifier.trigger(sourceName);

        // trigger dependent documents to be processed
        for (DocumentId dependent : dependents) {
          registry.getQueue(dependent.getSourceName()).submit(dependent.getId(), true);
        }
      } else {
        // otherwise just update processed date
        db.updateProcessed(Arrays.asList(id));
      }

      // if not save, do not trigger reprocess because of different dependencies
      boolean newDependencies =
          newProcessDocument.hasDifferentDependencies(oldDocument)
              && source.isSave()
              && !newResultDocument.isError();

      if (newResultDocument.isProcessed() && !newDependencies) {
        db.deleteFromQueue(Arrays.asList(id));
      } else {
        registry.getQueue(sourceName).submit(id, newDependencies);
      }
    }
  }

  private boolean equals(BoxDocument newDoc, BoxDocument dbDoc) {
    return Objects.equals(newDoc.getId(), dbDoc.getId())
        && Objects.equals(newDoc.getStatus(), dbDoc.getStatus())
        && Objects.equals(newDoc.getDocument(), dbDoc.getDocument())
        && Objects.equals(newDoc.getMessage(), dbDoc.getMessage())
        && Objects.equals(newDoc.getFacets(), dbDoc.getFacets())
        && Objects.equals(newDoc.getDependencies(), dbDoc.getDependencies())
        && Objects.equals(newDoc.getGroupId(), dbDoc.getGroupId())
        && (newDoc.getModified() == null
            || Objects.equals(newDoc.getModified(), dbDoc.getModified()))
        && (newDoc.getProcessed() == null
            || Objects.equals(newDoc.getProcessed(), dbDoc.getProcessed()))
        && (newDoc.getCursor() == 0 || Objects.equals(newDoc.getCursor(), dbDoc.getCursor()));
  }

  /**
   * @param sourceName
   * @throws Exception
   */
  boolean harvest(String sourceName) throws Exception {

    // skip if not primary
    if (!registry.isPrimary(sourceName)) {
      return false;
    }

    Source source = registry.getSource(sourceName);

    BoxDatabase cursorDb = source.getPreferredCursorDb();

    ObjectNode cursor = cursorDb.getHarvestCursor();

    logger.debug("harvesting " + source.getName() + " with cursor " + cursor);

    // IMPORTANT: the cursor is allowed to be updated by the harvest processor so we send a copy
    HarvestContext context = new HarvestContext(cursor.deepCopy());
    HarvestResult harvestResult = source.getHarvester().harvest(context);

    if (harvestResult.getCursor() == null) {
      harvestResult.setCursor(cursor);
    }

    if (!harvestResult.isEmpty()) {
      List<String> ids = harvestResult.stream().map(d -> d.getId()).collect(Collectors.toList());

      Map<String, BoxDocument> oldDocumentMap =
          source
              .getDb()
              .find(new BoxQuery().addIds(ids))
              .stream()
              .collect(Collectors.toMap(BoxDocument::getId, Function.identity(), (x, y) -> y));

      for (BoxDocument resultDocument : harvestResult) {
        BoxDocument oldDocument = oldDocumentMap.get(resultDocument.getId());

        // do not save deleted documents if they do not already exist in the database
        // unless directed by the harvest-only directive saveDeleted
        if (source.isSaveDeleted()
            || !resultDocument.isDeleted()
            || resultDocument.isDeleted() && oldDocument.isProcessed()) {
          save(sourceName, resultDocument, oldDocument);
        }
      }
    }

    // if cursor not updated manually during the harvest
    ObjectNode newCursor = harvestResult.getCursor();
    newCursor = newCursor == null ? JsonNodeFactory.instance.objectNode() : newCursor;

    cursorDb.setHarvestCursor(newCursor);

    return harvestResult.hasMore();
  }
}
