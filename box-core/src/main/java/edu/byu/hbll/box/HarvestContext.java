/** */
package edu.byu.hbll.box;

import java.util.Objects;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Information for the current harvest.
 *
 * @author Charles Draper
 */
public class HarvestContext {

  private ObjectNode cursor = JsonNodeFactory.instance.objectNode();

  /** */
  public HarvestContext() {}

  /** @param cursor the cursor of where the harvest last left off */
  public HarvestContext(ObjectNode cursor) {
    this.cursor = Objects.requireNonNull(cursor);
  }

  /** @return the saved cursor of where the harvest last left off */
  public ObjectNode getCursor() {
    return cursor;
  }
}
