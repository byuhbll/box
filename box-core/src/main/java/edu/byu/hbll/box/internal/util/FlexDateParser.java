/** */
package edu.byu.hbll.box.internal.util;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAccessor;

/** */
public class FlexDateParser {

  public static final DateTimeFormatter FORMATTER =
      new DateTimeFormatterBuilder()
          .parseCaseInsensitive()
          .append(DateTimeFormatter.ISO_LOCAL_DATE)
          .optionalStart()
          .appendLiteral('T')
          .append(DateTimeFormatter.ISO_TIME)
          .toFormatter();

  public static Instant parse(String text) throws DateTimeParseException {
    return parse(text, ZoneId.systemDefault());
  }

  public static Instant parse(String text, ZoneId defaultZoneId) throws DateTimeParseException {
    if (text == null) {
      return null;
    }

    ZonedDateTime zonedDateTime = null;
    TemporalAccessor accessor =
        FORMATTER.parseBest(text, ZonedDateTime::from, LocalDateTime::from, LocalDate::from);
    if (accessor instanceof ZonedDateTime) {
      zonedDateTime = (ZonedDateTime) accessor;
    } else if (accessor instanceof LocalDateTime) {
      zonedDateTime = ((LocalDateTime) accessor).atZone(defaultZoneId);
    } else {
      zonedDateTime = ((LocalDate) accessor).atStartOfDay(defaultZoneId);
    }

    return zonedDateTime.toInstant();
  }
}
