/** */
package edu.byu.hbll.box.impl;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxDatabase;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.DocumentId;
import edu.byu.hbll.box.QueryResult;
import edu.byu.hbll.box.internal.util.CursorUtils;

/** A box database that holds everything in memory. */
public class MemoryDatabase implements BoxDatabase {

  SortedMap<Entry, Entry> queue = new TreeMap<>();

  Map<String, BoxDocument> documentMap = new ConcurrentHashMap<>();

  SortedMap<Entry, Entry> harvestMap = new TreeMap<>();

  ObjectNode cursorMap = JsonNodeFactory.instance.objectNode();

  Map<String, Set<DocumentId>> dependentMap = new ConcurrentHashMap<>();

  Map<String, ObjectNode> historyMap = new ConcurrentHashMap<>();

  @Override
  public int addToQueue(Duration olderThan) {
    Instant old = Instant.now().minus(olderThan);
    int count = 0;

    for (BoxDocument document : documentMap.values()) {
      if (document.getProcessed().isBefore(old)) {
        addToQueue(Arrays.asList(document.getId()), false);
        count++;
      }
    }

    return count;
  }

  @Override
  public void addToQueue(Collection<String> ids, boolean resetAttempt) {
    for (String id : ids) {
      Entry entry = new Entry(id);

      synchronized (queue) {
        if (resetAttempt || !queue.containsKey(entry)) {
          queue.put(entry, entry);
        }
      }
    }
  }

  @Override
  public void deleteFromQueue(Collection<String> ids) {
    for (String id : ids) {
      Entry entry = new Entry(id);

      synchronized (queue) {
        queue.remove(entry);
      }
    }
  }

  @Override
  public Map<String, Set<DocumentId>> findDependencies(Collection<String> ids) {
    Map<String, Set<DocumentId>> dependencyMap = new HashMap<>();

    for (String id : ids) {
      Set<DocumentId> dependencies = new HashSet<>();
      dependencyMap.put(id, dependencies);

      BoxDocument document = documentMap.get(id);

      if (document != null) {
        document.getDependencies().addAll(document.getDependencies());
      }
    }

    return dependencyMap;
  }

  @Override
  public Map<DocumentId, Set<String>> findDependents(Collection<DocumentId> dependencies) {
    return new HashMap<>();
  }

  private BoxDocument findDocument(String id) {
    return new BoxDocument(documentMap.getOrDefault(id, new BoxDocument(id)));
  }

  @Override
  public QueryResult find(BoxQuery query) {
    if (!query.getIds().isEmpty()) {
      QueryResult result = new QueryResult();

      for (String id : query.getIds()) {
        result.add(findDocument(id));
      }

      result.updateNextCursor(query);
      return result;
    } else {
      List<BoxDocument> fullDocuments = new ArrayList<>();

      Entry entryCursor = new Entry("0", query.getCursor());

      synchronized (harvestMap) {
        int i = 0;

        for (Entry entry : harvestMap.tailMap(entryCursor).keySet()) {
          BoxDocument document = new BoxDocument(documentMap.get(entry.id));
          document.setCursor(entry.cursor);
          fullDocuments.add(document);

          if (++i >= query.getLimit()) {
            break;
          }
        }
      }
    }

    return new QueryResult();
  }

  @Override
  public ObjectNode getHarvestCursor() {
    return cursorMap;
  }

  @Override
  public Set<String> listSourceDependencies() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<String> nextFromQueue(int limit) {
    List<String> ids = new ArrayList<>();

    synchronized (queue) {
      for (int i = 0; i < limit && !queue.isEmpty(); i++) {
        Entry entry = queue.firstKey();

        if (entry.cursor < CursorUtils.nextCursor()) {
          ids.add(entry.id);
          queue.remove(entry);
        } else {
          break;
        }
      }
    }

    return ids;
  }

  @Override
  public void processOrphans(String groupId, Consumer<BoxDocument> function) {
    // TODO Auto-generated method stub

  }

  @Override
  public void removeDeleted(Duration olderThan) {
    Instant old = Instant.now().minus(olderThan);

    for (BoxDocument document : new ArrayList<>(documentMap.values())) {
      if (document.getModified().isBefore(old)) {
        documentMap.remove(document.getId());
      }
    }
  }

  @Override
  public void save(Collection<? extends BoxDocument> documents) {

    for (BoxDocument resultDocument : documents) {
      BoxDocument processDocument = new BoxDocument(resultDocument);

      Entry entry = new Entry(resultDocument.getId());

      synchronized (harvestMap) {
        harvestMap.put(entry, entry);
        documentMap.put(resultDocument.getId(), new BoxDocument(processDocument));
      }
    }
  }

  @Override
  public void setHarvestCursor(ObjectNode cursor) {
    cursorMap = cursor.deepCopy();
  }

  @Override
  public void startGroup(String groupId) {
    // TODO Auto-generated method stub

  }

  @Override
  public void updateProcessed(Collection<String> ids) {
    for (String id : ids) {
      if (documentMap.containsKey(id)) {
        BoxDocument document = documentMap.get(id);

        if (document != null) {
          document.setProcessed(Instant.now());
        }
      }
    }
  }

  private static class Entry implements Comparable<Entry> {

    String id;
    long cursor = CursorUtils.nextCursor();

    Entry(String id) {
      this.id = id;
    }

    Entry(String id, long cursor) {
      this.cursor = cursor;
    }

    @Override
    public int compareTo(Entry o) {
      if (equals(o)) {
        return 0;
      } else {
        int comparison = Long.compare(cursor, o.cursor);

        if (comparison != 0) {
          return comparison;
        } else {
          return id.compareTo(o.id);
        }
      }
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) return true;
      if (obj == null) return false;
      if (getClass() != obj.getClass()) return false;
      Entry other = (Entry) obj;
      if (id == null) {
        if (other.id != null) return false;
      } else if (!id.equals(other.id)) return false;
      return true;
    }

    @Override
    public int hashCode() {
      return id.hashCode();
    }
  }

  @Override
  public JsonNode findRegistryValue(String id) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void saveRegistryValue(String id, JsonNode data) {
    // TODO Auto-generated method stub

  }

  @Override
  public void updateDependencies(Collection<? extends BoxDocument> documents) {
    // TODO Auto-generated method stub

  }
}
