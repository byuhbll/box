/** */
package edu.byu.hbll.box.impl;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.UriBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxDocument.Status;
import edu.byu.hbll.box.ProcessContext;
import edu.byu.hbll.box.ProcessResult;
import edu.byu.hbll.box.SingleProcessor;
import edu.byu.hbll.xml.XmlJsonConverter;
import edu.byu.hbll.xml.XmlUtils;

/**
 * A processor that implements the OAI protocol.
 *
 * @author Charles Draper
 */
public class OaiProcessor implements SingleProcessor {

  static final Logger logger = LoggerFactory.getLogger(OaiProcessor.class);

  /** A default metadata prefix */
  public static final String DEFAULT_METADATA_PREFIX = "oai_dc";

  /** */
  private Client client = ClientBuilder.newClient();

  /** */
  private String uri;

  /** */
  private String metadataPrefix = DEFAULT_METADATA_PREFIX;

  /** */
  private String idRegex;

  /** */
  private String idReplacement;

  /** A white list of setSpecs to harvest */
  private Set<String> sets = new HashSet<>();

  private boolean removeNamespaces;

  /** */
  protected OaiProcessor() {}

  /** */
  public OaiProcessor(String uri) {
    this.uri = Objects.requireNonNull(uri);
  }

  @Override
  public ProcessResult process(ProcessContext context) {

    String id = context.getId();

    Document record = getRecord(id);

    List<Element> setNodes = XmlUtils.getElements(record, "record", "header", "setSpec");
    List<String> sets = new ArrayList<>();

    for (Element setNode : setNodes) {
      sets.add(setNode.getTextContent());
    }

    BoxDocument boxDocument;

    if (!this.sets.isEmpty() && Collections.disjoint(this.sets, sets)) {
      // if set is not part of the white list of sets then ignore it
      boxDocument = new BoxDocument(id, Status.DELETED);
    } else if ("deleted"
        .equals(XmlUtils.getElement(record, "record", "header").getAttribute("status"))) {
      // status is an optional attribute in the header that can state that the record is deleted
      boxDocument = new BoxDocument(id, Status.DELETED);
    } else {
      Document recordToConvert = record;

      if (removeNamespaces) {
        recordToConvert = XmlUtils.removeNamespaces(recordToConvert);
      }

      ObjectNode document = XmlJsonConverter.toJsonDoc(recordToConvert);
      boxDocument = new BoxDocument(id, document);
      sets.forEach(s -> boxDocument.addFacet("set", s));
    }

    return new ProcessResult(transform(record, boxDocument));
  }

  /**
   * Queries the OAI responder for the record of the given id.
   *
   * @param id the id of the record (prior to regex/replacement)
   * @return the retrieved xml document
   */
  public Document getRecord(String id) {
    String identifier = idRegex != null ? id.replaceAll(idRegex, idReplacement) : id;

    URI uri =
        UriBuilder.fromUri(this.uri)
            .queryParam("verb", "GetRecord")
            .queryParam("identifier", identifier)
            .queryParam("metadataPrefix", metadataPrefix)
            .build();

    Document response = client.target(uri).request().get(Document.class);

    Document record =
        XmlUtils.toDocument(XmlUtils.getElement(response, "OAI-PMH", "GetRecord", "record"));

    return record;
  }

  /** @return */
  protected BoxDocument transform(Document record, BoxDocument boxDocument) {
    return boxDocument;
  }

  /** @return the client */
  public Client getClient() {
    return client;
  }

  /** @param client the client to set */
  public void setClient(Client client) {
    this.client = Objects.requireNonNull(client);
  }

  /** @return the uri */
  public String getUri() {
    return uri;
  }

  /** @param uri the uri to set */
  public void setUri(String uri) {
    this.uri = Objects.requireNonNull(uri);
  }

  /** @return the metadataPrefix */
  public String getMetadataPrefix() {
    return metadataPrefix;
  }

  /** @param metadataPrefix the metadataPrefix to set */
  public void setMetadataPrefix(String metadataPrefix) {
    this.metadataPrefix = Objects.requireNonNull(metadataPrefix);
  }

  /** @return the idRegex */
  public String getIdRegex() {
    return idRegex;
  }

  /** @param idRegex the idRegex to set */
  public void setIdRegex(String idRegex) {
    this.idRegex = idRegex;
  }

  /** @return the idReplacement */
  public String getIdReplacement() {
    return idReplacement;
  }

  /** @param idReplacement the idReplacement to set */
  public void setIdReplacement(String idReplacement) {
    this.idReplacement = idReplacement;
  }

  /** @return the sets */
  public Set<String> getSets() {
    return sets;
  }

  /** @param sets the sets to set */
  public void setSets(Set<String> sets) {
    this.sets = Objects.requireNonNull(sets);
  }

  /** @return the removeNamespaces */
  public boolean isRemoveNamespaces() {
    return removeNamespaces;
  }

  /** @param removeNamespaces the removeNamespaces to set */
  public void setRemoveNamespaces(boolean removeNamespaces) {
    this.removeNamespaces = removeNamespaces;
  }
}
