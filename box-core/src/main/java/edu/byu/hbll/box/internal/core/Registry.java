/** */
package edu.byu.hbll.box.internal.core;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import javax.annotation.PreDestroy;
import javax.ejb.ScheduleExpression;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import edu.byu.hbll.box.Box;
import edu.byu.hbll.box.BoxConfigurable;
import edu.byu.hbll.box.BoxConfiguration;
import edu.byu.hbll.box.Source;
import edu.byu.hbll.box.client.BoxUpdatesClient;

/** */
@ApplicationScoped
public class Registry {

  static final Logger logger = LoggerFactory.getLogger(Registry.class);

  public static final String PRINCIPAL_SOURCE_ALIAS = "box";

  private volatile boolean configured = false;

  private Source principalSource;

  private Map<String, Source> sources = new LinkedHashMap<>();

  private DocumentHandler documentHandler;

  // @Resource(name = "concurrent/__defaultManagedThreadFactory")
  private ThreadFactory threadFactory;

  private Map<String, QueueRunner> queues = new HashMap<>();

  private List<Object> closeables = new ArrayList<>();

  private Map<String, Integer> harvestScheduleIds = new HashMap<>();

  private Map<String, DocumentProcessor> documentProcessors = new HashMap<>();

  private Map<String, PrimaryMonitor> primaryMonitors = new HashMap<>();

  @Inject private UpdatesNotifier updatesNotifier;

  @Inject private Scheduler scheduler;

  private Map<String, Set<String>> dependents = new ConcurrentHashMap<>();

  /** @param config */
  public void set(Box box, BoxConfiguration config) {
    if (configured) {
      return;
    }

    // set up thread factory
    // TODO injected thread factory doesn't work, using default for now
    threadFactory = Executors.defaultThreadFactory();
    // if(config.getThreadFactory() != null) {
    // threadFactory = config.getThreadFactory();
    // }

    // set up sources
    for (Source source : config.getSources()) {

      if (!source.isEnabled()) {
        continue;
      }

      if (source.isPrincipal() && principalSource == null) {
        principalSource = source;
      }

      sources.put(source.getName(), source);

      OnOffSemaphore onOffSemaphore = new OnOffSemaphore();

      if (source.getOn() != null && source.getOff() != null) {
        scheduler.schedule(() -> onOffSemaphore.setOn(true), source.getOn());
        scheduler.schedule(() -> onOffSemaphore.setOn(false), source.getOff());
        Instant nextOn = scheduler.getNextTimeout(source.getOn());
        Instant nextOff = scheduler.getNextTimeout(source.getOff());
        onOffSemaphore.init(nextOn, nextOff);
      }

      QuotaSemaphore quotaSemaphore = new QuotaSemaphore(source.getQuota());
      scheduler.schedule(quotaSemaphore, source.getQuotaReset());
      QueueRunner queueRunner = new QueueRunner(this, source, onOffSemaphore);
      queues.put(source.getName(), queueRunner);
      closeables.add(queueRunner);

      if (source.isProcessEnabled()) {
        DocumentProcessor documentProcessor = new DocumentProcessor(this, source, quotaSemaphore);
        closeables.add(documentProcessor);
        documentProcessors.put(source.getName(), documentProcessor);
      }

      // set up empty dependents per source
      dependents.computeIfAbsent(
          source.getName(), k -> Collections.newSetFromMap(new ConcurrentHashMap<>()));

      closeables.add(source.getProcessor());
      closeables.add(source.getHarvester());
      closeables.add(source.getDb());
      closeables.add(source.getOthers());
    }

    if (principalSource == null && !config.getSources().isEmpty()) {
      principalSource = config.getSources().get(0);
    }

    // set up document handler
    documentHandler = new DocumentHandler(this);

    // init source schedules
    for (Source source : sources.values()) {
      if (source.getHarvester() != null && source.isHarvestEnabled()) {
        int id =
            scheduler.schedule(
                () -> documentHandler.harvest(source.getName()), source.getHarvestSchedule());
        harvestScheduleIds.put(source.getName(), id);
      }

      if (source.getReprocessAge() != null) {
        scheduler.schedule(
            () -> {
              source.getDb().addToQueue(source.getReprocessAge());
              return false;
            },
            source.getReprocessSchedule());
      }

      if (source.getRemoveAge() != null) {
        scheduler.schedule(
            () -> {
              source.getDb().removeDeleted(source.getRemoveAge());
              return false;
            },
            source.getRemoveSchedule());
      }

      if (source.getHarvestResetSchedule() != null) {
        scheduler.schedule(
            () -> {
              source.getPreferredCursorDb().setHarvestCursor(JsonNodeFactory.instance.objectNode());
              return false;
            },
            source.getHarvestResetSchedule());
      }

      if (source.getHarvester() != null) {
        initBoxSourceConnection(source);
      }

      PrimaryMonitor primaryMonitor = new PrimaryMonitor(source.getPreferredCursorDb());
      this.primaryMonitors.put(source.getName(), primaryMonitor);
      closeables.add(primaryMonitor);

      scheduler.schedule(
          primaryMonitor, new ScheduleExpression().second("*/2").minute("*").hour("*"));

      // init dependents
      for (String dependency : source.getDb().listSourceDependencies()) {
        if (dependents.containsKey(dependency)) {
          dependents.get(dependency).add(source.getName());
        }
      }
    }

    configured = true;

    // allow scheduled tasks to execute at this point
    scheduler.enable();
  }

  private void initBoxSourceConnection(Source source) {

    String baseUri = null;

    try {

      for (Field field : source.getHarvester().getClass().getFields()) {
        if (field.isAnnotationPresent(BoxSourceBaseUri.class)
            && field.getType().isAssignableFrom(String.class)) {
          field.setAccessible(true);
          baseUri = (String) field.get(source.getHarvester());
        }
      }

      for (Method method : source.getHarvester().getClass().getMethods()) {
        if (method.isAnnotationPresent(BoxSourceBaseUri.class)
            && method.getReturnType().isAssignableFrom(String.class)) {
          method.setAccessible(true);
          baseUri = (String) method.invoke(source.getHarvester());
        }
      }

      if (baseUri != null && !baseUri.trim().isEmpty()) {
        closeables.add(new BoxUpdatesClient(baseUri, () -> triggerHarvest(source.getName())));
      }

    } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
      logger.error("unable to access @BoxSourceBaseUri");
    } catch (Exception e) {
      logger.error("unable to connect to " + baseUri + " to receive real-time updates " + e);
    }
  }

  /** @return the documentHandler */
  public DocumentHandler getDocumentHandler() {
    return documentHandler;
  }

  /** @return the threadFactory */
  public ThreadFactory getThreadFactory() {
    return threadFactory;
  }

  /**
   * @param name
   * @return
   */
  public Source getSource(String name) {
    return sources.get(verifySource(name));
  }

  /** @return */
  public Source getPrincipalSource() {
    return principalSource;
  }

  public boolean isPrimary(String sourceName) {
    return primaryMonitors.get(verifySource(sourceName)).isPrimary();
  }

  /**
   * @param name
   * @return
   */
  public QueueRunner getQueue(String name) {
    return queues.get(name);
  }

  /** @return */
  public List<Source> getSources() {
    return new ArrayList<>(sources.values());
  }

  /**
   * @param name
   * @return
   */
  public boolean isSource(String name) {
    return sources.containsKey(name);
  }

  /** @param sourceName */
  public void triggerHarvest(String sourceName) {
    if (harvestScheduleIds.containsKey(sourceName)) {
      scheduler.trigger(harvestScheduleIds.get(sourceName));
    }
  }

  public UpdatesNotifier getUpdatesNotifier() {
    return updatesNotifier;
  }

  public boolean isConfigured() {
    return configured;
  }

  /**
   * @param sourceName
   * @return
   */
  public DocumentProcessor getDocumentProcessor(String sourceName) {
    return documentProcessors.get(sourceName);
  }

  /** @param sourceName */
  public String verifySource(String sourceName) {
    sourceName =
        sourceName == null
                || sourceName.trim().isEmpty()
                || sourceName.equals(PRINCIPAL_SOURCE_ALIAS)
            ? getPrincipalSource().getName()
            : sourceName;

    if (!isSource(sourceName)) {
      throw new IllegalArgumentException("source " + sourceName + " not recognized");
    }

    return sourceName;
  }

  public Set<String> getDependents(String sourceName) {
    return dependents.get(sourceName);
  }

  public void addDependency(String dependent, String dependency) {
    dependents.get(dependency).add(dependent);
  }

  /** */
  @PreDestroy
  public void preDestroy() {
    for (Object closeable : closeables) {
      if (closeable != null) {
        try {
          if (closeable instanceof BoxConfigurable) {
            ((BoxConfigurable) closeable).preDestroy();
          } else if (closeable instanceof AutoCloseable) {
            ((AutoCloseable) closeable).close();
          }
        } catch (Exception e) {
          logger.error(e.toString(), e);
        }
      }
    }
  }
}
