/** */
package edu.byu.hbll.box.impl;

import javax.enterprise.inject.Instance;
import javax.enterprise.inject.spi.CDI;
import edu.byu.hbll.box.ProcessBatch;
import edu.byu.hbll.box.ProcessResult;
import edu.byu.hbll.box.Processor;

/**
 * A processor that injects another processor using CDI and uses the injected harvester as the main
 * processor.
 *
 * @author Charles Draper
 */
public class CdiProcessor implements Processor {

  /** */
  private Class<? extends Processor> injectable;

  /** @param injectable */
  public CdiProcessor(Class<? extends Processor> injectable) {
    this.injectable = injectable;
  }

  /**
   * @param batch
   * @return
   */
  @SuppressWarnings({"rawtypes", "unchecked"})
  @Override
  public ProcessResult process(ProcessBatch batch) {
    Instance instance = CDI.current().select(injectable);
    Processor processor = (Processor) instance.get();

    try {
      return processor.process(batch);
    } finally {
      instance.destroy(processor);
    }
  }
}
