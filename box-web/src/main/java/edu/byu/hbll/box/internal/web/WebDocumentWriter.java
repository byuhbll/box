/** */
package edu.byu.hbll.box.internal.web;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.json.ObjectMapperFactory;

/**
 * Allows for serialization of {@link WebDocument}s in JAX-RS.
 *
 * @author Charles Draper
 */
@Provider
@Produces(MediaType.APPLICATION_JSON)
public class WebDocumentWriter implements MessageBodyWriter<WebDocument> {

  ObjectMapper mapper = ObjectMapperFactory.newUnchecked();

  @Override
  public long getSize(
      WebDocument t,
      Class<?> type,
      Type genericType,
      Annotation[] annotations,
      MediaType mediaType) {
    return 0;
  }

  @Override
  public boolean isWriteable(
      Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
    return WebDocument.class.isAssignableFrom(type);
  }

  @Override
  public void writeTo(
      WebDocument t,
      Class<?> type,
      Type genericType,
      Annotation[] annotations,
      MediaType mediaType,
      MultivaluedMap<String, Object> httpHeaders,
      OutputStream entityStream)
      throws IOException, WebApplicationException {

    mapper.writeValue(entityStream, convert(t));
  }

  /** @return */
  public ObjectNode convert(WebDocument webDocument) {
    return webDocument.getDocument().toJson(webDocument.getMetadataLevel());
  }
}
