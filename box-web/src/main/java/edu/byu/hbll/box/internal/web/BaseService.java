/** */
package edu.byu.hbll.box.internal.web;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.byu.hbll.box.internal.core.Registry;

/** @author Charles Draper */
public class BaseService {

  /** */
  static final Logger logger = LoggerFactory.getLogger(BaseService.class);

  /** */
  private static final ObjectMapper mapper = new ObjectMapper();

  /** */
  @Inject private Registry registry;

  /** @param sourceName */
  protected String resolveSourceName(String sourceName) {
    try {
      return registry.verifySource(sourceName);
    } catch (Exception e) {
      return null;
    }
  }

  protected String errorMessage(String message) {
    return mapper.createObjectNode().put("error", message).toString();
  }

  protected Response uncrecognizedSource() {
    return Response.status(Status.BAD_REQUEST)
        .entity(errorMessage("source not recognized"))
        .build();
  }
}
