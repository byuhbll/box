/** */
package edu.byu.hbll.box.internal.web;

import java.util.ArrayList;
import java.util.List;

/** */
public class DocumentsResponse {

  /** */
  private List<WebDocument> documents = new ArrayList<>();

  /** @return the documents */
  public List<WebDocument> getDocuments() {
    return documents;
  }

  /** @param documents the documents to set */
  public void setDocuments(List<WebDocument> documents) {
    this.documents = documents;
  }
}
