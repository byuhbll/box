/** */
package edu.byu.hbll.box.internal.web;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.Facet;
import edu.byu.hbll.box.MetadataLevel;

/** */
public class DocumentsParams {

  @PathParam("sourceName")
  private String sourceName;

  @QueryParam("id")
  private List<String> ids = new ArrayList<>();

  @QueryParam("wait")
  private String wait;

  @QueryParam("process")
  private String process;

  @QueryParam("from")
  private String fromText;

  @QueryParam("cursor")
  @DefaultValue("0")
  private long cursor;

  @QueryParam("limit")
  @DefaultValue("-1")
  private int limit;

  private List<BoxDocument.Status> statuses = new ArrayList<>();

  @QueryParam("field")
  private List<String> fields = new ArrayList<>();

  @QueryParam("facet")
  private List<String> facets = new ArrayList<>();

  private MetadataLevel metadataLevel = MetadataLevel.CORE;

  @QueryParam("metadataOnly")
  private String metadataOnly;

  @Context private UriInfo uriInfo;

  /** */
  public DocumentsParams() {}

  /** @param params */
  public DocumentsParams(DocumentParams params) {
    this.sourceName = params.getSourceName();
    this.ids.add(params.getId());
    setWait(params.isWait() || params.isProcess());
    setProcess(params.isProcess());
    this.fields = params.getFields();
    this.uriInfo = params.getUriInfo();
    this.metadataLevel = params.getMetadataLevel();
    setMetadataOnly(params.isMetadataOnly());
  }

  /** @return the sourceName */
  public String getSourceName() {
    return sourceName;
  }

  /** @param sourceName the sourceName to set */
  public void setSourceName(String sourceName) {
    this.sourceName = sourceName;
  }

  /** @return the ids */
  public List<String> getIds() {
    return ids;
  }

  /** @param ids the ids to set */
  public void setIds(List<String> ids) {
    this.ids = ids;
  }

  /** @return the wait */
  public boolean isWait() {
    return DocumentService.booleanValueOf(wait);
  }

  /** @param wait the wait to set */
  public void setWait(boolean wait) {
    this.wait = Boolean.toString(wait);
  }

  /** @return the process */
  public boolean isProcess() {
    return DocumentService.booleanValueOf(process);
  }

  /** @param process the process to set */
  public void setProcess(boolean process) {
    this.process = Boolean.toString(process);
  }

  /** @return the fromText */
  public String getFromText() {
    return fromText;
  }

  /** @param fromText the fromText to set */
  public void setFromText(String fromText) {
    this.fromText = fromText;
  }

  /** @return the cursor */
  public long getCursor() {
    return cursor;
  }

  /** @param cursor the cursor to set */
  public void setCursor(long cursor) {
    this.cursor = cursor;
  }

  /** @return the limit */
  public int getLimit() {
    return limit;
  }

  /** @param limit the limit to set */
  public void setLimit(int limit) {
    this.limit = limit;
  }

  /** @return the fields */
  public List<String> getFields() {
    return fields;
  }

  /** @param fields the fields to set */
  public void setFields(List<String> fields) {
    this.fields = fields;
  }

  /** @return the facets */
  public List<String> getFacets() {
    return facets;
  }

  /** @param facets the facets to set */
  public void setFacets(List<String> facets) {
    this.facets = facets;
  }

  /** @return the metadataLevel */
  public MetadataLevel getMetadataLevel() {
    return metadataLevel;
  }

  /** @param metadataLevel the metadataLevel to set */
  public void setMetadataLevel(MetadataLevel metadataLevel) {
    this.metadataLevel = metadataLevel;
  }

  /** @param metadataLevel the metadataLevel to set */
  @QueryParam("metadataLevel")
  public void setMetadataLevel(String metadataLevel) {
    if (metadataLevel == null || metadataLevel.isEmpty()) {
      return;
    }

    this.metadataLevel = MetadataLevel.valueOf(metadataLevel.toUpperCase());
  }

  /** @return the metadataOnly */
  public boolean isMetadataOnly() {
    return DocumentService.booleanValueOf(metadataOnly);
  }

  /** @param metadataOnly the metadataOnly to set */
  public void setMetadataOnly(boolean metadataOnly) {
    this.metadataOnly = Boolean.toString(metadataOnly);
  }

  /** @return the statuses */
  public List<BoxDocument.Status> getStatuses() {
    return statuses;
  }

  /** @param statuses the statuses to set */
  @QueryParam("status")
  public void setStatuses(List<String> statuses) {
    this.statuses = new ArrayList<>();

    if (statuses.isEmpty()) {
      this.statuses.add(BoxDocument.Status.READY);
      this.statuses.add(BoxDocument.Status.DELETED);
    } else {
      for (String status : statuses) {
        this.statuses.add(BoxDocument.Status.valueOf(status.toUpperCase()));
      }
    }
  }

  /** @return the uriInfo */
  public UriInfo getUriInfo() {
    return uriInfo;
  }

  /** @param uriInfo the uriInfo to set */
  public void setUriInfo(UriInfo uriInfo) {
    this.uriInfo = uriInfo;
  }

  /** @return */
  public BoxQuery toQuery() {
    BoxQuery query =
        new BoxQuery()
            .setIds(ids)
            .setProcess(isProcess())
            .setWait(isWait())
            .setCursor(cursor)
            .setLimit(limit)
            .setStatuses(statuses)
            .setFields(fields)
            .setFacets(Facet.parse(facets))
            .setMetadataLevel(metadataLevel)
            .setMetadataOnly(isMetadataOnly());

    return query;
  }
}
