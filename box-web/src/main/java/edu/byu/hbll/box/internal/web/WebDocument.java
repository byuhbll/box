/** */
package edu.byu.hbll.box.internal.web;

import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.MetadataLevel;

/** @author Charles Draper */
public class WebDocument {

  /** */
  private BoxDocument document;

  /** */
  private MetadataLevel metadataLevel;

  /** */
  public WebDocument() {}

  /** @param document */
  public WebDocument(BoxDocument document) {
    this(document, MetadataLevel.NONE);
  }

  /**
   * @param document
   * @param metadataLevel
   */
  public WebDocument(BoxDocument document, MetadataLevel metadataLevel) {
    this.document = document;
    this.metadataLevel = metadataLevel;
  }

  /** @return the document */
  public BoxDocument getDocument() {
    return document;
  }

  /** @param document the document to set */
  public void setDocument(BoxDocument document) {
    this.document = document;
  }

  /** @return the metadataLevel */
  public MetadataLevel getMetadataLevel() {
    return metadataLevel;
  }

  /** @param metadataLevel the metadataLevel to set */
  public void setMetadataLevel(MetadataLevel metadataLevel) {
    this.metadataLevel = metadataLevel;
  }
}
