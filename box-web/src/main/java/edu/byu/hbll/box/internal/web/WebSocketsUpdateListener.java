/** */
package edu.byu.hbll.box.internal.web;

import java.util.Objects;
import javax.websocket.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import edu.byu.hbll.box.internal.core.UpdatesNotifier;

/** @author Charles Draper */
public class WebSocketsUpdateListener implements Runnable {

  static final Logger logger = LoggerFactory.getLogger(WebSocketsUpdateListener.class);

  private String sourceName;
  private Session session;
  private UpdatesNotifier updatesNotifier;

  /**
   * @param updatesNotifier
   * @param sourceName
   * @param session
   */
  public WebSocketsUpdateListener(
      UpdatesNotifier updatesNotifier, String sourceName, Session session) {
    this.updatesNotifier = updatesNotifier;
    this.sourceName = sourceName;
    this.session = session;
  }

  @Override
  public void run() {
    try {
      session.getBasicRemote().sendText(sourceName);
    } catch (Exception e) {
      logger.warn("canceling updates notifier {}", e);
      updatesNotifier.unregister(sourceName, this);
    }
  }

  @Override
  public int hashCode() {
    return Objects.hash(session, sourceName);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (!(obj instanceof WebSocketsUpdateListener)) {
      return false;
    }

    WebSocketsUpdateListener other = (WebSocketsUpdateListener) obj;

    return Objects.equals(sourceName, other.sourceName) && Objects.equals(session, other.session);
  }
}
