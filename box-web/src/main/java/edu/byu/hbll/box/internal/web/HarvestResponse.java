/** */
package edu.byu.hbll.box.internal.web;

/** */
public class HarvestResponse extends DocumentsResponse {

  /** */
  private String nextUri;

  /** */
  private String nextCursor;

  public String getNextUri() {
    return nextUri;
  }

  public void setNextUri(String nextUri) {
    this.nextUri = nextUri;
  }

  public String getNextCursor() {
    return nextCursor;
  }

  public void setNextCursor(String nextCursor) {
    this.nextCursor = nextCursor;
  }
}
