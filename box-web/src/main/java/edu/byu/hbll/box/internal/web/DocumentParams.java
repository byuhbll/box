/** */
package edu.byu.hbll.box.internal.web;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import edu.byu.hbll.box.MetadataLevel;

/** */
public class DocumentParams {

  /** */
  @PathParam("sourceName")
  private String sourceName;

  /** */
  @PathParam("id")
  private String id;

  /** */
  @QueryParam("wait")
  private String wait;

  /** */
  @QueryParam("process")
  private String process;

  /** */
  @QueryParam("field")
  private List<String> fields = new ArrayList<>();

  /** */
  private MetadataLevel metadataLevel = MetadataLevel.CORE;

  /** */
  @QueryParam("metadataOnly")
  private String metadataOnly;

  /** */
  @Context private UriInfo uriInfo;

  /** @return the sourceName */
  public String getSourceName() {
    return sourceName;
  }

  /** @param sourceName the sourceName to set */
  public void setSourceName(String sourceName) {
    this.sourceName = sourceName;
  }

  /** @return the id */
  public String getId() {
    return id;
  }

  /** @param id the id to set */
  public void setId(String id) {
    this.id = id;
  }

  /** @return the wait */
  public boolean isWait() {
    return DocumentService.booleanValueOf(wait);
  }

  /** @param wait the wait to set */
  public void setWait(boolean wait) {
    this.wait = Boolean.toString(wait);
  }

  /** @return the process */
  public boolean isProcess() {
    return DocumentService.booleanValueOf(process);
  }

  /** @param process the process to set */
  public void setProcess(boolean process) {
    this.process = Boolean.toString(process);
  }

  /** @return the fields */
  public List<String> getFields() {
    return fields;
  }

  /** @param fields the fields to set */
  public void setFields(List<String> fields) {
    this.fields = fields;
  }

  /** @return the metadataLevel */
  public MetadataLevel getMetadataLevel() {
    return metadataLevel;
  }

  /** @param metadataLevel the metadataLevel to set */
  @QueryParam("metadataLevel")
  public void setMetadataLevel(String metadataLevel) {
    if (metadataLevel == null || metadataLevel.isEmpty()) {
      return;
    }

    this.metadataLevel = MetadataLevel.valueOf(metadataLevel.toUpperCase());
  }

  /** @param metadataLevel the metadataLevel to set */
  public void setMetadataLevel(MetadataLevel metadataLevel) {
    this.metadataLevel = metadataLevel;
  }

  /** @return the metadataOnly */
  public boolean isMetadataOnly() {
    return DocumentService.booleanValueOf(metadataOnly);
  }

  /** @param metadataOnly the metadataOnly to set */
  public void setMetadataOnly(boolean metadataOnly) {
    this.metadataOnly = Boolean.toString(metadataOnly);
  }

  /** @return the uriInfo */
  public UriInfo getUriInfo() {
    return uriInfo;
  }

  /** @param uriInfo the uriInfo to set */
  public void setUriInfo(UriInfo uriInfo) {
    this.uriInfo = uriInfo;
  }
}
