/** */
package edu.byu.hbll.box.internal.web;

import java.time.LocalDateTime;
import java.util.Map;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.internal.core.DocumentProcessor;
import edu.byu.hbll.box.internal.core.Registry;
import edu.byu.hbll.stats.time.Benchmark;

/**
 * NOTE: The {@link Path} endpoints in this class are designed with an extra unnecessary template in
 * order to push them down in matching precedence so as to not hijack all of the application's other
 * paths. https://www.safaribooksonline.com/library/view/restful-java-with/9781449361433/ch04.html
 */
@Path("{sourceName}{x: /stats}")
public class StatsService {

  /** */
  static final Logger logger = LoggerFactory.getLogger(StatsService.class);

  /** */
  private ObjectMapper mapper = new ObjectMapper();

  /** */
  @Inject private Registry registry;

  private LocalDateTime start = LocalDateTime.now();

  /**
   * @param sourceName
   * @param uriInfo
   * @return
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String reprocess(@PathParam("sourceName") String sourceName, @Context UriInfo uriInfo) {

    logger.info("{}", uriInfo.getRequestUri());

    String finalSourceName = registry.verifySource(sourceName);
    DocumentProcessor documentProcessor = registry.getDocumentProcessor(finalSourceName);
    Map<String, Benchmark.Data> stats = documentProcessor.getStats();
    ObjectNode response = mapper.createObjectNode();
    response.put("WARNING", "This API call is experimental and is subject to change.");
    response.put("start", start.toString());
    Benchmark.Data overallStats = stats.get("processBatch");

    if (overallStats != null) {
      response.with("process").put("numBatches", overallStats.getCount());
      response.with("process").put("numDocuments", documentProcessor.getNumDocuments());
      response.with("process").put("averageBatchSize", documentProcessor.getAverageBatchSize());
      stats
          .entrySet()
          .stream()
          .forEach(
              e ->
                  response
                      .with("process")
                      .put(
                          e.getKey() + "Time",
                          ((float) e.getValue().getAverageRecent() / 1000000)));
      response.with("process").put("throughput", overallStats.getThroughput());
      response.with("process").put("throughputRecent", overallStats.getThroughputRecent());
    }

    return response.toString();
  }
}
