/** */
package edu.byu.hbll.box.internal.web;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.Box;
import edu.byu.hbll.box.SourceHealth;

/**
 * NOTE: The {@link Path} endpoints in this class are designed with an extra unnecessary template in
 * order to push them down in matching precedence so as to not hijack all of the application's other
 * paths. https://www.safaribooksonline.com/library/view/restful-java-with/9781449361433/ch04.html
 */
@Path("{sourceName}{x: .?}")
public class HealthService extends BaseService {

  /** */
  static final Logger logger = LoggerFactory.getLogger(HealthService.class);

  /** */
  private static final ObjectMapper mapper = new ObjectMapper();

  /** */
  @Inject private Box box;

  /**
   * @param sourceName
   * @param uriInfo
   * @return
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("health")
  public Response health(@PathParam("sourceName") String sourceName, @Context UriInfo uriInfo) {

    logger.debug("{}", uriInfo.getRequestUri());

    sourceName = resolveSourceName(sourceName);

    if (sourceName == null) {
      return uncrecognizedSource();
    }

    SourceHealth sourceHealth = box.getHealth().getSourceHealth(sourceName);
    ObjectNode response = mapper.createObjectNode();
    response.put("name", sourceName);
    Response.Status status = Status.OK;

    if (sourceHealth != null) {

      response.put("state", sourceHealth.isHealthy() ? "UP" : "DOWN");

      if (sourceHealth.getMessage() != null) {
        response.with("data").put("error", sourceHealth.getMessage());
        status = Status.SERVICE_UNAVAILABLE;
      }
    } else {
      response.put("state", "DOWN");
      response.with("data").put("error", "health check not performed yet");
      status = Status.SERVICE_UNAVAILABLE;
    }

    return Response.status(status).entity(response.toString()).build();
  }
}
