/** */
package edu.byu.hbll.box.internal.web;

import javax.inject.Inject;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import edu.byu.hbll.box.internal.core.Registry;
import edu.byu.hbll.box.internal.core.UpdatesNotifier;

/** */
@ServerEndpoint("/{sourceName}/updates")
public class UpdatesNotifierService {

  /** */
  static final Logger logger = LoggerFactory.getLogger(UpdatesNotifierService.class);

  @Inject UpdatesNotifier updatesNotifier;

  @Inject Registry registry;

  @OnOpen
  public void open(@PathParam("sourceName") String sourceName, Session session) {
    sourceName = registry.verifySource(sourceName);
    updatesNotifier.register(
        sourceName, new WebSocketsUpdateListener(updatesNotifier, sourceName, session));
  }

  @OnClose
  public void close(@PathParam("sourceName") String sourceName, Session session) {
    sourceName = registry.verifySource(sourceName);
    updatesNotifier.unregister(
        sourceName, new WebSocketsUpdateListener(updatesNotifier, sourceName, session));
  }

  @OnError
  public void onError(Throwable error) {
    System.out.println(error.toString());
  }

  @OnMessage
  public void handleMessage(String message, Session session) {
    logger.error("websockets error " + message);
  }
}
