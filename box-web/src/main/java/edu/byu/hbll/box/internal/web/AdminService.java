/** */
package edu.byu.hbll.box.internal.web;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.Box;
import edu.byu.hbll.box.Source;
import edu.byu.hbll.box.internal.core.Registry;

/**
 * NOTE: The {@link Path} endpoints in this class are designed with an extra unnecessary template in
 * order to push them down in matching precedence so as to not hijack all of the application's other
 * paths. https://www.safaribooksonline.com/library/view/restful-java-with/9781449361433/ch04.html
 */
@Path("{sourceName}{x: /admin}")
public class AdminService extends BaseService {

  /** */
  static final Logger logger = LoggerFactory.getLogger(AdminService.class);

  /** */
  @Inject private Registry registry;

  @Inject private Box box;

  /**
   * @param sourceName
   * @param uriInfo
   * @return
   */
  @POST
  @Path("reprocess")
  @Produces(MediaType.APPLICATION_JSON)
  public String reprocess(
      @PathParam("sourceName") String sourceName,
      @PathParam("age") @DefaultValue("PT0S") String age,
      @Context UriInfo uriInfo) {

    logger.info("{}", uriInfo.getRequestUri());

    String finalSourceName = verifySource(sourceName);

    Duration olderThan = Duration.parse(age);
    Source source = registry.getSource(finalSourceName);

    new Thread(() -> source.getDb().addToQueue(olderThan)).start();

    String message =
        createMessage(
            "adding documents older than " + age + " from " + sourceName + " to the process queue");

    return message;
  }

  /**
   * @param sourceName
   * @param uriInfo
   * @return
   */
  @POST
  @Path("harvest")
  @Produces(MediaType.APPLICATION_JSON)
  public String harvest(@PathParam("sourceName") String sourceName, @Context UriInfo uriInfo) {

    logger.info("{}", uriInfo.getRequestUri());

    sourceName = verifySource(sourceName);

    registry.triggerHarvest(sourceName);

    String message = createMessage("triggered harvest for " + sourceName);

    return message;
  }

  /**
   * @param sourceName
   * @param uriInfo
   * @return
   */
  @POST
  @Path("clear")
  @Produces(MediaType.APPLICATION_JSON)
  public String clear(@PathParam("sourceName") String sourceName, @Context UriInfo uriInfo) {
    logger.info("{}", uriInfo.getRequestUri());
    sourceName = verifySource(sourceName);

    List<String> cleared = new ArrayList<>();
    List<String> errored = new ArrayList<>();

    List<String> sourceNames = new ArrayList<>();

    if (sourceName.equals(Source.SOURCE_NAME_ALL)) {
      sourceNames.addAll(box.getSourceNames());
    } else {
      sourceNames.add(sourceName);
    }

    for (String s : sourceNames) {
      try {
        box.clear(s);
        cleared.add(s);
      } catch (Exception e) {
        logger.warn("unable to clear database for " + s + ": " + e);
        errored.add(s);
      }
    }

    String message = createMessage("cleared box database for " + cleared + ", errored on " + errored);
    return message;
  }

  private String createMessage(String message) {
    ObjectNode node = JsonNodeFactory.instance.objectNode();
    node.put("message", message);
    return node.toString();
  }

  /** @param sourceName */
  private String verifySource(String sourceName) {
    if (sourceName.equals(Source.SOURCE_NAME_ALL)) {
      return sourceName;
    }

    sourceName = resolveSourceName(sourceName);

    if (sourceName == null) {
      throw new IllegalArgumentException("source " + sourceName + " not recognized");
    }

    return sourceName;
  }
}
