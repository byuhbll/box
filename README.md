Box is an add-on framework for Java EE applications that performs asynchronous gathering of source data with subsequent processing or transformation of that data into output JSON documents and exposing those documents through a web API. Box excels at maintaining existing documents including keeping them up-to-date in real time and deleting them when required. Box supports aggregating data from multiple sources as well as splitting data into multiple documents. How documents are processed is left up to each application. The web api makes the documents available by ID or by harvesting all documents. Harvests can be filtered using facets and documents can be pared down by requesting only needed fields.

# Getting Started
Find additional Tutorials at [Tutorials](https://bitbucket.org/byuhbll/box/wiki/Tutorials)

Box requires a Java EE 7 environment and existing application to run. To include Box with your application add the following dependencies to your Maven POM.

```xml
<dependency>
  <groupId>edu.byu.hbll.box</groupId>
  <artifactId>box-core</artifactId>
  <version>1.3.5</version>
</dependency>
<dependency>
  <groupId>edu.byu.hbll.box</groupId>
  <artifactId>box-web</artifactId>
  <version>1.3.5</version>
</dependency>
```
    
At this point Box will lay dormant in your application until configured. See Configuration.

A Box instance is made up of processing units called sources. For Box to do anything, you need to configure at least one source. Sources within a single Box instance can interact with each other through a dependency hierarchy.

The next step is to configure Box. See Configuration.

# Configuration
There are multiple ways to configure Box, all of which require injecting the `ApplicationScoped` CDI bean named `Box` into your application.

```java
  @Inject
  private Box box;
```

This object allows you to configure Box. Configuration is all or nothing and you can only configure Box once. Generally Box configuration should occur at application start-up.

## Yaml File

You can configure a new instance of Box with a YAML file. Use the `config` library to load YAML as a JsonNode and pass that node into `box`.

```xml
<dependency>
  <groupId>edu.byu.hbll</groupId>
  <artifactId>config</artifactId>
  <version>3.2.0</version>
</dependency>
```

```java
JsonNode config = new YamlLoader().load(Paths.get("/path/to/config"));
box.initialize(config.path("box"));
```

The YAML has the following form. IMPORTANT: In the sample YAML below, the root field `box` is not necessary, but it helps keep Box configurations separate from other configurations the app requires. You must pass the box object, not the root object (ie, use `box.initialize(config.path("box"))` not `box.initialize(config)`).

```yaml
# Box configuration section
box:
  # Source definitions
  sources:
    # Source 1 (required, any name matching [0-9A-Za-z_]+)
    src1:
      # true|false to access this source through web service without specifying source name
      # ie, /box/document vs /src1/document
      # if no principal is defined, the first source listed is automatically made principal
      principal: false
      # enables this source, a disabled source is skipped completely (default: true);
      enabled: true
      # when to go looking for deleted documents to remove (default: 0 * * * * * *)
      removeSchedule: "0 * * * * * *"
      # removes documents that were deleted more than ISO 8601 duration ago (default: null)
      # note if set to null, removing of deleted documents will not happen
      removeAge: P90D
      # queries resulting documents to generate facets (default: none)
      facetField:
        # facetname: [path.to.field]
      # whether to save the resulting documents (default: true)
      save: true
      # handle and save document only if marked as dependency of other document (default: false)
      dependencyOnly: false
      # default document limit for requests if not specified by caller (default: 10)
      defaultLimit: 10
      process:
        # Class that implements edu.byu.hbll.box.Processor (required)
        type: edu.byu.hbll.box.impl.View
        # One of new, cdi, or ejb (default: new)
        # new -> create new instance using no arg constructor
        # cdi -> inject instance of class using CDI
        # ejb -> inject EJB using supplied mappedName
        instance: new
        # mappedName of ejb, if null defaults to java:global/<appname>/<simpleclassname> (default: null)
        mappedName: null
        # process documents in batches of size up to batchCapacity (default: 1)
        batchCapacity: 1
        # time to wait while batch fills up, use Duration ISO 8601 (default: PT0S)
        batchDelay: PT0S
        # number of threads running the batches (default: 1)
        threadCount: 1
        # limit number of batches processed to this (default: 0)
        quota: 0
        # reset the used-up quota on this schedule (default: 0 0 0 * * * *)
        quotaReset: "0 0 0 * * * *"
        # suspend each thread after processed batch, helps with throttling (default: PT0S)
        suspend: PT0S
        # when to enable processing (default: null)
        # note if either on or off is null, processing will always be on
        "on": "0 0 0 * * * *"
        # when to disable processing (default: null)
        # note if either on or off is null, processing will always be on
        "off": "0 0 7 * * * *"
        # enables this processor, a disabled processor never gets created (default: true);
        enabled: true
        # reprocess documents older than this age in ISO 8601 duration (default: null)
        # note if set to null, reprocessing will not happen
        reprocessAge: P7D
        # whether or not to always process requested documents (default: false)
        # often used in conjunction with save: false
        process: false
        # any additional parameters needed to configure the client
        params:
          # ...
      harvest:
        # Class that implements edu.byu.hbll.box.Harvester (required)
        type: edu.byu.hbll.box.impl.View
        # One of new, cdi, or ejb (default: new)
        # new -> create new instance using no arg constructor
        # cdi -> inject instance of class using CDI
        # ejb -> inject EJB using supplied mappedName
        instance: new
        # mappedName of ejb, if null defaults to java:global/<appname>/<simpleclassname> (default: null)
        mappedName: null
        # enable the harvester, a disabled harvester never gets created (default: true)
        enabled: true
        # when to run the harvest (default: 0 * * * * * *)
        schedule: "0 * * * * * *"
        # schedule for reseting the harvest by clearing the stored cursor (default: null)
        resetSchedule: null
        # save new deleted documents (default: false)
        saveDeleted: false
        # any additional parameters needed to configure the harvester
        params:
          # ...
      db: # same as global database definition below, but specific to this source
        ...
      cursor: # overrides the database config for the harvest cursor only, if type is left blank or null, it will use the global database config, otherwise configuration is the same as the definition below
        ...
    # Source 2:
    # src2:
      # ...
  # Database definition (can be overridden per source)
  db:
    # class that implements edu.byu.hbll.box.BoxDatabase (default: edu.byu.hbll.box.impl.InMemoryDatabase)
    # other pre-implemented options: edu.byu.hbll.box.impl.MongoDatabase
    type: edu.byu.hbll.box.impl.InMemoryDatabase
    # any additional parameters needed to configure the database client
    params:
      # ...
```

All schedules are written cron-like, but strictly follow the specifications found at http://docs.oracle.com/javaee/7/api/javax/ejb/Schedule.html. The time unit order is as follows:

    second minute hour dayOfMonth month dayOfWeek year 

## JsonNode
You can configure Box by passing a JsonNode to the `Box` object just like when using the YAML file. IMPORTANT: Box expects the children of the `box` field in the YAML file example above to be at the root, not the `box` field.

```java
ObjectNode config = JsonNodeFactory.instance.objectNode();
...
box.initialize(config);
```

## Programmatically
You can also configure Box programmatically by passing a `BoxConfiguration` object to the `Box` object.

```java
BoxConfiguration config = new BoxConfiguration();
...
box.initialize(config)
```

# Vocabulary
## Source
Initial or seed data come from somewhere and in Box we refer to those as sources. A source exists of a processor or harvester or both. These two processors differ in the way data is queried and brought into the system. Each source in an application should have a unique name matching the pattern [0-9A-Za-z_].
### Processor
A processor that only executes for a given ID when requested by another application.
### Harvester
A harvester that processes new and updated documents on a schedule perhaps by processing updated documents from other sources.

# Implementations
Box comes pre-baked with several common implementations of its Processor, Harvester, and BoxDatabase interfaces.

## EJB
Implementations of Processor and Harvester can be EJBs when using the EjbProcessor and EjbHarvester. The YAML file describes how to use them from the configuration.

## CDI
Implementations of Processor and Harvester can be injected when using the CdiProcessor and CdiHarvester. The YAML file describes how to use them from the configuration.

## View
A Processor, Harvester, and Database implementation that acts as a read-only view into another source inside or outside the current box. Box sees a view as nothing more than a local source. Configuring a view properly is somewhat complicated, so Box supports templates for easily setting them up. It is also possible to use the View as separate components. For instance, you may only want the View for harvesting from another source in order to follow that source and fill the process queue.

```yaml
box:
  sources:
    mysrc:
      template:
        id: view
        params:
          uri: https://www.example.com/path/to/box
          
          # instead of hitting local sources through the web interface, you can specify the name
          # of the source here. This overrides anything in the uri field.
          local: mysrc
          
          # optional params
          # fields to return for each document. Default: [null]
          fields: [field1, field2]
          # only include documents with these statuses. Default: [READY,DELETED]
          statuses: [READY,DELETED]
          # filter documents by facets; key value pairs in the form of "name:value". Default: [null]
          facets: []
          # limit of documents per request Default: -1 (non specified limit)
          limit: 10
          # whether or not to harvest documents from view as unprocessed so they'll get put on the process queue
          harvestUnprocessed: false
          # username for basic authentication. Default: null
          username: myusername
          # password to use with basic authentication. Default: null
          password: mypassword
```

### Source Follower
View can be used as a source follower. Every document created, updated, or deleted at the upstream source results in a parallel document being processed in this source. For this you will not use the view template, but rather define View as just a Harvester alongside your Processor.

```yaml
box:
  sources:
    mysrc:
      process:
        ...
      harvest:
        type: edu.byu.hbll.box.impl.View
        params:
          uri: https://www.example.com/path/to/box
          harvestUnprocessed: true
```

## FacetView
Built on top of a View, the FacetView targets documents by facet instead of id. When querying the upstream source as a View would do, it translates the requested id query into a facet query. The documents in the response are are then a collection of documents representing the requested facet value.

```yaml
box:
  sources:
    mysrc:
      template:
        id: facetView
        params:
          uri: https://www.example.com/path/to/box
          facetName: the_facet_group_to_target
          
          # instead of hitting local sources through the web interface, you can specify the name
          # of the source here. This overrides anything in the uri field.
          local: mysrc
          
          # optional params
          # fields to return for each document. Default: [null]
          fields: [field1, field2]
          # only include documents with these statuses. Default: [READY,DELETED]
          statuses: [READY,DELETED]
          # filter documents by facets; key value pairs in the form of "name:value". Default: [null]
          facets: []
          # limit of documents per request Default: -1 (non specified limit)
          limit: 10
          # whether or not to harvest documents from view as unprocessed so they'll get put on the process queue
          harvestUnprocessed: false
          # username for basic authentication. Default: null
          username: myusername
          # password to use with basic authentication. Default: null
          password: mypassword
          # limit number of documents included with each facet. Default: -1 (no limit)
          documentLimit: -1
```

## Cached Views
Both the View and FacetViews can be used to cache upstream documents rather than viewing them live. Again templates are used for this:

```yaml
box:
  sources:
    mysrc:
      template:
        id: cachedView
        params:
          uri: https://www.example.com/path/to/box
          
          # optional params
          # fields to return for each document. Default: [null]
          fields: [field1, field2]
          # only include documents with these statuses. Default: [READY,DELETED]
          statuses: [READY,DELETED]
          # filter documents by facets; key value pairs in the form of "name:value". Default: [null]
          facets: []
          # limit of documents per request Default: -1 (non specified limit)
          limit: 10
          # whether or not to harvest documents from view as unprocessed so they'll get put on the process queue
          harvestUnprocessed: false
          # username for basic authentication. Default: null
          username: myusername
          # password to use with basic authentication. Default: null
          password: mypassword
```

```yaml
box:
  sources:
    mysrc:
      template:
        id: cachedFacetView
        params:
          uri: https://www.example.com/path/to/box
          facetName: the_facet_group_to_target
          
          # optional params
          # fields to return for each document. Default: [null]
          fields: [field1, field2]
          # only include documents with these statuses. Default: [READY,DELETED]
          statuses: [READY,DELETED]
          # filter documents by facets; key value pairs in the form of "name:value". Default: [null]
          facets: []
          # limit of documents per request Default: -1 (non specified limit)
          limit: 10
          # whether or not to harvest documents from view as unprocessed so they'll get put on the process queue
          harvestUnprocessed: false
          # username for basic authentication. Default: null
          username: myusername
          # password to use with basic authentication. Default: null
          password: mypassword
```

## OAI
A Processor and Harvester that requests documents from OAI responders are implemented for you as the OaiProcessor and OaiHarvester. These processors take the following params:

```yaml
box:
  sources:
    process:
      type: edu.byu.hbll.box.impl.OaiProcessor
      params:
        # the URI of the OAI responder (required)
        uri: "https://www.example.com/oai"
        # The metadataPrefix to use (default: oai_dc)
        metadataPrefix: oai_dc
        # regex to convert the Box ID to the OAI ID
        idRegex: "^ojs\\."
        # regex to convert the Box ID to the OAI ID
        idReplacement: "oai:ojsspc.lib.byu.edu:"
        
    harvest:
      type: edu.byu.hbll.box.impl.OaiHarvester
      params:
        # the URI of the OAI responder (required)
        uri: "https://www.example.com/oai"
        # The metadataPrefix to use (default: oai_dc)
        metadataPrefix: oai_dc
        # Whether or not to harvest ids only instead of full records (default: false)
        idsOnly: false
        # Whether or not to query one month at a time (default: false)
        chunked: false
        # regex to convert the Box ID to the OAI ID (default: null)
        idRegex: "^ojs\\."
        # regex to convert the Box ID to the OAI ID (default: null)
        idReplacement: "oai:ojsspc.lib.byu.edu:"
```

## MongoDB
MongoDB implementation of BoxDatabase.

```yaml
box:
  db:
    type: edu.byu.hbll.box.impl.MongoDatabase
    params:
      # MongoDB Connection String URI (default: mongodb://localhost/) See https://docs.mongodb.com/manual/reference/connection-string/ 
      uri: mongodb://localhost/
      # Database name for this instance of box (required)
      database: box
```

## In Memory Database
An implementation of BoxDatabase that holds all data in memory.

```yaml
box:
  db:
    type: edu.byu.hbll.box.impl.InMemoryDatabase
```

# API

To enable the box web service API, the box-web module must be included as dependency in the POM. The box API path begins after the application's root path. All APIs are found under the `/box` root path.

    https://www.example.com/app/box

## Multiple Documents and Harvesting

Path

    GET /box/documents
    GET /{source}/documents
     
Path parameters
     
    source         : the source name (type: string, default: none)

Query parameters for multiple documents

    id             : filter by document id (type: string, default: null, multiple: true)
    wait           : wait for documents to process (type: boolean, default: false)
    process        : (re)process the documents (type: boolean, default: false)
    field          : only return these document fields (type: string, default: null, multiple: true)

Query parameters for harvesting

    from           : modified since (type: ISO Date, default: null)
    cursor         : modified since (type: long, default: 0)
    limit          : page size (type: int, default: 10)
    status         : include only documents with these statuses (type: string, default: ready,deleted)

    facet          : filter by facet, OR logic within a facet group, AND logic between groups (type: string in the form `key:value`, default: null, multiple: true)

Common query parameters

    field          : only return these document fields (type: string, default: null, multiple: true)
    metadataLevel  : how much metadata to return (type: enum; values: none, core, full; default: core)
    metadataOnly   : whether or not to only return metadata (type: boolean; default: false)

## Single Documents

Path

    GET /box/documents/{id}
    GET /{source}/documents/{id}

Path parameters
     
    source         : the source name (type: string, default: none)
    id             : the document id (type: string, default: none)
     
Query parameters

    wait           : wait for documents to process (type: boolean, default: false)
    process        : (re)process the documents (type: boolean, default: false)
    field          : only return these document fields (type: string, default: none, multiple: true)
    metadataLevel  : how much metadata to return (type: enum; values: none, core, full; default: core)
    metadataOnly   : whether or not to only return metadata (type: boolean; default: false)

## Administration

### Reprocess documents

Path

    POST /box/admin/reprocess
    POST /{source}/admin/reprocess
    
Path parameters
     
    source : the source name (type: string, default: none)
     
Query parameters

    age    : min age of documents (type: ISO 8601 duration, default: PT0S)

### Trigger harvest

Path

    POST /box/admin/harvest
    POST /{source}/admin/harvest
    
Path parameters
     
    source : the source name (type: string, default: none)

### Clear Database

Path

    POST /box/admin/clear
    POST /{source}/admin/clear
    
Path parameters
     
    source : the source name; `all` is a reserved source name referring to all sources (type: string, default: none)


## Stats

Path

    GET /box/stats
    GET /{source}/stats
    
Path parameters
     
    source : the source name (type: string, default: none)
     

# Security

Box itself does not provide any security for its web services; however, you can add your own if needed. You can setup security in your web.xml and application server. To do basic authentication, for example, create a src/main/webapp/WEB-INF/web.xml file with the following components:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://xmlns.jcp.org/xml/ns/javaee"
  xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd" id="WebApp_ID"
  version="3.1">
  <security-constraint>
    <web-resource-collection>
      <web-resource-name>box</web-resource-name>
      <url-pattern>/box/admin/*</url-pattern>
    </web-resource-collection>
    <auth-constraint>
      <role-name>boxAdmin</role-name>
    </auth-constraint>
  </security-constraint>
  <login-config>
    <auth-method>BASIC</auth-method>
    <realm-name>jdbcRealm</realm-name>
  </login-config>
</web-app>
```

# ID Only Harvest
At times a harvester is only able to indicate the IDs of the documents, but not process the documents itself. In this case, a direct processor must exist to actually process the documents according the gathered IDs. To indicate that the harvester is only sending IDs and not documents, do not set the document.

```java
new ResultDocument(id);
```

# Orphaned Documents
Generally documents that should be deleted are explicitly marked so in the `BoxDocument`. There are times; however, that the source alone cannot determine when a document should be deleted. This can happen for `Processor` in which case the `reprocessSchedule` and `reprocessAge` directives become vital. When those are set, Box will attempt to reprocess documents on a schedule allowing the `Processor` to mark the document deleted when appropriate.

This can also occur for a `Harvester` that only knows about new or updated documents, but not deletes. If the `Harvester` handled this itself, it would have to perform a diff of the documents that SHOULD exist against the documents that DO exist. Thankfully, Box facilitates this diff and will clean up orphan documents for you. This is done by using the `startGroup` and `endGroup` directives. At the beginning of a fresh harvest, mark the start of the group. When the harvest completes, mark the end of the group. Box will then delete any documents belonging to that group that were not processed during that time. 

Orphaned documents can also occur when an input document splits into multiple output documents. The child documents that get created can change over time leaving orphaned documents. Box can automatically clean these up by using the group mechanism. Basically the processor that splits the document will signal the beginning of the group and then later signal the end of the group. Any existing documents belonging to the same group that were not processed during the window will be deleted. This is how it's typically done:

```java
// code to split a parent document into multiple child documents
ProcessResult result = new ProcessResult();
result.startGroup(parentId);
result.add(childDocument1.groupId(parentId));
result.add(childDocument2.groupId(parentId));
result.add(childDocument3.groupId(parentId));
result.endGroup(parentId);
// mark the parent document as deleted so it's removed from the process queue
result.add(new BoxDocument(parentId).setAsDeleted());
``` 

Note that the start and end group signals do not need to be in the same ProcessResult or HarvestResult. They can traverse long periods of time and even restarts.

Note that scheduled reprocessing or direct processing of these child documents can occur. The Processor will need to be able to handle this when a request is received to (re)process a child document.

# Dependencies
Box manages dependencies for you when a document is dependent on documents from other sources. These dependencies are set during process.

```java
new BoxDocument(id).addDependency(dependencySourceName, dependencyId);
```

Box will asynchronously gather these dependencies and hand them to the Processor along with the document to be processed. It reprocesses the document each time a dependency is made available or updated.

Box also allows dependencies to be collected before processing. Sometimes it's necessary to gather dependencies before the document can be processed. For this case, set the id of the document and its dependencies, but don't set the document itself. It needs to remain in an UNPROCESSED state. 

# Important
A circular dependency involving only views will lock up the applications involved. So don't create a circular dependency circle of just views. Even one non-view in there would be fine. It's only if they're ALL views.

Requested documents that do not exist at a harvest only source (ie, do not have a processor or processor is disabled) will automatically be marked as deleted in the response.

An unprocessed document cannot overwrite a processed document. This aligns with the philosophy of not having documents regress. A processed document's dependencies however can be updated by an unprocessed document.
 

