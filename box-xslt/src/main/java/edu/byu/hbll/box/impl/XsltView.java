/** */
package edu.byu.hbll.box.impl;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.DocumentId;
import edu.byu.hbll.box.InitConfig;
import edu.byu.hbll.box.ProcessBatch;
import edu.byu.hbll.box.ProcessContext;
import edu.byu.hbll.xml.XmlJsonConverter;
import edu.byu.hbll.xml.XmlJsonSchema;
import edu.byu.hbll.xslt.XslTransformer;
import edu.byu.hbll.xslt.plugins.Functions;
import net.sf.saxon.lib.ExtensionFunctionDefinition;

/**
 * A harvest/direct processor that retrieves documents from another Box and transforms them using
 * XSLT.
 *
 * <p>The source document will be of the form:
 *
 * <pre>
 * &lt;source&gt;
 *   &lt;id&gt;&lt;-- id of the document --&gt;&lt;/id&gt;
 *   &lt;document&gt;
 *     &lt;!-- the source document --&gt;
 *   &lt;/document&gt;
 *   &lt;dependency&gt;
 *     &lt;-- optional Box dependency to accompany the document (repeatable) --&gt;
 *     &lt;sourceName&gt;&lt;-- sourceName of the dependency --&gt;&lt;/sourceName&gt;
 *     &lt;id&gt;&lt;-- id of the dependency --&gt;&lt;/id&gt;
 *     &lt;document&gt;&lt;-- the dependency document --&gt;&lt;/document&gt;
 *   &lt;/dependency&gt;
 * &lt;/source&gt;
 * </pre>
 *
 * <p>The resulting document should be of the form:
 *
 * <pre>
 * &lt;result&gt;
 *   &lt;document&gt;
 *     &lt;!-- the resulting document --&gt;
 *   &lt;/document&gt;
 *   &lt;facet&gt;
 *     &lt;-- optional Box facet to accompany the document (repeatable) --&gt;
 *     &lt;name&gt;&lt;-- name of the facet group --&gt;&lt;/name&gt;
 *     &lt;value&gt;&lt;-- value of the facet --&gt;&lt;/value&gt;
 *   &lt;/facet&gt;
 *   &lt;dependency&gt;
 *     &lt;-- optional Box dependency to accompany the document (repeatable) --&gt;
 *     &lt;sourceName&gt;&lt;-- sourceName of the dependency --&gt;&lt;/sourceName&gt;
 *     &lt;id&gt;&lt;-- id of the dependency --&gt;&lt;/id&gt;
 *   &lt;/dependency&gt;
 * &lt;/result&gt;
 * </pre>
 *
 * @author Charles Draper
 */
public class XsltView extends View {

  static final Logger logger = LoggerFactory.getLogger(XsltView.class);

  /** The XSLT Transformer used to transform the documents. */
  private XslTransformer transformer;

  /** The source schema, describes how to convert the JSON to XML pre-transformation. */
  private XmlJsonSchema sourceSchema = new XmlJsonSchema();

  /** The result schema, describes how to convert the XML to JSON post-transformation. */
  private XmlJsonSchema resultSchema = new XmlJsonSchema();

  /** */
  protected XsltView() {
    super();
  }

  /**
   * @param baseUri base Box URI for the source documents
   * @param xsltFile the XSLT to use
   */
  public XsltView(String baseUri, File xsltFile) {
    this(baseUri, xsltFile, null, null, null);
  }

  /**
   * @param baseUri base Box URI for the source documents
   * @param xsltFile the XSLT to use
   * @param xsltDir optionally watch this directory for changes and reload the XSLT (null allowed)
   * @param sourceXsdFile optionally transform resulting JSON to XML pre-transformation (null
   *     allowed)
   * @param resultXsdFile optionally transform resulting XML to JSON using these rules (null
   *     allowed)
   */
  public XsltView(
      String baseUri, File xsltFile, File xsltDir, File sourceXsdFile, File resultXsdFile) {
    super(baseUri);
    init(xsltFile, xsltDir, sourceXsdFile, resultXsdFile, Collections.emptyList());
  }

  /**
   * @param baseUri base Box URI for the source documents
   * @param xsltFile the XSLT to use
   * @param xsltDir optionally watch this directory for changes and reload the XSLT (null allowed)
   * @param sourceXsdFile optionally transform resulting JSON to XML pre-transformation (null
   *     allowed)
   * @param resultXsdFile optionally transform resulting XML to JSON using these rules (null
   *     allowed)
   * @param functions additional extension functions to include
   */
  public XsltView(
      String baseUri,
      File xsltFile,
      File xsltDir,
      File sourceXsdFile,
      File resultXsdFile,
      Collection<? extends ExtensionFunctionDefinition> functions) {
    super(baseUri);
    init(xsltFile, xsltDir, sourceXsdFile, resultXsdFile, functions);
  }

  @Override
  public void postConstruct(InitConfig config) {
    super.postConstruct(config);
    ObjectNode params = config.getParams();
    File xsltFile = new File(params.path("xsltFilePath").asText(null));
    File xsltDir =
        params.has("xsltDirectoryPath")
            ? new File(params.path("xsltDirectoryPath").asText(null))
            : null;
    File sourceXsdFile =
        params.has("sourceXsdFilePath")
            ? new File(params.path("sourceXsdFilePath").asText(null))
            : null;
    File resultXsdFile =
        params.has("resultXsdFilePath")
            ? new File(params.path("resultXsdFilePath").asText(null))
            : null;
    init(xsltFile, xsltDir, sourceXsdFile, resultXsdFile, Collections.emptyList());
  }

  /**
   * Initializes the transformer and schema.
   *
   * @param xsltFile the XSLT to use
   * @param xsltDir optionally watch this directory for changes and reload the XSLT (null allowed)
   * @param sourceXsdFile optionally transform resulting JSON to XML pre-transformation (null
   *     allowed)
   * @param resultXsdFile optionally transform resulting XML to JSON using these rules (null
   *     allowed)
   * @param functions additional extension functions to include
   */
  private void init(
      File xsltFile,
      File xsltDir,
      File sourceXsdFile,
      File resultXsdFile,
      Collection<? extends ExtensionFunctionDefinition> functions) {

    try {
      Collection<ExtensionFunctionDefinition> allFunctions = new ArrayList<>();
      allFunctions.addAll(Functions.newFunctions());
      allFunctions.addAll(functions());
      allFunctions.addAll(functions);

      transformer = new XslTransformer(xsltFile, xsltDir, allFunctions);
      sourceSchema = new XmlJsonSchema("source");

      if (sourceXsdFile != null) {
        XmlJsonSchema documentSchema = XmlJsonSchema.parseXsd(new StreamSource(sourceXsdFile));
        documentSchema.setName("document");
        documentSchema.setXmlName("document");
        sourceSchema.add(documentSchema);
      }

      resultSchema = new XmlJsonSchema("result");

      if (resultXsdFile != null) {
        XmlJsonSchema documentSchema = XmlJsonSchema.parseXsd(new StreamSource(resultXsdFile));
        documentSchema.setName("document");
        documentSchema.setXmlName("document");
        resultSchema.add(documentSchema);
      }

      resultSchema.add(new XmlJsonSchema("facet").array());
      resultSchema.add(new XmlJsonSchema("dependency").array());
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  protected List<BoxDocument> transform(ProcessBatch batch) {
    List<BoxDocument> result = new ArrayList<>();

    for (ProcessContext context : batch) {
      result.add(transform(context));
    }

    return result;
  }

  /**
   * Transforms one document at a time.
   *
   * @param processContext the source document and metadata
   * @return the resulting transformed document
   */
  protected BoxDocument transform(ProcessContext processContext) {
    String id = processContext.getId();
    BoxDocument sourceDocument = processContext.getFirstDependency(getSourceName());
    BoxDocument resultDocument = new BoxDocument(id);

    if (sourceDocument.isDeleted()) {
      resultDocument.setAsDeleted();
    } else {
      try {

        // create source wrapper document
        ObjectNode source = JsonNodeFactory.instance.objectNode().put("id", id);
        source.set("document", sourceDocument.getDocument());

        for (DocumentId documentId : processContext.getDependencies().keySet()) {
          BoxDocument dependency = processContext.getDependency(documentId);
          
          // do not include the source document as a dependency
          if (dependency != sourceDocument && !dependency.isDeleted()) {
            source
                .withArray("dependency")
                .addObject()
                .put("sourceName", documentId.getSourceName())
                .put("id", documentId.getId())
                .set("document", dependency.getDocument());
          }
        }

        // convert source wrapper document to XML
        Document sourceXml = XmlJsonConverter.toXmlDoc(source, sourceSchema);

        // add default parameters
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("id", id);
        params.put("sourceName", getSourceName());

        // transform the source according to the XSLT rules
        Document transformedXml = transform(sourceXml, params);

        Element rootElement = transformedXml.getDocumentElement();

        if (rootElement == null) {
          throw new IllegalArgumentException(
              "transformed result is blank, it must contain a root element of <result>");
        }

        if (!rootElement.getLocalName().equals("result")) {
          throw new IllegalArgumentException(
              "transformed result must contain a root element of <result>");
        }

        // convert the transformed result back to a JSON document
        ObjectNode document = XmlJsonConverter.toJsonDoc(rootElement, resultSchema);

        if (document.path("document").isObject()) {
          resultDocument.setDocument((ObjectNode) document.path("document"));
        } else {
          throw new IllegalArgumentException(
              "root element <result> must contain a <document> element even if empty");
        }

        addDependencies(resultDocument, document.path("dependency"));
        addFacets(resultDocument, document.path("facet"));
      } catch (Exception e) {
        logger.error("Error transforming " + id + ": " + e, e);
        resultDocument.setAsError(e.toString());
      }
    }

    return resultDocument;
  }

  /**
   * Transforms the source xml into the result xml.
   *
   * @param sourceXml the source xml
   * @param params any parameters to send to the Transformer
   * @return
   * @throws TransformerException if an error occurs with the transformation
   */
  protected Document transform(Document sourceXml, Map<String, ? extends Object> params)
      throws TransformerException {
    return transformer.transform(sourceXml, params);
  }

  /**
   * Adds all dependencies from the transformed document to the given resultDocument.
   *
   * @param resultDocument the resultDocument
   * @param dependencies the dependencies to add
   */
  private void addDependencies(BoxDocument resultDocument, JsonNode dependencies) {
    for (JsonNode dependency : dependencies) {
      resultDocument.addDependency(
          dependency.path("sourceName").asText(null), dependency.path("id").asText(null));
    }
  }

  /**
   * Adds all facets from the transformed document to the given resultDocument.
   *
   * @param resultDocument the resultDocument
   * @param facets the facets to add
   */
  private void addFacets(BoxDocument resultDocument, JsonNode facets) {
    for (JsonNode facet : facets) {
      resultDocument.addFacet(facet.path("name").asText(null), facet.path("value").asText(null));
    }
  }

  /**
   * Child classes can specify additional extension functions to include with the Saxon Transformer.
   * This is called exactly once during the initialization of the class.
   *
   * @return a collection of additional extensions functions to be included with the transformer
   */
  protected Collection<? extends ExtensionFunctionDefinition> functions() {
    return Collections.emptyList();
  }

  @Override
  public void preDestroy() {
    try {
      super.preDestroy();
    } catch (Exception e) {
      logger.warn(e.toString());
    }

    transformer.close();
  }
}
